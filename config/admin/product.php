<?php

return [
    /*
     * Is Feature
     */
    'isFeature'       =>
    [
        "0" => 'No',
        "1" => 'Yes',
    ],
    /*
     * Flash Sale
     */
    'flashSale'       =>
    [
        "0" => 'No',
        "1" => 'Yes',
    ],
    /*
     * Flash Sale Status
     */
    'flashSaleStatus' =>
    [
        "1" => 'Active',
        "0" => 'Inactive',
    ],
    /*
     * Special Status
     */
    'specialStatus'   =>
    [
        "1" => 'Active',
        "0" => 'Inactive',
    ],
    /*
     * Special Product
     */
    'isSpecial'       =>
    [
        "0" => 'No',
        "1" => 'Yes',
    ],
    /*
     * Is Active
     */
    'isActive'        =>
    [
        "1" => 'Active',
        "0" => 'Inactive',
    ],
    /*
     * New Arrival
     */
    'isNewArrival'    =>
    [
        "0" => 'No',
        "1" => 'Yes',
    ],
    /*
     * Latest Collection
     */
    'isLatestCollection'    =>
    [
        "0" => 'No',
        "1" => 'Yes',
    ],
    'addMoreLimit'    => 5,
];
