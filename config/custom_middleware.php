<?php

/*
 * Custom config file
 * 
 */

return [
    'view_super_admin_role'   => 'View_1_1',
    'create_super_admin_role' => 'Create_1_1',
    'edit_super_admin_role'   => 'Edit_1_1',
    'delete_super_admin_role' => 'Delete_1_1',
    'view_admin'              => 'View_2',
    'create_admin'            => 'Create_2',
    'edit_admin'              => 'Edit_2',
    'view_product'            => 'View_2_10',
    'create_product'          => 'Create_2_10',
    'edit_product'            => 'Edit_2_10',
    'delete_product'          => 'Delete_2_10',
    'view_product_category'   => 'View_2_10_18',
    'create_product_category' => 'Create_2_10_18',
    'edit_product_category'   => 'Edit_2_10_18',
    'delete_product_category' => 'Delete_2_10_18',
    'view_customer'           => 'View_2_12',
    'create_customer'         => 'Create_2_12',
    'edit_customer'           => 'Edit_2_12',
    'delete_customer'         => 'Delete_2_12',
    'view_product_product'    => 'View_2_10_22',
    'create_product_product'  => 'Create_2_10_22',
    'edit_product_product'    => 'Edit_2_10_22',
    'delete_product_product'  => 'Delete_2_10_22',
];

