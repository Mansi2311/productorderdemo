<?php
return [
    'per_page'                  => env('PER_PAGE', 10),
    'btn-primary'               => 'btn btn-outline-primary btn-sm',
    'btn-danger'                => 'btn btn-outline-danger btn-sm',
    'btn-info'                  => 'btn btn-outline-info btn-sm',
    'btn-warning'               => 'btn btn-outline-warning btn-sm',
    'btn-success'               => 'btn btn-outline-success btn-sm',
    'btn-primary-form'          => 'btn btn-outline-primary',
    'btn-success-form'          => 'btn btn-outline-success',
    'btn-danger-form'           => 'btn btn-outline-danger',
    'card-primary'              => 'card-primary card-outline',
    'text-note-css'             => 'text-primary',
    'select2-css'               => 'select2-primary',
    'date-time'                 => 'd-m-Y H:i:s',
    'date'                      => 'd-m-Y',
];

