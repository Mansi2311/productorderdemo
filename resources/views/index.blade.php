@extends('layout/main')
@section('additional_css')
<link rel="stylesheet" href="{{asset('dist/css/AdminLTEDash.min.css')}}">
@endsection
@section('content')
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Login Successfully</h1>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page_specific_js')
@endsection