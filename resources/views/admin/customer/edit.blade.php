@extends('layout/main')
@section('page_title',  trans('admin.customer'))
@section('additional_css')
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('public/custom/css/custom.css')}}">
@endsection
@section('content')
@include('layout/toaster')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{__('admin.customer')}}</h1>
                </div>
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{route('admin.home.index')}}">{{__('admin.home')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.customer.index')}}">{{__('admin.customer')}}</a></li>
                        <li class="breadcrumb-item active">{{__('admin.edit')}}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card {{config('custom.card-primary')}}">
                <div class="card-header">
                    <h3 class="card-title">{{__('admin.edit')}}</h3>
                </div>
                <form method="post" action="{{route('admin.customer.update',['id' => $singleRecord->id])}}">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group"> 
                                    <label>{{__('admin.first_name')}}</label><span class="required"></span>
                                    <input type="text" name="first_name" placeholder="{{__('admin.first_name')}}" class="form-control text-input length-30 {{ $errors->has('first_name') ?'is-invalid':'' }}"  value="{{old('first_name',$singleRecord->first_name)}}">
                                    @error('first_name')
                                    <div class="text-red">{!! $message !!}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group"> 
                                    <label>{{__('admin.last_name')}}</label><span class="required"></span>
                                    <input type="text" name="last_name" placeholder="{{__('admin.last_name')}}" class="form-control text-input length-30 {{ $errors->has('last_name') ?'is-invalid':'' }}" value="{{old('last_name',$singleRecord->last_name)}}">
                                    @error('last_name')
                                    <div class="text-red">{!! $message !!}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-12">
                                <div class="form-group"> 
                                    <label>{{__('admin.telephone')}}</label><span class="required"></span>
                                    <input type="text" name="mobile" placeholder="{{__('admin.telephone')}}" class="form-control number-input length-10 {{ $errors->has('mobile') ?'is-invalid':'' }}" value="{{old('mobile',$singleRecord->mobile)}}">
                                    @error('mobile')
                                    <div class="text-red">{!! $message !!}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group"> 
                                    <label>{{__('admin.email_address')}}</label><span class="required"></span>
                                    <input type="email" name="email" placeholder="{{__('admin.email_address')}}" class="form-control text-input length-50 {{ $errors->has('email') ?'is-invalid':'' }}" value="{{old('email',$singleRecord->email)}}">
                                    @error('email')
                                    <div class="text-red">{!! $message !!}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-12">
                                <div class="form-group">
                                    <label class="radio-label">{{__('admin.gender')}}</label>
                                    <div class="radiobox-row">
                                        <label class="radiobox-col">
                                            <input type="radio" checked="checked" id="male" name="gender"  value="1" {{old('gender',$singleRecord->gender) == '1' ? 'checked' : '' }} >
                                            <span class="checkmark"></span>
                                            {{__('admin.male')}}
                                        </label>
                                        <label class="radiobox-col">
                                            <input type="radio" name="gender" id="female" value="0" {{old('gender',$singleRecord->gender) == '0' ? 'checked' : '' }} >
                                            <span class="checkmark"></span>
                                            {{__('admin.female')}}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group"> 
                                <label>{{__('admin.address')}}</label><span class="required"></span>
                                <textarea type="text" name="address" placeholder="{{__('admin.address')}}" class="form-control text-input length-50 {{ $errors->has('address') ?'is-invalid':'' }}" >{{old('address',$singleRecord->address)}}</textarea>
                                @error('address')
                                <div class="text-red">{!! $message !!}</div>
                                @endif
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <span class="float-left">{!!__('admin.additional_notes')!!}</span>
                        <div class="btn-toolbar float-right">
                            <div class="btn-group mr-2">
                                <a href="{{route('admin.customer.index')}}" class="{{config('custom.btn-danger-form')}}" title="{{__('admin.cancel')}}">{{__('admin.cancel')}}</a>
                            </div>
                            <div class="btn-group">
                                <button type="submit" class="{{config('custom.btn-success-form')}}" title="{{__('admin.update')}}">{{__('admin.update')}}</button>
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </section>
</div>
@endsection
@section('page_specific_js')
@include('layout/select2')
<script>

</script>
@endsection
