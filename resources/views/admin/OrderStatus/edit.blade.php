@extends('layout/main')
@section('page_title',  trans('admin.order'))
@section('additional_css')
@endsection
@section('content')
@include('layout/toaster')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{__('admin.order_status')}}</h1>
                </div>
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{route('admin.home.index')}}">{{__('admin.home')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.order-status.index')}}">{{__('admin.order_status')}}</a></li>
                        <li class="breadcrumb-item active">{{__('admin.add')}}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card {{config('custom.card-primary')}}">
                <div class="card-header">
                    <h3 class="card-title">{{__('admin.add')}}</h3>
                </div>
                <form method="post" action="{{route('admin.order-status.update',['id' => $singleRecord->id])}}">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label>{{__('admin.select-customer')}}</label><span class="required"></span>
                                    <select name="admin_id"  class="form-control select2 select-customer {{config('custom.select2-css')}}" data-dropdown-css-class="{{config('custom.select2-css')}}">
                                        @foreach($customer as $cust)
                                        <option value="{{$cust->id}}" {{ (collect(old('admin_id',$singleRecord->admin_id))->contains($cust->id)) ? 'selected':'' }}>
                                            {{$cust?$cust->first_name.' '.$cust->last_name:''}}</option>
                                        @endforeach
                                    </select>
                                    @error('admin_id')
                                    <div class="text-red">{{ $message }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label>{{__('admin.select-product')}}</label><span class="required"></span>
                                    <select name="product_id"  class="form-control select2 select-product {{config('custom.select2-css')}}" data-dropdown-css-class="{{config('custom.select2-css')}}">
                                        @foreach($product as $record)
                                        <option value="{{$record->id}}" {{ (collect(old('product_id',$singleRecord->product_id))->contains($record->id)) ? 'selected':'' }}>
                                            {{$record?$record->product_name:''}}</option>
                                        @endforeach
                                    </select>
                                    @error('product_id')
                                    <div class="text-red">{{ $message }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label>{{__('admin.order_status')}}</label><span class="required"></span>
                                    <input type="text" class="form-control length-10" placeholder="{{__('admin.order_status')}}" name="order_status" type="text" value="{{old('order_status',$singleRecord->order_status)}}">
                                    @error('order_status')
                                    <div class="text-red">{{ $message }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <span class="float-left">{!!__('admin.additional_notes')!!}</span>
                        <div class="btn-toolbar float-right">
                            <div class="btn-group mr-2">
                                <a href="{{route('admin.order-status.index')}}" class="{{config('custom.btn-danger-form')}}" title="{{__('admin.cancel')}}">{{__('admin.cancel')}}</a>
                            </div>
                            <div class="btn-group">
                                <button type="submit" class="{{config('custom.btn-success-form')}}" title="{{__('admin.update')}}">{{__('admin.update')}}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@endsection
@section('page_specific_js')
<script>
    $(document).ready(function ()
    {
        $(".select-product").select2({
            placeholder: "{{__('admin.select-product')}}",
            allowClear: true
        });
        $(".select-customer").select2({
            placeholder: "{{__('admin.select-customer')}}",
            allowClear: true
        });
    });
</script>
@endsection