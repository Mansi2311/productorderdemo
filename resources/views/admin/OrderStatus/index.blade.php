@extends('layout/main')
@section('page_title',  trans('admin.product'))
@section('additional_css')
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            @include('layout/toaster')
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{__('admin.product')}}</h1>
                </div>
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{route('admin.home.index')}}">{{__('admin.home')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.product.index')}}">{{__('admin.product')}}</a></li>
                        <li class="breadcrumb-item active">{{__('admin.list')}}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <form class="list-table" method="get" action="{{route('admin.order-status.index')}}">
                        @if(false)
                        <div class="row">
                            <div class="col-12">
                                <section class="content">
                                    <div class="card {{config('custom.card-primary')}}">                                      

                                        <div class="card-footer">
                                            <div class="btn-toolbar float-right">
                                                <div class="btn-group">
                                                    <button type="submit" class="{{config('custom.btn-success-form')}}" title="{{__('admin.search')}}">{{__('admin.search')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>                       
                        @endif
                        <div class="card {{config('custom.card-primary')}}">
                            <div class="table-card card-header pl-0">
                                <div class="card-tools w-100">
                                    <div class="input-group input-group-sm float-right custom-search">
                                        <input type="text" name="q" class="form-control float-right float-sm-right" placeholder="{{__('admin.search')}}" value="{{isset($searchText)?$searchText:''}}">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default" title="{{__('admin.search')}}"><i class="fas fa-search"></i></button>
                                        </div>
                                        <a class="pl-2"  href="{{route('admin.order-status.add')}}"><button type="button" class="{{config('custom.btn-primary','btn btn-outline-primary')}} add-new-btn" title="{{__('admin.add')}}">{{__('admin.add')}}</button></a>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <table data-model="AdminOrderStatus" class="table normal-table table-striped">
                                <thead>
                                    <tr>
                                        <th class="asc-desc text-center" id="@if($sortKey=='id'){{$sortVal}}@endif">{{__('admin.first_column')}}<input @if($sortKey!='id')disabled=""@endif  type="hidden" name="id" value="@if($sortKey=='id'){{$sortVal}}@endif"></th>
                                        <th class="asc-desc" id="@if($sortKey=='product_id'){{$sortVal}}@endif">{{__('admin.product_name')}}<input @if($sortKey!='product_id')disabled=""@endif  type="hidden" name="product_id" value="@if($sortKey=='product_id'){{$sortVal}}@endif"></th>
                                        <th class=" asc-desc" id="@if($sortKey=='admin_id'){{$sortVal}}@endif">{{__('admin.customer_name')}}<input @if($sortKey!='admin_id')disabled=""@endif  type="hidden" name="admin_id" value="@if($sortKey=='admin_id'){{$sortVal}}@endif"></th>
                                        <th class=" asc-desc" id="@if($sortKey=='order_status'){{$sortVal}}@endif">{{__('admin.order_status')}}<input @if($sortKey!='order_status')disabled=""@endif  type="hidden" name="order_status" value="@if($sortKey=='order_status'){{$sortVal}}@endif"></th>
                                        <th class="text-center">{{__('admin.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($orderStatus as $record)
                                    <tr id="{{$record->id}}">
                                        <td class="text-center">{{ucfirst($record->id)}}</td>
                                        <?php
                                        $product  = $record->getProduct($record->product_id);
                                        $customer = $record->getCustomer($record->admin_id);
                                        ?>
                                        <td>{{$product?ucfirst($product->product_name):''}}</td>
                                        <td>{{ucfirst($customer->first_name.' '.$customer->last_name)}}</td>
                                        <td>{{$record?ucfirst($record->order_status):''}}</td>
                                        <td class="text-center">
                                            <a href="{{route('admin.order-status.edit',['id'=>$record->id])}}"><button type="button" class="{{config('custom.btn-primary','btn btn-outline-primary btn-sm')}}" title="{{__('admin.edit')}}"><i class="fas fa-edit"></i></button></a>
                                            <a onclick="showSweetAlert('{{route('admin.order-status.delete',['id'=>$record->id])}}')"><button type="button" class="{{config('custom.btn-danger','btn btn-outline-danger btn-sm')}}" title="{{__('admin.delete')}}"><i class="fas fa-trash"></i></button></a>
                                        </td>
                                    </tr>
                                    @empty
                                    @endforelse
                                </tbody>
                            </table>
                            <!-- /.card-body -->
                        </div>
                        <div class="table-pagination pt20 float-right">
                            {{$orderStatus->appends(request()->input())->links()}}
                        </div>
                    </form>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
@section('page_specific_js')
@include('common/sorting')
@include('layout/select2') 
@endsection
