@extends('layout/main')
@section('page_title',  trans('admin.product'))
@section('additional_css')
@endsection
@section('content')
@include('layout/toaster')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{__('admin.product')}}</h1>
                </div>
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{route('admin.home.index')}}">{{__('admin.home')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.product.index')}}">{{__('admin.product')}}</a></li>
                        <li class="breadcrumb-item active">{{__('admin.edit')}}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card {{config('custom.card-primary')}}">
                <div class="card-header">
                    <h3 class="card-title">{{__('admin.edit')}}</h3>
                </div>
                <form method="post" action="{{route('admin.product.update',['id' => $singleRecord->id])}}">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>{{__('admin.product_name')}}</label><span class="required"></span>
                                    <input name="product_name" type="text" class="form-control {{ $errors->has('product_name') ? 'is-invalid':'' }}" placeholder="{{__('admin.product_name')}}" value="{{old('product_name',$singleRecord->product_name)}}">
                                    @error('product_name')
                                    <div class="text-red">{{ $message }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label>{{__('admin.product_code')}}</label><span class="required"></span>
                                    <input class="form-control length-10" placeholder="{{__('admin.product_code')}}" name="product_code" type="text" value="{{old('product_code',$singleRecord->product_code)}}">
                                    @error('product_code')
                                    <div class="text-red">{{ $message }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label>{{__('admin.product_price')}}</label><span class="required"></span>
                                    <input class="form-control float-input length-10" placeholder="{{__('admin.product_price')}}" name="product_price" type="text" value="{{old('product_price',$singleRecord->product_price)}}">
                                    @error('product_price')
                                    <div class="text-red">{{ $message }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label>{{__('admin.product_quantity')}}</label><span class="required"></span>
                                    <input  class="form-control float-input length-10" placeholder="{{__('admin.product_quantity')}}" name="product_quantity" type="text" value="{{old('product_quantity',$singleRecord->product_quantity)}}">
                                    @error('product_quantity')
                                    <div class="text-red">{{ $message }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label>{{__('admin.product_description')}}</label>
                                    <textarea  class="textarea form-control {{ $errors->has('product_description') ? 'is-invalid':'' }}" placeholder="{{__('admin.product_description')}} " id="product_description" name="product_description" rows="4" cols="300">{{old('product_description',$singleRecord->product_description)}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <span class="float-left">{!!__('admin.additional_notes')!!}</span>
                        <div class="btn-toolbar float-right">
                            <div class="btn-group mr-2">
                                <a href="{{route('admin.product.index')}}" class="{{config('custom.btn-danger-form')}}" title="{{__('admin.cancel')}}">{{__('admin.cancel')}}</a>
                            </div>
                            <div class="btn-group">
                                <button type="submit" class="{{config('custom.btn-success-form')}}" title="{{__('admin.update')}}">{{__('admin.update')}}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@endsection
@section('page_specific_js')
@endsection