@extends('layout/main')
@section('page_title',  trans('admin.product'))
@section('additional_css')
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            @include('layout/toaster')
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{__('admin.product')}}</h1>
                </div>
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{route('admin.home.index')}}">{{__('admin.home')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.product.index')}}">{{__('admin.product')}}</a></li>
                        <li class="breadcrumb-item active">{{__('admin.list')}}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <form class="list-table" method="get" action="{{route('admin.product.index')}}">
                  
                        <div class="card {{config('custom.card-primary')}}">
                            <div class="table-card card-header pl-0">
                                <div class="card-tools w-100">
                                    <div class="input-group input-group-sm float-right custom-search">
                                        <input type="text" name="q" class="form-control float-right float-sm-right" placeholder="{{__('admin.search')}}" value="{{isset($searchText)?$searchText:''}}">

                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default" title="{{__('admin.search')}}"><i class="fas fa-search"></i></button>
                                        </div>
                                        <a class="pl-2"  href="{{route('admin.product.add')}}"><button type="button" class="{{config('custom.btn-primary','btn btn-outline-primary')}} add-new-btn" title="{{__('admin.add')}}">{{__('admin.add')}}</button></a>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <table data-model="AdminProduct" class="table normal-table table-striped">
                                <thead>
                                    <tr>
                                        <th class="asc-desc text-center" id="@if($sortKey=='id'){{$sortVal}}@endif">{{__('admin.first_column')}}<input @if($sortKey!='id')disabled=""@endif  type="hidden" name="id" value="@if($sortKey=='id'){{$sortVal}}@endif"></th>
                                        <th class="asc-desc" id="@if($sortKey=='product_name'){{$sortVal}}@endif">{{__('admin.product_name')}}<input @if($sortKey!='product_name')disabled=""@endif  type="hidden" name="product_name" value="@if($sortKey=='product_name'){{$sortVal}}@endif"></th>
                                        <th class="text-center asc-desc" id="@if($sortKey=='product_price'){{$sortVal}}@endif">{{__('admin.product_price')}}<input @if($sortKey!='product_price')disabled=""@endif  type="hidden" name="product_price" value="@if($sortKey=='product_price'){{$sortVal}}@endif"></th>
                                        <th class="text-center asc-desc" id="@if($sortKey=='product_quantity'){{$sortVal}}@endif">{{__('admin.product_quantity_index')}}<input @if($sortKey!='product_quantity')disabled=""@endif  type="hidden" name="product_quantity" value="@if($sortKey=='product_quantity'){{$sortVal}}@endif"></th>
                                        <th class="text-center">{{__('admin.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($product as $record)
                                    <tr id="{{$record->id}}">
                                        <td class="text-center">{{ucfirst($record->id)}}</td>
                                        <td>{{ucfirst($record->product_name)}}</td>
                                        <td class="text-center">{{$record->product_price}}</td>
                                        <td class="text-center">
                                            @if($record->product_quantity)
                                            {{$record->product_quantity}}
                                            @else
                                            <span class='badge badge-danger'>{{__('admin.out_of_stock')}}</span>
                                            @endif
                                        </td>

                                        <td class="text-center">
                                            <a href="{{route('admin.product.edit',['id'=>$record->id])}}"><button type="button" class="{{config('custom.btn-primary','btn btn-outline-primary btn-sm')}}" title="{{__('admin.edit')}}"><i class="fas fa-edit"></i></button></a>
                                            <a onclick="showSweetAlert('{{route('admin.product.delete',['id'=>$record->id])}}')"><button type="button" class="{{config('custom.btn-danger','btn btn-outline-danger btn-sm')}}" title="{{__('admin.delete')}}"><i class="fas fa-trash"></i></button></a>
                                        </td>
                                    </tr>
                                    @empty
                                    @endforelse
                                </tbody>
                            </table>
                            <!-- /.card-body -->
                        </div>
                        <div class="table-pagination pt20 float-right">
                            {{$product->appends(request()->input())->links()}}
                        </div>
                    </form>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
@section('page_specific_js')
@include('common/sorting')
@include('layout/sweetalert')
@include('layout/datatable')
@include('layout/select2') 
<script>
    $(document).ready(function ()
    {
    $(".select-category").select2({
    placeholder: "{{__('admin.filter_by_category')}}",
            allowClear: true
    });
    });
</script>
@endsection
