<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{__('admin.login')}}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
        <style>
            .page-logo
            {
                height: 100px;
                width: 100px;
            }
        </style>
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
         
            <div class="card">
                <div class="card-body login-card-body">
                    <p class="login-box-msg">{{__('admin.login_text')}}</p>
                    @include('layout/toaster')
                    @if($errors->has('email') || $errors->has('password'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{$errors->first('email')?$errors->first('email'):$errors->first('password')}}
                    </div>
                    @endif
                    <form action="{{route('admin.login')}}" method="post">
                        @csrf
                        <div class="input-group mb-3">
                            <input name="email" type="text" class="form-control {{ $errors->has('email') ?'is-invalid':'' }}" placeholder="{{__('admin.email_username')}}" value="{{old('email')}}">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input name="password" type="password" class="form-control {{ $errors->has('password') ?'is-invalid':'' }}" placeholder="{{__('admin.password')}}">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-8">
                                <p class="mt-2">
                                    <a></a>
                                </p>
                            </div>
                            <div class="col-4">
                                <button type="submit" class="{{config('custom.btn-primary-form')}} btn-block">{{__('admin.login')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('dist/js/adminlte.min.js')}}"></script>
    </body>
</html>
