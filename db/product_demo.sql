-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2021 at 10:01 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `product_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(192) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `first_name`, `last_name`, `email`, `gender`, `mobile`, `address`, `email_verified_at`, `password`, `remember_token`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'test', 'user', 'test@test.com', '1', '942706350', 'time squre,ahmedabad', NULL, '$2y$10$XkvJleuSY2FdDGpBkCcCSeJyfyynXDFbrvwcms.6Jwgkc7IShA0S2', NULL, 1, '2021-03-30 10:46:52', '2021-03-30 10:48:06', NULL, NULL, NULL, NULL),
(2, 'Admin', 'admin', 'admin@neogeninfotech.com', '', '', '', NULL, '$2y$10$XkMV0gKQyS/B71dDjhL17.6jBTMBBk3/sNXFbU32cPZpQ9XnvZK4G', NULL, 1, '2021-03-08 00:52:02', '2021-03-30 10:19:47', '2021-03-30 10:19:47', NULL, NULL, NULL),
(5, 'mansi', 'trivedi', 'demo@gmail.com', 'female', '1234567890', 'time square', NULL, '$2y$10$7aAerdGW9JwPhGUMOSo/f.OqFXgep/.mW5HVMuwPzAOR9cai9YJpq', NULL, 1, '2021-03-30 08:20:08', '2021-03-30 08:20:08', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_about_us`
--

CREATE TABLE `admin_about_us` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_about_us_page_description`
--

CREATE TABLE `admin_about_us_page_description` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_about_us_id` int(11) NOT NULL COMMENT 'FK = admin_about_us',
  `language_id` int(11) NOT NULL COMMENT 'FK = admin_languages',
  `note` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_blog`
--

CREATE TABLE `admin_blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `sorting` int(11) DEFAULT NULL,
  `categories_id` int(11) DEFAULT NULL,
  `blog_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_future` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_blog`
--

INSERT INTO `admin_blog` (`id`, `sorting`, `categories_id`, `blog_image`, `is_active`, `is_future`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 1, 'public/blog/image/JL9OnNs3X9Ttm4CX5hUkglcUKuNbqg3pIP4wVL2y.jpg', 1, 0, '2021-03-25 07:08:22', '2021-03-26 06:25:06', NULL, 2, 2, NULL),
(2, 3, 3, 'public/blog/image/ykdsVeczBtmdODfkhHgXHYremUh6VWMKjM1ajLpA.jpg', 1, 1, '2021-03-25 07:09:04', '2021-03-26 06:20:22', NULL, 2, 2, NULL),
(3, 2, 3, 'public/blog/image/6hqHG1vDKHmSmkVPKjmJ3OuQCuhXFb0JzrymKwOO.jpg', 1, 0, '2021-03-25 07:12:32', '2021-03-26 06:18:18', NULL, 2, 2, NULL),
(4, 4, 1, 'public/blog/image/3QpiLNsXW9YfTmoAm4T3xxqRufhcPSsQOY9NcSMV.jpg', 1, 0, '2021-03-25 07:13:55', '2021-03-26 06:20:06', '2021-03-26 06:20:06', 2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `admin_blog_description`
--

CREATE TABLE `admin_blog_description` (
  `id` int(10) UNSIGNED NOT NULL,
  `blog_id` int(11) NOT NULL DEFAULT 0 COMMENT 'FK = admin_blog',
  `language_id` int(11) NOT NULL COMMENT 'FK = admin_languages',
  `blog_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_blog_description`
--

INSERT INTO `admin_blog_description` (`id`, `blog_id`, `language_id`, `blog_name`, `blog_description`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 1, 'nayna1', 'news1', 1, '2021-03-25 07:08:22', '2021-03-25 07:22:21', NULL, 2, 2, NULL),
(2, 1, 6, 'nyana1', 'news1', 1, '2021-03-25 07:08:22', '2021-03-25 07:22:21', NULL, 2, 2, NULL),
(3, 2, 1, 'Data NEws123', NULL, 1, '2021-03-25 07:09:04', '2021-03-25 07:09:04', NULL, 2, NULL, NULL),
(4, 2, 6, 'Data NEws123', NULL, 1, '2021-03-25 07:09:04', '2021-03-25 07:09:04', NULL, 2, NULL, NULL),
(5, 3, 1, 'nesw', NULL, 1, '2021-03-25 07:12:32', '2021-03-25 07:12:32', NULL, 2, NULL, NULL),
(6, 3, 6, 'sdsf', NULL, 1, '2021-03-25 07:12:32', '2021-03-25 07:12:32', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_brand`
--

CREATE TABLE `admin_brand` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sorting` int(11) NOT NULL,
  `brand_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_brand`
--

INSERT INTO `admin_brand` (`id`, `sorting`, `brand_slug`, `brand_logo`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 0, 'paytm-1', 'public/product/brand/v0s2aPlfqON9zvdWqJB4RGFRJfhmUmpkYgGw77PU.jpg', 0, '2021-03-15 07:12:38', '2021-03-15 23:44:44', '2021-03-15 23:44:44', 2, 2, 2),
(2, 0, 'brand-hindi', 'public/product/brand/RwVxXKmwwoF9VfJt0Duj5mV2NhFI4pdfVnGTZDsr.jpg', 1, '2021-03-15 07:19:04', '2021-03-15 23:44:50', '2021-03-15 23:44:50', 2, 2, 2),
(3, 2, 'english-1', 'public/product/brand/Dxv75m0d8NrZW72zghtiK6XovF5SrE5br5QKIoho.jpg', 0, '2021-03-15 23:44:02', '2021-03-26 05:22:54', NULL, 2, 2, NULL),
(4, 3, 'card-1', 'public/product/brand/fWVoVzh8Vx5ksPQSes9UJtnRoDnRJKfvlQ2pCtwW.jpg', 1, '2021-03-16 00:45:45', '2021-03-26 05:22:54', NULL, 2, 2, NULL),
(5, 1, 'test', 'public/product/brand/Op8qRjMUXNcQRnuNK6tK8bomsV640O7bRceY0Rfd.jpg', 1, '2021-03-16 01:04:59', '2021-03-26 05:09:27', NULL, 2, 2, NULL),
(6, 4, 'test-1-1', 'public/product/brand/0KKTdesOhaT5IhaFKdzP265IAiuumBGTOrE1srjw.jpg', 1, '2021-03-26 05:23:16', '2021-03-26 05:23:16', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_brand_description`
--

CREATE TABLE `admin_brand_description` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `brand_id` int(11) DEFAULT NULL COMMENT 'FK = admin_brand',
  `language_id` int(11) NOT NULL COMMENT 'FK = admin_languages',
  `brand_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_brand_description`
--

INSERT INTO `admin_brand_description` (`id`, `brand_id`, `language_id`, `brand_name`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 1, 'Paytm', 1, '2021-03-15 07:12:38', '2021-03-15 23:44:44', '2021-03-15 23:44:44', 2, 2, 2),
(2, 1, 6, 'Paytm', 1, '2021-03-15 07:12:38', '2021-03-15 23:44:44', '2021-03-15 23:44:44', 2, 2, 2),
(3, 1, 9, 'Paytm', 1, '2021-03-15 07:12:38', '2021-03-15 23:44:44', '2021-03-15 23:44:44', 2, 2, 2),
(4, 1, 10, 'Paytm', 1, '2021-03-15 07:12:38', '2021-03-15 23:44:44', '2021-03-15 23:44:44', 2, 2, 2),
(5, 2, 1, 'brand_en', 1, '2021-03-15 07:19:04', '2021-03-15 23:44:50', '2021-03-15 23:44:50', 2, 2, 2),
(6, 2, 6, 'brand_jp', 1, '2021-03-15 07:19:04', '2021-03-15 23:44:50', '2021-03-15 23:44:50', 2, 2, 2),
(7, 2, 9, 'brand_french', 1, '2021-03-15 07:19:04', '2021-03-15 23:44:50', '2021-03-15 23:44:50', 2, 2, 2),
(8, 2, 10, 'brand_hindi', 1, '2021-03-15 07:19:04', '2021-03-15 23:44:50', '2021-03-15 23:44:50', 2, 2, 2),
(9, 3, 1, 'english', 1, '2021-03-15 23:44:02', '2021-03-18 05:09:45', NULL, 2, 2, NULL),
(10, 3, 6, 'english', 1, '2021-03-15 23:44:02', '2021-03-18 05:09:45', NULL, 2, 2, NULL),
(11, 3, 9, 'English', 1, '2021-03-15 23:44:03', '2021-03-15 23:44:03', NULL, 2, NULL, NULL),
(12, 3, 10, 'English', 1, '2021-03-15 23:44:03', '2021-03-15 23:44:03', NULL, 2, NULL, NULL),
(13, 4, 1, 'Card', 1, '2021-03-16 00:45:45', '2021-03-16 00:48:45', NULL, 2, 2, NULL),
(14, 4, 6, 'Card', 1, '2021-03-16 00:45:45', '2021-03-16 00:48:45', NULL, 2, 2, NULL),
(15, 4, 9, 'Card', 1, '2021-03-16 00:45:45', '2021-03-16 00:48:45', NULL, 2, 2, NULL),
(16, 4, 10, 'Card', 1, '2021-03-16 00:45:45', '2021-03-16 00:48:45', NULL, 2, 2, NULL),
(17, 5, 1, 'test', 1, '2021-03-16 01:04:59', '2021-03-16 01:04:59', NULL, 2, NULL, NULL),
(18, 5, 6, 'test', 1, '2021-03-16 01:04:59', '2021-03-16 01:04:59', NULL, 2, NULL, NULL),
(19, 5, 9, 'tes', 1, '2021-03-16 01:04:59', '2021-03-16 01:04:59', NULL, 2, NULL, NULL),
(20, 5, 10, 'test', 1, '2021-03-16 01:04:59', '2021-03-16 01:04:59', NULL, 2, NULL, NULL),
(21, 6, 1, 'test 1', 1, '2021-03-26 05:23:16', '2021-03-26 05:23:16', NULL, 2, NULL, NULL),
(22, 6, 6, 'test 1', 1, '2021-03-26 05:23:16', '2021-03-26 05:23:16', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_category`
--

CREATE TABLE `admin_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `sorting` int(11) DEFAULT NULL,
  `categories_slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_category`
--

INSERT INTO `admin_category` (`id`, `parent_id`, `sorting`, `categories_slug`, `category_image`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 1, 'test', 'public/product-category/irctov5hGAKO7urWka4RxsqAIkBA1z52ZiXegAhr.jpg', 0, '2021-03-23 02:20:16', '2021-03-26 05:29:53', NULL, 2, 2, NULL),
(2, 2, 2, 'demo', 'public/product-category/uKuLlMxkLlnmIK1vEusFjE6tWOnqyZZvOBfsjNr9.jpg', 1, '2021-03-23 02:20:30', '2021-03-23 02:20:30', NULL, 2, 2, NULL),
(3, 4, 1, 'sdf', 'public/product-category/LTANPrW5NM6vOR4j2Zr5RsfhBZRtCtacijTba1UM.jpg', 1, '2021-03-23 03:38:12', '2021-03-23 03:38:12', NULL, 2, 2, NULL),
(4, 0, 1, 'sdv-1', 'public/product-category/e2HBdYLyhCfesmMwvsPrhoSfP0K7txsvG6u9SWIz.jpg', 1, '2021-03-23 03:38:21', '2021-03-26 04:06:07', '2021-03-26 04:06:07', 2, 2, 2),
(5, 0, 3, '123', 'public/product-category/jfG64fHygNpOBQDpwhZwtVIiJlbXuW4N24ldA89O.jpg', 1, '2021-03-23 03:48:22', '2021-03-26 04:06:07', NULL, 2, 2, NULL),
(6, 0, 2, 'demo123-1', 'public/product-category/OhmJlfv8WxndpRF3KMfe9JiKbtZ9IPBoruX3xtqi.jpg', 1, '2021-03-23 03:48:51', '2021-03-26 05:01:39', NULL, 2, 2, NULL),
(7, 5, 3, '123-acrylic', NULL, 0, '2021-03-26 05:10:03', '2021-03-26 05:29:42', NULL, 2, 2, NULL),
(8, 6, 4, 'demo123-1-test', NULL, 1, '2021-03-26 05:14:57', '2021-03-26 05:15:12', NULL, 2, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_category_description`
--

CREATE TABLE `admin_category_description` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT 0 COMMENT 'FK = admin_category',
  `language_id` int(11) NOT NULL COMMENT 'FK = admin_languages',
  `category_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_category_description`
--

INSERT INTO `admin_category_description` (`id`, `category_id`, `language_id`, `category_name`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 1, 'test', 1, '2021-03-23 02:20:16', '2021-03-23 02:20:16', NULL, 2, NULL, NULL),
(2, 1, 6, 'test', 1, '2021-03-23 02:20:16', '2021-03-23 02:20:16', NULL, 2, NULL, NULL),
(3, 2, 1, 'demo', 1, '2021-03-23 02:20:30', '2021-03-23 02:20:30', NULL, 2, NULL, NULL),
(4, 2, 6, 'demo', 1, '2021-03-23 02:20:30', '2021-03-23 02:20:30', NULL, 2, NULL, NULL),
(5, 3, 1, 'SDF', 1, '2021-03-23 03:38:12', '2021-03-23 03:38:12', NULL, 2, NULL, NULL),
(6, 3, 6, 'SDCV', 1, '2021-03-23 03:38:12', '2021-03-23 03:38:12', NULL, 2, NULL, NULL),
(7, 4, 1, 'ASD', 1, '2021-03-23 03:38:21', '2021-03-26 04:06:07', '2021-03-26 04:06:07', 2, 2, 2),
(8, 4, 6, 'SDV', 1, '2021-03-23 03:38:21', '2021-03-26 04:06:07', '2021-03-26 04:06:07', 2, 2, 2),
(9, 5, 1, 'ASD1', 1, '2021-03-23 03:48:22', '2021-03-26 04:04:41', NULL, 2, 2, NULL),
(10, 5, 6, '123', 1, '2021-03-23 03:48:22', '2021-03-23 03:48:22', NULL, 2, NULL, NULL),
(11, 6, 1, 'demo123', 1, '2021-03-23 03:48:51', '2021-03-23 03:48:51', NULL, 2, NULL, NULL),
(12, 6, 6, 'demo123', 1, '2021-03-23 03:48:51', '2021-03-23 03:48:51', NULL, 2, NULL, NULL),
(13, 7, 1, 'Acrylic', 1, '2021-03-26 05:10:03', '2021-03-26 05:10:03', NULL, 2, NULL, NULL),
(14, 7, 6, 'xvzvc', 1, '2021-03-26 05:10:03', '2021-03-26 05:10:03', NULL, 2, NULL, NULL),
(15, 8, 1, 'test', 1, '2021-03-26 05:14:57', '2021-03-26 05:14:57', NULL, 2, NULL, NULL),
(16, 8, 6, 'test', 1, '2021-03-26 05:14:57', '2021-03-26 05:14:57', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_city`
--

CREATE TABLE `admin_city` (
  `id` int(10) UNSIGNED NOT NULL,
  `sorting` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL COMMENT 'FK = admin_country',
  `is_default` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_city`
--

INSERT INTO `admin_city` (`id`, `sorting`, `country_id`, `is_default`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 0, 3, '0', 0, '2021-03-08 02:17:15', '2021-03-08 07:39:20', '2021-03-08 07:39:20', 2, 2, 2),
(2, 3, 3, '0', 1, '2021-03-08 04:12:05', '2021-03-26 03:39:14', NULL, 2, 2, NULL),
(3, 2, 8, '0', 1, '2021-03-08 07:34:54', '2021-03-08 07:39:25', '2021-03-08 07:39:25', 2, 2, 2),
(4, 1, 14, '0', 1, '2021-03-10 00:37:14', '2021-03-26 03:46:15', NULL, 2, 2, NULL),
(5, 3, 3, '0', 1, '2021-03-11 06:09:54', '2021-03-11 06:10:29', '2021-03-11 06:10:29', 2, 2, 2),
(6, 2, 3, '0', 1, '2021-03-26 03:35:28', '2021-03-26 03:39:14', NULL, 2, 2, NULL),
(7, 4, 17, '0', 1, '2021-03-26 03:41:24', '2021-03-26 03:41:24', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_city_description`
--

CREATE TABLE `admin_city_description` (
  `id` int(10) UNSIGNED NOT NULL,
  `city_id` int(11) NOT NULL DEFAULT 0 COMMENT 'FK = admin_city',
  `language_id` int(11) NOT NULL COMMENT 'FK = admin_languages',
  `city_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_city_description`
--

INSERT INTO `admin_city_description` (`id`, `city_id`, `language_id`, `city_name`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 1, 'Ahmedabad', 1, '2021-03-08 02:17:15', '2021-03-08 07:39:20', '2021-03-08 07:39:20', 2, 2, 2),
(2, 1, 2, 'アーメダバード', 1, '2021-03-08 02:17:15', '2021-03-08 07:39:20', '2021-03-08 07:39:20', 2, 2, 2),
(3, 2, 1, 'gandhinagar', 1, '2021-03-08 04:12:05', '2021-03-08 04:12:05', NULL, 2, NULL, NULL),
(4, 2, 2, 'Gandhinagar', 1, '2021-03-08 04:12:05', '2021-03-08 04:12:05', NULL, 2, NULL, NULL),
(5, 3, 1, 'up', 1, '2021-03-08 07:34:54', '2021-03-08 07:39:25', '2021-03-08 07:39:25', 2, 2, 2),
(6, 2, 6, 'g j', 1, '2021-03-09 01:25:20', '2021-03-09 01:25:20', NULL, 2, NULL, NULL),
(7, 4, 1, 'Ahemedabad1', 1, '2021-03-10 00:37:14', '2021-03-26 03:45:53', NULL, 2, 2, NULL),
(8, 4, 6, 'japan2', 1, '2021-03-10 00:37:14', '2021-03-26 03:45:53', NULL, 2, 2, NULL),
(9, 4, 9, 'japan', 1, '2021-03-10 00:37:14', '2021-03-10 00:37:14', NULL, 2, NULL, NULL),
(10, 5, 1, 'Surat1', 1, '2021-03-11 06:09:54', '2021-03-11 06:10:29', '2021-03-11 06:10:29', 2, 2, 2),
(11, 5, 6, 'Surat1', 1, '2021-03-11 06:09:54', '2021-03-11 06:10:29', '2021-03-11 06:10:29', 2, 2, 2),
(12, 5, 9, 'Surat1', 1, '2021-03-11 06:09:54', '2021-03-11 06:10:29', '2021-03-11 06:10:29', 2, 2, 2),
(13, 5, 10, 'Surat1', 1, '2021-03-11 06:09:54', '2021-03-11 06:10:29', '2021-03-11 06:10:29', 2, 2, 2),
(14, 6, 1, 'surat', 1, '2021-03-26 03:35:28', '2021-03-26 03:35:28', NULL, 2, NULL, NULL),
(15, 6, 6, 'surat', 1, '2021-03-26 03:35:28', '2021-03-26 03:35:28', NULL, 2, NULL, NULL),
(16, 7, 1, 'test', 1, '2021-03-26 03:41:24', '2021-03-26 03:41:24', NULL, 2, NULL, NULL),
(17, 7, 6, 'test', 1, '2021-03-26 03:41:24', '2021-03-26 03:41:24', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_contact_us`
--

CREATE TABLE `admin_contact_us` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_country`
--

CREATE TABLE `admin_country` (
  `id` int(10) UNSIGNED NOT NULL,
  `sorting` int(11) DEFAULT NULL,
  `is_default` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_country`
--

INSERT INTO `admin_country` (`id`, `sorting`, `is_default`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, '0', 1, '2021-03-08 01:47:00', '2021-03-08 04:46:37', '2021-03-08 04:46:37', 2, 2, 2),
(2, 0, '0', 1, '2021-03-08 01:47:06', '2021-03-08 04:46:39', '2021-03-08 04:46:39', 2, 2, 2),
(3, 1, '0', 1, '2021-03-08 04:46:24', '2021-03-26 03:39:07', NULL, 2, 2, NULL),
(4, 2, '0', 1, '2021-03-08 07:10:30', '2021-03-08 07:12:44', '2021-03-08 07:12:44', 2, 2, 2),
(5, 2, '0', 1, '2021-03-08 07:22:50', '2021-03-08 07:22:55', '2021-03-08 07:22:55', 2, 2, 2),
(6, 2, '0', 1, '2021-03-08 07:28:47', '2021-03-08 07:31:27', '2021-03-08 07:31:27', 2, 2, 2),
(7, 2, '0', 1, '2021-03-08 07:31:41', '2021-03-08 07:31:52', '2021-03-08 07:31:52', 2, 2, 2),
(8, 1, '0', 1, '2021-03-08 07:34:42', '2021-03-09 00:52:21', '2021-03-09 00:52:21', 2, 2, 2),
(9, 3, '0', 1, '2021-03-08 07:45:08', '2021-03-08 07:45:18', '2021-03-08 07:45:18', 2, 2, 2),
(10, 0, '0', 1, '2021-03-09 00:50:21', '2021-03-09 00:52:25', '2021-03-09 00:52:25', 2, 2, 2),
(11, 0, '0', 0, '2021-03-09 00:53:59', '2021-03-09 01:17:26', '2021-03-09 01:17:26', 2, 2, 2),
(12, 2, '0', 0, '2021-03-09 01:17:48', '2021-03-09 01:18:02', '2021-03-09 01:18:02', 2, 2, 2),
(13, 2, '0', 1, '2021-03-09 01:18:09', '2021-03-09 01:38:41', '2021-03-09 01:38:41', 2, 2, 2),
(14, 2, '0', 1, '2021-03-09 01:38:51', '2021-03-26 03:39:07', NULL, 2, 2, NULL),
(15, 3, '0', 1, '2021-03-10 02:04:26', '2021-03-11 06:10:39', '2021-03-11 06:10:39', 2, 2, 2),
(16, 3, '0', 1, '2021-03-26 03:28:57', '2021-03-26 03:31:47', '2021-03-26 03:31:47', 2, 2, 2),
(17, 3, '0', 1, '2021-03-26 03:39:16', '2021-03-26 03:39:16', NULL, 2, NULL, NULL),
(18, 4, '0', 1, '2021-03-26 03:40:49', '2021-03-26 03:40:58', '2021-03-26 03:40:58', 2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `admin_country_description`
--

CREATE TABLE `admin_country_description` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT 0 COMMENT 'FK = admin_country',
  `language_id` int(11) NOT NULL COMMENT 'FK = admin_languages',
  `country_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_country_description`
--

INSERT INTO `admin_country_description` (`id`, `country_id`, `language_id`, `country_name`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 1, 'India', 1, '2021-03-08 01:47:00', '2021-03-08 04:46:37', '2021-03-08 04:46:37', 2, 2, 2),
(3, 3, 1, 'India', 1, '2021-03-08 04:46:24', '2021-03-18 03:57:03', NULL, 2, 2, NULL),
(6, 5, 1, 'Kuwait', 1, '2021-03-08 07:22:50', '2021-03-08 07:22:50', NULL, 2, NULL, NULL),
(7, 6, 1, 'Japan1', 1, '2021-03-08 07:28:47', '2021-03-08 07:31:27', '2021-03-08 07:31:27', 2, 2, 2),
(8, 7, 1, 'Japan1', 1, '2021-03-08 07:31:41', '2021-03-08 07:31:52', '2021-03-08 07:31:52', 2, 2, 2),
(17, 12, 1, 'Japan', 1, '2021-03-09 01:17:49', '2021-03-09 01:18:02', '2021-03-09 01:18:02', 2, 2, 2),
(18, 12, 6, 'Japan japanese', 1, '2021-03-09 01:17:49', '2021-03-09 01:18:02', '2021-03-09 01:18:02', 2, 2, 2),
(19, 13, 1, 'Japan', 1, '2021-03-09 01:18:09', '2021-03-09 01:38:41', '2021-03-09 01:38:41', 2, 2, 2),
(20, 13, 6, 'Japan japanese', 1, '2021-03-09 01:18:09', '2021-03-09 01:38:41', '2021-03-09 01:38:41', 2, 2, 2),
(21, 13, 8, 'japan hindi', 1, '2021-03-09 01:19:21', '2021-03-09 01:38:41', '2021-03-09 01:38:41', 2, 2, 2),
(22, 14, 1, 'Japan', 1, '2021-03-09 01:38:51', '2021-03-09 01:38:51', NULL, 2, NULL, NULL),
(23, 14, 6, 'Japan japanese', 1, '2021-03-09 01:38:51', '2021-03-09 01:38:51', NULL, 2, NULL, NULL),
(24, 15, 1, 'Srilanka', 1, '2021-03-10 02:04:26', '2021-03-11 06:10:39', '2021-03-11 06:10:39', 2, 2, 2),
(25, 15, 6, 'Srilanka', 1, '2021-03-10 02:04:26', '2021-03-11 06:10:39', '2021-03-11 06:10:39', 2, 2, 2),
(26, 15, 9, 'Srilanka', 1, '2021-03-10 02:04:26', '2021-03-11 06:10:39', '2021-03-11 06:10:39', 2, 2, 2),
(27, 3, 6, 'India', 1, '2021-03-18 03:56:36', '2021-03-18 03:57:03', NULL, 2, 2, NULL),
(28, 16, 1, 'Australia', 1, '2021-03-26 03:28:57', '2021-03-26 03:31:47', '2021-03-26 03:31:47', 2, 2, 2),
(29, 16, 6, 'Australia', 1, '2021-03-26 03:28:57', '2021-03-26 03:31:47', '2021-03-26 03:31:47', 2, 2, 2),
(30, 17, 1, 'Australia', 1, '2021-03-26 03:39:16', '2021-03-26 03:39:16', NULL, 2, NULL, NULL),
(31, 17, 6, 'Australia', 1, '2021-03-26 03:39:16', '2021-03-26 03:39:16', NULL, 2, NULL, NULL),
(32, 18, 1, 'America', 1, '2021-03-26 03:40:49', '2021-03-26 03:40:58', '2021-03-26 03:40:58', 2, 2, 2),
(33, 18, 6, 'America', 1, '2021-03-26 03:40:49', '2021-03-26 03:40:58', '2021-03-26 03:40:58', 2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `admin_coupons`
--

CREATE TABLE `admin_coupons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `coupons` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_type_id` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'FK = admin_discount_type',
  `coupon_amount` int(11) DEFAULT NULL,
  `coupon_start_date` date DEFAULT NULL,
  `coupon_expiry_date` date DEFAULT NULL,
  `minimum_spend` int(11) DEFAULT NULL,
  `maximum_spend` int(11) DEFAULT NULL,
  `allow_free_shipping` tinyint(1) DEFAULT NULL,
  `individual_use_only` tinyint(1) DEFAULT NULL,
  `exclude_sale_items` tinyint(1) DEFAULT NULL,
  `products` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exclude_products` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories_id` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'FK = admin_category',
  `exclude_categories` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_restrictions` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usage_limit_per_coupon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usage_limit_per_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_coupons`
--

INSERT INTO `admin_coupons` (`id`, `coupons`, `description`, `discount_type_id`, `coupon_amount`, `coupon_start_date`, `coupon_expiry_date`, `minimum_spend`, `maximum_spend`, `allow_free_shipping`, `individual_use_only`, `exclude_sale_items`, `products`, `exclude_products`, `categories_id`, `exclude_categories`, `email_restrictions`, `usage_limit_per_coupon`, `usage_limit_per_user`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'Loot 10', 'IT', '1,2,3', 100, '2021-03-23', '2021-03-23', 20, 80, 0, 0, 0, NULL, NULL, '4,5', '6', NULL, NULL, NULL, 1, '2021-03-23 08:13:32', '2021-03-23 08:13:47', NULL, 2, 2, NULL),
(2, 'test', 'data', '1,2,3', 100, '2021-03-26', '2021-03-27', 10, 20, 1, 1, 0, NULL, NULL, '5', '6', NULL, NULL, NULL, 1, '2021-03-26 05:35:46', '2021-03-26 05:40:21', NULL, 2, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_coupon_discount_categories`
--

CREATE TABLE `admin_coupon_discount_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `coupon_id` int(11) DEFAULT NULL COMMENT 'FK = admin_coupon',
  `reference_id` int(11) DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories_type` tinyint(1) DEFAULT NULL COMMENT '"1 = IncludeCategory", "0 = ExcludeCategory"',
  `discount_type` enum('cart discount','cart % discount','product discount','product % discount') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_coupon_discount_categories`
--

INSERT INTO `admin_coupon_discount_categories` (`id`, `coupon_id`, `reference_id`, `type`, `categories_type`, `discount_type`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 2, 3, 'discount', NULL, NULL, 1, '2021-03-23 08:13:32', '2021-03-26 05:40:21', NULL, 2, 2, NULL),
(2, 1, 2, 'discount', NULL, NULL, 1, '2021-03-23 08:13:32', '2021-03-23 08:13:32', NULL, 2, NULL, NULL),
(3, 1, 4, 'category', 1, NULL, 1, '2021-03-23 08:13:32', '2021-03-23 08:13:32', NULL, 2, NULL, NULL),
(4, 1, 5, 'category', 1, NULL, 1, '2021-03-23 08:13:32', '2021-03-23 08:13:32', NULL, 2, NULL, NULL),
(5, 1, 6, 'category', 0, NULL, 1, '2021-03-23 08:13:32', '2021-03-23 08:13:32', NULL, 2, NULL, NULL),
(6, 2, 1, 'discount', NULL, NULL, 1, '2021-03-26 05:35:46', '2021-03-26 05:35:46', NULL, 2, NULL, NULL),
(7, 2, 2, 'discount', NULL, NULL, 1, '2021-03-26 05:35:46', '2021-03-26 05:35:46', NULL, 2, NULL, NULL),
(8, 2, 5, 'category', 1, NULL, 1, '2021-03-26 05:35:46', '2021-03-26 05:35:46', NULL, 2, NULL, NULL),
(9, 2, 6, 'category', 0, NULL, 1, '2021-03-26 05:35:46', '2021-03-26 05:35:46', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_courier_company`
--

CREATE TABLE `admin_courier_company` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_courier_company`
--

INSERT INTO `admin_courier_company` (`id`, `company_name`, `link`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'ABCD', 'Https://stackoverflow.com/ques', 1, '2021-03-09 05:16:26', '2021-03-26 03:47:34', NULL, 2, 2, NULL),
(8, 'Test', 'http://www.fao.org/countryprofiles/iso3list/en/', 1, '2021-03-26 03:46:58', '2021-03-26 03:47:25', NULL, 2, 2, NULL),
(9, 'DElhivery2', 'Https://www.dhl.com/en/express/tracking.html?AWB=ASDFASDF&brand=DHL1', 1, '2021-03-26 03:59:20', '2021-03-26 04:00:09', '2021-03-26 04:00:09', 2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `admin_currencies`
--

CREATE TABLE `admin_currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol_left` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `symbol_right` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'FK = sadmin_countries',
  `position` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Left/Right/Left with Space/Right with Space',
  `decimal_point` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thousands_seperator` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `decimal_seperator` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` double(13,8) DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT 0,
  `is_current` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_currencies`
--

INSERT INTO `admin_currencies` (`id`, `title`, `code`, `symbol_left`, `symbol_right`, `country_id`, `position`, `decimal_point`, `thousands_seperator`, `decimal_seperator`, `value`, `is_default`, `is_current`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'Indian rupee', 'INR', '$', 'Rs', 3, '1', '1', ',', '@', 2.00000000, 1, 0, 1, '2021-03-09 00:04:31', '2021-03-09 00:15:04', '2021-03-09 00:15:04', 2, 2, 2),
(2, 'Saudi riyal', 'SAR', '﷼', 'س', 8, '2', 'ر', ',', '1', 3.00000000, 0, 0, 1, '2021-03-09 00:13:33', '2021-03-09 00:15:00', '2021-03-09 00:15:00', 2, 2, 2),
(3, 'Indian rupee1', 'INS', '₹', '$', 3, '1', '3', '$', ',', 2.00000000, 0, 0, 1, '2021-03-09 00:18:45', '2021-03-26 03:46:10', NULL, 2, 2, NULL),
(4, 'Saudi Riyal', 'SAR', '﷼', 'س', 11, '2', '1', '.', '1', 2.00000000, 0, 0, 1, '2021-03-09 00:20:10', '2021-03-09 00:57:29', '2021-03-09 00:57:29', 2, 2, 2),
(5, 'wer', '23', 'as', 'sd', 3, 'asd', 'a', 'a', 's', 2.00000000, 0, 0, 0, '2021-03-09 00:20:29', '2021-03-09 00:20:51', '2021-03-09 00:20:51', 2, 2, 2),
(15, 'Indian', 'INR', 'dfg', 'fgh', 3, 'fg', 'f', 'f', 'f', 234.00000000, 0, 0, 0, '2021-03-09 01:30:21', '2021-03-26 03:46:19', NULL, 2, 2, NULL),
(16, 'asd', 'sd', 'sdc', 'sdc', NULL, 'sdc', 'x', 's', 's', 123.00000000, 0, 0, 1, '2021-03-09 02:10:51', '2021-03-09 04:04:41', '2021-03-09 04:04:41', 2, 2, 2),
(17, 'test', 'code', 'aa', 'bb', NULL, 'a', 'a', ',', '.', 500.00000000, 0, 0, 0, '2021-03-26 03:54:45', '2021-03-26 03:57:49', '2021-03-26 03:57:49', 2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `admin_discount_type`
--

CREATE TABLE `admin_discount_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `discount_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_discount_type`
--

INSERT INTO `admin_discount_type` (`id`, `discount_type`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'Cart Discount', 1, '2021-03-16 02:29:33', '2021-03-16 02:29:33', NULL, NULL, NULL, NULL),
(2, 'Cart % Discount', 1, '2021-03-16 02:29:33', '2021-03-16 02:29:33', NULL, NULL, NULL, NULL),
(3, 'Product Discount', 1, '2021-03-16 02:29:33', '2021-03-16 02:29:33', NULL, NULL, NULL, NULL),
(4, 'Product % Discount', 1, '2021-03-16 02:29:33', '2021-03-16 02:29:33', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_languages`
--

CREATE TABLE `admin_languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` char(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direction` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'LTR / RTL',
  `is_default` tinyint(1) DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_languages`
--

INSERT INTO `admin_languages` (`id`, `name`, `code`, `image`, `direction`, `is_default`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'english', 'en', 'public/language/baKxDYpZrWIbtPPDNuBNBkE7WVYWn1NilYezxpSx.jpg', 'rtl', 1, 1, '2021-03-08 00:52:07', '2021-03-26 05:00:53', NULL, NULL, NULL, NULL),
(6, 'japanese', 'jj', 'public/language/1xTWrdmcfjBysPLFpSl45dhT2c3OMBg9pCKVkm9F.jpg', 'rtl', 0, 1, '2021-03-08 07:37:38', '2021-03-26 05:04:04', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_media_setting`
--

CREATE TABLE `admin_media_setting` (
  `id` int(10) UNSIGNED NOT NULL,
  `sorting` int(11) NOT NULL,
  `setting_key` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_multi_lang` tinyint(1) NOT NULL DEFAULT 0,
  `img_height` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_width` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_require` tinyint(1) NOT NULL DEFAULT 0,
  `can_edit` tinyint(1) NOT NULL DEFAULT 1,
  `validation` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_media_setting`
--

INSERT INTO `admin_media_setting` (`id`, `sorting`, `setting_key`, `description`, `is_multi_lang`, `img_height`, `img_width`, `img_size`, `is_require`, `can_edit`, `validation`, `type`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 'Product Image', 'Product Dimension', 0, '1280', '1601', '200', 0, 1, '', 'text', 1, '2021-03-23 05:34:57', '2021-03-26 04:42:09', NULL, NULL, 2, NULL),
(2, 2, 'Logo Image', 'Logo Image Dimension', 0, '16', '16', '100', 1, 1, '', 'text', 1, '2021-03-23 05:34:57', '2021-03-23 05:34:57', NULL, NULL, NULL, NULL),
(3, 3, 'Category Image', 'Category Image Dimension', 0, '129', '98', '10', 1, 1, '', 'text', 1, '2021-03-23 05:34:57', '2021-03-23 05:34:57', NULL, NULL, NULL, NULL),
(4, 4, 'Brand Image', 'Brand Image Dimension', 0, '129', '98', '10', 1, 1, '', 'text', 1, '2021-03-23 05:34:57', '2021-03-26 05:22:22', NULL, NULL, 2, NULL),
(5, 5, 'Blog Image', 'Blog Image Dimension', 0, '123', '165', '10', 1, 1, '', 'text', 1, '2021-03-23 05:34:57', '2021-03-23 05:42:43', NULL, NULL, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_news_categories`
--

CREATE TABLE `admin_news_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `sorting` int(11) DEFAULT NULL,
  `news_category_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_news_categories`
--

INSERT INTO `admin_news_categories` (`id`, `sorting`, `news_category_image`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, NULL, 1, '2021-03-26 00:20:56', '2021-03-26 00:20:56', NULL, 2, NULL, NULL),
(2, 2, NULL, 1, '2021-03-26 00:21:14', '2021-03-26 00:21:14', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_news_categories_description`
--

CREATE TABLE `admin_news_categories_description` (
  `id` int(10) UNSIGNED NOT NULL,
  `news_categories_id` int(11) NOT NULL DEFAULT 0 COMMENT 'FK = admin_news_categories',
  `language_id` int(11) NOT NULL COMMENT 'FK = admin_languages',
  `news_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_news_categories_description`
--

INSERT INTO `admin_news_categories_description` (`id`, `news_categories_id`, `language_id`, `news_name`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 1, 'Data', 1, '2021-03-26 00:20:56', '2021-03-26 00:29:20', NULL, 2, 2, NULL),
(2, 1, 6, 'test2', 1, '2021-03-26 00:20:56', '2021-03-26 00:20:56', NULL, 2, NULL, NULL),
(3, 2, 1, 'Data', 1, '2021-03-26 00:21:14', '2021-03-26 00:29:26', NULL, 2, 2, NULL),
(4, 2, 6, 'Data2', 1, '2021-03-26 00:21:14', '2021-03-26 00:21:14', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_order_status`
--

CREATE TABLE `admin_order_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `sorting` int(11) DEFAULT NULL,
  `order_status` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_id` int(125) NOT NULL,
  `product_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_order_status`
--

INSERT INTO `admin_order_status` (`id`, `sorting`, `order_status`, `admin_id`, `product_id`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, NULL, 'pending1', 5, 2, 1, '2021-03-30 13:38:11', '2021-03-30 13:49:07', NULL, 5, 5, NULL),
(2, NULL, 'shipping', 1, 1, 1, '2021-03-30 13:39:42', '2021-03-30 13:49:47', '2021-03-30 13:49:47', 5, 5, 5),
(3, NULL, 'shipping', 1, 1, 1, '2021-03-30 13:59:48', '2021-03-30 13:59:48', NULL, 5, NULL, NULL),
(4, NULL, 'Shipped', 1, 1, 1, '2021-03-30 14:03:59', '2021-03-30 14:03:59', NULL, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_payment_method`
--

CREATE TABLE `admin_payment_method` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sorting` int(11) NOT NULL,
  `environment` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_payment_method`
--

INSERT INTO `admin_payment_method` (`id`, `sorting`, `environment`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 1, 1, '2021-03-11 06:37:10', '2021-03-11 06:37:10', NULL, 2, NULL, NULL),
(2, 2, 1, 1, '2021-03-11 06:39:13', '2021-03-11 06:39:13', NULL, 2, NULL, NULL),
(5, 3, 0, 1, '2021-03-26 04:27:38', '2021-03-26 04:27:38', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_payment_method_description`
--

CREATE TABLE `admin_payment_method_description` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `payment_method_id` int(11) DEFAULT NULL COMMENT 'FK = admin_payment_method',
  `language_id` int(11) NOT NULL COMMENT 'FK = admin_languages',
  `payment_method` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_payment_method_description`
--

INSERT INTO `admin_payment_method_description` (`id`, `payment_method_id`, `language_id`, `payment_method`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 1, 'Paytm', 1, '2021-03-11 06:37:10', '2021-03-11 06:37:10', NULL, 2, NULL, NULL),
(2, 1, 6, 'Paytm', 1, '2021-03-11 06:37:10', '2021-03-11 06:37:10', NULL, 2, NULL, NULL),
(3, 1, 9, 'Paytm', 1, '2021-03-11 06:37:10', '2021-03-11 06:37:10', NULL, 2, NULL, NULL),
(4, 1, 10, 'Paytm', 1, '2021-03-11 06:37:10', '2021-03-11 06:37:10', NULL, 2, NULL, NULL),
(5, 2, 1, 'Card', 1, '2021-03-11 06:39:13', '2021-03-11 06:39:13', NULL, 2, NULL, NULL),
(6, 2, 6, 'Card', 1, '2021-03-11 06:39:13', '2021-03-11 06:39:13', NULL, 2, NULL, NULL),
(7, 2, 9, 'Card', 1, '2021-03-11 06:39:13', '2021-03-11 06:39:13', NULL, 2, NULL, NULL),
(8, 2, 10, 'Card', 1, '2021-03-11 06:39:13', '2021-03-11 06:39:13', NULL, 2, NULL, NULL),
(13, 5, 1, '1', 1, '2021-03-26 04:27:38', '2021-03-26 04:27:38', NULL, 2, NULL, NULL),
(14, 5, 6, '2', 1, '2021-03-26 04:27:38', '2021-03-26 04:27:38', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_payment_method_master`
--

CREATE TABLE `admin_payment_method_master` (
  `id` int(10) UNSIGNED NOT NULL,
  `sorting` int(11) NOT NULL,
  `setting_key` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `setting_val` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_multi_lang` tinyint(1) NOT NULL DEFAULT 0,
  `img_height` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_width` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_require` tinyint(1) NOT NULL DEFAULT 0,
  `can_edit` tinyint(1) NOT NULL DEFAULT 1,
  `validation` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_payment_method_master`
--

INSERT INTO `admin_payment_method_master` (`id`, `sorting`, `setting_key`, `setting_val`, `description`, `is_multi_lang`, `img_height`, `img_width`, `img_size`, `is_require`, `can_edit`, `validation`, `type`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 'App Name 1', 'Neogen Infotech 1', 'Application/Site name', 0, '', '', '', 1, 1, '', 'text', 1, '2021-03-11 04:45:04', '2021-03-11 04:45:04', NULL, NULL, NULL, NULL),
(2, 2, 'App Name 2', 'Neogen Infotech 2', 'Application/Site name', 0, '', '', '', 1, 1, '', 'text', 1, '2021-03-11 04:45:04', '2021-03-11 04:45:04', NULL, NULL, NULL, NULL),
(3, 3, 'App Name 3', 'Neogen Infotech 3', 'Application/Site name', 0, '', '', '', 1, 1, '', 'text', 1, '2021-03-11 04:45:04', '2021-03-11 04:45:04', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_payment_method_settings`
--

CREATE TABLE `admin_payment_method_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `sorting` int(11) NOT NULL,
  `payment_id` int(11) DEFAULT NULL COMMENT 'FK = admin_payment_method',
  `setting_key` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `setting_val` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_multi_lang` tinyint(1) NOT NULL DEFAULT 0,
  `img_height` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_width` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_require` tinyint(1) NOT NULL DEFAULT 0,
  `can_edit` tinyint(1) NOT NULL DEFAULT 1,
  `validation` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_payment_method_settings`
--

INSERT INTO `admin_payment_method_settings` (`id`, `sorting`, `payment_id`, `setting_key`, `setting_val`, `description`, `is_multi_lang`, `img_height`, `img_width`, `img_size`, `is_require`, `can_edit`, `validation`, `type`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 0, 1, 'App Name 1', NULL, '', 0, NULL, NULL, NULL, 0, 1, NULL, '', 1, '2021-03-11 06:37:10', '2021-03-11 06:37:10', NULL, 2, NULL, NULL),
(2, 0, 1, 'App Name 2', NULL, '', 0, NULL, NULL, NULL, 0, 1, NULL, '', 1, '2021-03-11 06:37:10', '2021-03-11 06:37:10', NULL, 2, NULL, NULL),
(3, 0, 1, 'App Name 3', NULL, '', 0, NULL, NULL, NULL, 0, 1, NULL, '', 1, '2021-03-11 06:37:10', '2021-03-11 06:37:10', NULL, 2, NULL, NULL),
(4, 0, 2, 'App Name 1', NULL, '', 0, NULL, NULL, NULL, 0, 1, NULL, '', 1, '2021-03-11 06:39:13', '2021-03-11 06:39:13', NULL, 2, NULL, NULL),
(6, 0, 5, 'App Name 1', NULL, '', 0, NULL, NULL, NULL, 0, 1, NULL, '', 1, '2021-03-26 04:27:38', '2021-03-26 04:27:38', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_privacy_policy`
--

CREATE TABLE `admin_privacy_policy` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_privacy_policy_description`
--

CREATE TABLE `admin_privacy_policy_description` (
  `id` int(10) UNSIGNED NOT NULL,
  `privacy_policy_id` int(11) NOT NULL COMMENT 'Privacy Policy',
  `privacy_policy` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_id` int(11) NOT NULL COMMENT 'FK = admin_languages',
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_products`
--

CREATE TABLE `admin_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `sorting` int(11) DEFAULT NULL,
  `product_price` double(10,2) NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_quantity` int(11) DEFAULT NULL,
  `product_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_products`
--

INSERT INTO `admin_products` (`id`, `sorting`, `product_price`, `product_name`, `product_quantity`, `product_code`, `product_description`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 0, 201.00, 'test1', 3, '123test1', 'test2', 1, '2021-03-30 12:12:35', '2021-03-30 14:19:46', NULL, 5, 5, NULL),
(2, 0, 500.00, 'demo', 1, 'demo', 'demo', 1, '2021-03-30 12:15:50', '2021-03-30 12:15:50', NULL, 5, NULL, NULL),
(3, 0, 300.00, 'teat', 2, 'teat', NULL, 1, '2021-03-30 12:25:23', '2021-03-30 12:25:29', '2021-03-30 12:25:29', 5, 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `admin_products_categories`
--

CREATE TABLE `admin_products_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `products_id` int(10) UNSIGNED NOT NULL COMMENT 'FK = admin_products',
  `categories_id` int(10) UNSIGNED NOT NULL COMMENT 'FK = admin_categories',
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_products_categories`
--

INSERT INTO `admin_products_categories` (`id`, `products_id`, `categories_id`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 1, 1, '2021-03-23 06:30:25', '2021-03-23 06:30:25', NULL, 2, NULL, NULL),
(2, 2, 1, 1, '2021-03-23 06:30:48', '2021-03-24 07:22:41', '2021-03-24 07:22:41', 2, NULL, NULL),
(3, 3, 1, 1, '2021-03-23 06:32:03', '2021-03-23 06:32:03', NULL, 2, NULL, NULL),
(6, 6, 1, 1, '2021-03-23 06:41:14', '2021-03-23 06:41:14', NULL, 2, NULL, NULL),
(7, 7, 1, 1, '2021-03-23 06:42:05', '2021-03-23 06:42:05', NULL, 2, NULL, NULL),
(8, 8, 1, 1, '2021-03-23 06:44:04', '2021-03-23 06:44:04', NULL, 2, NULL, NULL),
(9, 9, 1, 1, '2021-03-23 06:44:38', '2021-03-23 06:44:38', NULL, 2, NULL, NULL),
(14, 14, 1, 1, '2021-03-23 06:53:35', '2021-03-23 06:53:35', NULL, 2, NULL, NULL),
(15, 15, 1, 1, '2021-03-23 06:54:10', '2021-03-23 06:54:10', NULL, 2, NULL, NULL),
(16, 16, 1, 1, '2021-03-23 06:54:33', '2021-03-23 06:55:29', '2021-03-23 06:55:29', 2, NULL, NULL),
(17, 16, 1, 1, '2021-03-23 06:55:29', '2021-03-23 06:55:42', '2021-03-23 06:55:42', 2, NULL, NULL),
(18, 16, 1, 1, '2021-03-23 06:55:42', '2021-03-23 06:55:42', NULL, 2, NULL, NULL),
(19, 2, 1, 1, '2021-03-24 07:22:41', '2021-03-24 07:22:53', '2021-03-24 07:22:53', 2, NULL, NULL),
(20, 2, 1, 1, '2021-03-24 07:22:53', '2021-03-26 05:10:23', '2021-03-26 05:10:23', 2, NULL, NULL),
(21, 2, 1, 1, '2021-03-26 05:10:23', '2021-03-26 05:10:23', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_products_description`
--

CREATE TABLE `admin_products_description` (
  `id` int(10) UNSIGNED NOT NULL,
  `products_id` int(10) UNSIGNED NOT NULL COMMENT 'FK = admin_products',
  `language_id` int(11) NOT NULL COMMENT 'FK = admin_languages',
  `products_name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `products_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_detail` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `additional_info` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_products_description`
--

INSERT INTO `admin_products_description` (`id`, `products_id`, `language_id`, `products_name`, `products_description`, `product_detail`, `additional_info`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 1, 'Test', NULL, NULL, NULL, 1, '2021-03-23 06:30:25', '2021-03-23 06:30:25', NULL, 2, NULL, NULL),
(2, 1, 6, 'Test', NULL, NULL, NULL, 1, '2021-03-23 06:30:25', '2021-03-23 06:30:25', NULL, 2, NULL, NULL),
(3, 2, 1, 'Test1', NULL, NULL, NULL, 1, '2021-03-23 06:30:48', '2021-03-23 06:30:48', NULL, 2, NULL, NULL),
(4, 2, 6, 'Test', NULL, NULL, NULL, 1, '2021-03-23 06:30:48', '2021-03-23 06:30:48', NULL, 2, NULL, NULL),
(5, 3, 1, 'Test1', NULL, NULL, NULL, 1, '2021-03-23 06:32:04', '2021-03-23 06:32:04', NULL, 2, NULL, NULL),
(6, 3, 6, 'Test', NULL, NULL, NULL, 1, '2021-03-23 06:32:04', '2021-03-23 06:32:04', NULL, 2, NULL, NULL),
(7, 6, 1, 'Test', NULL, NULL, NULL, 1, '2021-03-23 06:41:14', '2021-03-23 06:41:14', NULL, 2, NULL, NULL),
(8, 6, 6, 'Test', NULL, NULL, NULL, 1, '2021-03-23 06:41:14', '2021-03-23 06:41:14', NULL, 2, NULL, NULL),
(9, 7, 1, 'Test', NULL, NULL, NULL, 1, '2021-03-23 06:42:05', '2021-03-23 06:42:05', NULL, 2, NULL, NULL),
(10, 7, 6, 'Test', NULL, NULL, NULL, 1, '2021-03-23 06:42:05', '2021-03-23 06:42:05', NULL, 2, NULL, NULL),
(11, 8, 1, 'Test', NULL, NULL, NULL, 1, '2021-03-23 06:44:04', '2021-03-23 06:44:04', NULL, 2, NULL, NULL),
(12, 8, 6, 'Test', NULL, NULL, NULL, 1, '2021-03-23 06:44:04', '2021-03-23 06:44:04', NULL, 2, NULL, NULL),
(13, 9, 1, 'Test', NULL, NULL, NULL, 1, '2021-03-23 06:44:38', '2021-03-23 06:44:38', NULL, 2, NULL, NULL),
(14, 9, 6, 'Test', NULL, NULL, NULL, 1, '2021-03-23 06:44:38', '2021-03-23 06:44:38', NULL, 2, NULL, NULL),
(15, 14, 1, 'Test', NULL, NULL, NULL, 1, '2021-03-23 06:53:35', '2021-03-23 06:53:35', NULL, 2, NULL, NULL),
(16, 14, 6, 'Test', NULL, NULL, NULL, 1, '2021-03-23 06:53:35', '2021-03-23 06:53:35', NULL, 2, NULL, NULL),
(17, 15, 1, 'Test', NULL, NULL, NULL, 1, '2021-03-23 06:54:10', '2021-03-23 06:54:10', NULL, 2, NULL, NULL),
(18, 15, 6, 'Test', NULL, NULL, NULL, 1, '2021-03-23 06:54:10', '2021-03-23 06:54:10', NULL, 2, NULL, NULL),
(19, 16, 1, 'Test', NULL, NULL, NULL, 1, '2021-03-23 06:54:34', '2021-03-23 06:54:34', NULL, 2, NULL, NULL),
(20, 16, 6, 'Test', NULL, NULL, NULL, 1, '2021-03-23 06:54:34', '2021-03-23 06:54:34', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_products_images`
--

CREATE TABLE `admin_products_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `products_id` int(10) UNSIGNED NOT NULL COMMENT 'FK = admin_products',
  `image_url` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_thumb_url` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_cart_url` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_products_images`
--

INSERT INTO `admin_products_images` (`id`, `products_id`, `image_url`, `image_thumb_url`, `image_cart_url`, `is_default`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 'public/product/image/download_1616500825.jpg', 'public/product/image/thumb/download_1616500825.jpg', 'public/product/image/cart/download_1616500825.jpg', 1, 1, '2021-03-23 06:30:25', '2021-03-23 06:30:25', NULL, 2, NULL, NULL),
(2, 1, 'public/product/image/download_1616500825.jpg', 'public/product/image/thumb/download_1616500825.jpg', 'public/product/image/cart/download_1616500825.jpg', 0, 1, '2021-03-23 06:30:25', '2021-03-26 04:40:58', '2021-03-26 04:40:58', 2, 2, 2),
(3, 2, 'public/product/image/download_1616500825.jpg', 'public/product/image/thumb/download_1616500825.jpg', 'public/product/image/cart/download_1616500825.jpg', 1, 1, '2021-03-23 06:30:48', '2021-03-23 06:30:48', NULL, 2, NULL, NULL),
(4, 2, 'public/product/image/download_1616500825.jpg', 'public/product/image/thumb/download_1616500825.jpg', 'public/product/image/cart/download_1616500825.jpg', 0, 1, '2021-03-23 06:30:48', '2021-03-26 04:43:41', '2021-03-26 04:43:41', 2, 2, 2),
(5, 3, 'public/product/image/download_1616500825.jpg', 'public/product/image/thumb/download_1616500825.jpg', 'public/product/image/cart/download_1616500825.jpg', 1, 1, '2021-03-23 06:32:03', '2021-03-23 06:32:03', NULL, 2, NULL, NULL),
(6, 3, 'public/product/image/download_1616500825.jpg', 'public/product/image/thumb/download_1616500825.jpg', 'public/product/image/cart/download_1616500825.jpg', 0, 1, '2021-03-23 06:32:03', '2021-03-23 06:39:23', '2021-03-23 06:39:23', 2, 2, 2),
(7, 3, 'public/product/image/download_1616500923.jpg', 'public/product/image/thumb/download_1616500923.jpg', 'public/product/image/cart/download_1616500923.jpg', 1, 1, '2021-03-23 06:32:04', '2021-03-23 06:32:04', NULL, 2, NULL, NULL),
(12, 6, 'public/product/image/download_1616500825.jpg', 'public/product/image/thumb/download_1616500825.jpg', 'public/product/image/cart/download_1616500825.jpg', 1, 1, '2021-03-23 06:41:14', '2021-03-23 06:41:14', NULL, 2, NULL, NULL),
(13, 6, 'public/product/image/download_1616500825.jpg', 'public/product/image/thumb/download_1616500825.jpg', 'public/product/image/cart/download_1616500825.jpg', 0, 1, '2021-03-23 06:41:14', '2021-03-23 06:41:14', NULL, 2, NULL, NULL),
(14, 7, 'public/product/image/download_1616500825.jpg', 'public/product/image/thumb/download_1616500825.jpg', 'public/product/image/cart/download_1616500825.jpg', 1, 1, '2021-03-23 06:42:05', '2021-03-23 06:42:05', NULL, 2, NULL, NULL),
(15, 7, 'public/product/image/download_1616500825.jpg', 'public/product/image/thumb/download_1616500825.jpg', 'public/product/image/cart/download_1616500825.jpg', 0, 1, '2021-03-23 06:42:05', '2021-03-23 06:43:36', '2021-03-23 06:43:36', 2, 2, 2),
(16, 8, 'public/product/image/download_1616500825.jpg', 'public/product/image/thumb/download_1616500825.jpg', 'public/product/image/cart/download_1616500825.jpg', 1, 1, '2021-03-23 06:44:04', '2021-03-23 06:44:04', NULL, 2, NULL, NULL),
(17, 9, 'public/product/image/download_1616500825.jpg', 'public/product/image/thumb/download_1616500825.jpg', 'public/product/image/cart/download_1616500825.jpg', 1, 1, '2021-03-23 06:44:38', '2021-03-23 06:44:38', NULL, 2, NULL, NULL),
(22, 14, 'public/product/image/download_1616500825.jpg', 'public/product/image/thumb/download_1616500825.jpg', 'public/product/image/cart/download_1616500825.jpg', 1, 1, '2021-03-23 06:53:35', '2021-03-23 06:53:35', '2021-03-23 06:53:35', 2, 2, 2),
(23, 14, 'public/product/image/download_1616502215.jpg', 'public/product/image/thumb/download_1616502215.jpg', 'public/product/image/cart/download_1616502215.jpg', 1, 1, '2021-03-23 06:53:35', '2021-03-23 06:53:35', NULL, 2, NULL, NULL),
(24, 15, 'public/product/image/download_1616502215.jpg', 'public/product/image/thumb/download_1616502215.jpg', 'public/product/image/cart/download_1616502215.jpg', 1, 1, '2021-03-23 06:54:10', '2021-03-23 06:54:10', NULL, 2, NULL, NULL),
(25, 16, 'public/product/image/download_1616502215.jpg', 'public/product/image/thumb/download_1616502215.jpg', 'public/product/image/cart/download_1616502215.jpg', 1, 1, '2021-03-23 06:54:33', '2021-03-23 06:54:33', '2021-03-23 06:54:33', 2, 2, 2),
(26, 16, 'public/product/image/download_1616502273.jpg', 'public/product/image/thumb/download_1616502273.jpg', 'public/product/image/cart/download_1616502273.jpg', 1, 1, '2021-03-23 06:54:34', '2021-03-23 06:54:34', NULL, 2, NULL, NULL),
(27, 16, 'public/product/image/download_1616502329.jpg', 'public/product/image/thumb/download_1616502329.jpg', 'public/product/image/cart/download_1616502329.jpg', 0, 1, '2021-03-23 06:55:29', '2021-03-23 06:55:29', NULL, 2, NULL, NULL),
(28, 16, 'public/product/image/download_1616502342.jpg', 'public/product/image/thumb/download_1616502342.jpg', 'public/product/image/cart/download_1616502342.jpg', 0, 1, '2021-03-23 06:55:42', '2021-03-23 06:55:42', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_return_policy`
--

CREATE TABLE `admin_return_policy` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_return_policy_description`
--

CREATE TABLE `admin_return_policy_description` (
  `id` int(10) UNSIGNED NOT NULL,
  `return_policy_id` int(11) NOT NULL COMMENT 'Return Policy',
  `return_policy` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_id` int(11) NOT NULL COMMENT 'FK = admin_languages',
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_settings`
--

CREATE TABLE `admin_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `sorting` int(11) NOT NULL,
  `setting_key` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `setting_val` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_multi_lang` tinyint(1) NOT NULL DEFAULT 0,
  `img_height` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_width` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_require` tinyint(1) NOT NULL DEFAULT 0,
  `can_edit` tinyint(1) NOT NULL DEFAULT 1,
  `validation` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_settings`
--

INSERT INTO `admin_settings` (`id`, `sorting`, `setting_key`, `setting_val`, `description`, `is_multi_lang`, `img_height`, `img_width`, `img_size`, `is_require`, `can_edit`, `validation`, `type`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 'App Name', 'Japan Ecommerce', 'Application/Site name', 0, '', '', '', 1, 1, '', 'text', 1, '2021-03-10 01:19:50', '2021-03-12 01:27:54', NULL, NULL, NULL, NULL),
(2, 2, 'App Logo', '1', 'Application/Site Logo (Size 360x360 pixels)', 0, '360', '360', '', 0, 1, '', 'file', 1, '2021-03-10 01:19:50', '2021-03-15 23:46:00', NULL, NULL, NULL, NULL),
(3, 3, 'FavIcon', 'favicon/logo.ico', 'Application/Site FavIcon (Size 16x16 pixels)', 0, '16', '16', '', 0, 1, '', 'file', 1, '2021-03-10 01:19:50', '2021-03-10 01:19:50', NULL, NULL, NULL, NULL),
(4, 4, 'Web/App Environment', '1', 'Web Environment', 0, '', '', '', 1, 1, '', 'radio', 1, '2021-03-10 01:19:50', '2021-03-22 07:48:10', NULL, NULL, NULL, NULL),
(5, 5, 'Contact us Email', 'info@neogeninfotech.com', 'Communication Email ID of Administrator', 0, '', '', '', 1, 1, '', 'text', 1, '2021-03-10 01:19:50', '2021-03-10 01:19:50', NULL, NULL, NULL, NULL),
(6, 6, 'Order Email', 'noreply@neogeninfotech.com', 'Communication Email for order, All Email will sent from this Email ID', 0, '', '', '', 1, 1, '', 'text', 1, '2021-03-10 01:19:50', '2021-03-10 01:20:35', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_settings_description`
--

CREATE TABLE `admin_settings_description` (
  `id` int(10) UNSIGNED NOT NULL,
  `settings_id` int(11) NOT NULL COMMENT 'FK = admin_settings',
  `language_id` int(11) NOT NULL COMMENT 'FK = admin_languages',
  `setting_key` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `setting_val` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_shipping`
--

CREATE TABLE `admin_shipping` (
  `id` int(10) UNSIGNED NOT NULL,
  `rate` int(11) DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_shipping`
--

INSERT INTO `admin_shipping` (`id`, `rate`, `currency`, `is_default`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 534, 'INR', '0', 1, '2021-03-09 08:20:28', '2021-03-26 03:51:10', NULL, 2, 2, NULL),
(3, 10, 'INS', '0', 1, '2021-03-09 23:49:49', '2021-03-26 04:12:25', NULL, 2, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_shipping_description`
--

CREATE TABLE `admin_shipping_description` (
  `id` int(10) UNSIGNED NOT NULL,
  `shipping_id` int(11) NOT NULL DEFAULT 0 COMMENT 'FK = admin_shipping',
  `language_id` int(10) DEFAULT NULL,
  `shipping_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_shipping_description`
--

INSERT INTO `admin_shipping_description` (`id`, `shipping_id`, `language_id`, `shipping_name`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 1, 'japan', 1, '2021-03-09 08:20:28', '2021-03-09 08:20:28', NULL, 2, NULL, NULL),
(2, 1, 6, 'japan1', 1, '2021-03-09 08:20:28', '2021-03-09 08:20:28', NULL, 2, NULL, NULL),
(3, 1, 9, 'japan2', 1, '2021-03-09 08:20:28', '2021-03-09 08:20:28', NULL, 2, NULL, NULL),
(7, 3, 1, 'Ahemedabad', 1, '2021-03-09 23:49:49', '2021-03-09 23:49:49', NULL, 2, NULL, NULL),
(8, 3, 6, 'Ahemedabad1', 1, '2021-03-09 23:49:49', '2021-03-09 23:49:49', NULL, 2, NULL, NULL),
(9, 3, 9, 'Ahemedabad2', 1, '2021-03-09 23:49:49', '2021-03-09 23:49:49', NULL, 2, NULL, NULL),
(18, 1, NULL, 'xsd', 1, '2021-03-11 00:14:55', '2021-03-11 00:14:55', NULL, 2, NULL, NULL),
(19, 1, NULL, 'japan', 1, '2021-03-11 07:07:29', '2021-03-11 07:07:29', NULL, 2, NULL, NULL),
(20, 1, NULL, 'cdfg', 1, '2021-03-11 07:08:17', '2021-03-11 07:08:17', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_social_media_setting`
--

CREATE TABLE `admin_social_media_setting` (
  `id` int(10) UNSIGNED NOT NULL,
  `fb_app_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fb_secret_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fb_app_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_fb_login` tinyint(1) NOT NULL DEFAULT 0,
  `is_google_login` tinyint(1) NOT NULL DEFAULT 0,
  `google_app_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `google_app_secret` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `google_app_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_social_media_setting`
--

INSERT INTO `admin_social_media_setting` (`id`, `fb_app_id`, `fb_secret_id`, `fb_app_url`, `is_fb_login`, `is_google_login`, `google_app_id`, `google_app_secret`, `google_app_url`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'mayur', 'fb.join/abc.com', 'https://www.abc2.com/', 1, 0, 'test', 'testree', 'https://www.abc.com', 1, '2021-03-10 04:19:56', '2021-03-22 07:54:31', NULL, 2, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_state`
--

CREATE TABLE `admin_state` (
  `id` int(10) UNSIGNED NOT NULL,
  `sorting` int(11) DEFAULT NULL,
  `country_id` int(10) DEFAULT NULL COMMENT 'FK = admin_country',
  `is_default` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_state`
--

INSERT INTO `admin_state` (`id`, `sorting`, `country_id`, `is_default`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 4, 14, '0', 1, '2021-03-10 06:38:00', '2021-03-26 04:20:09', NULL, 2, 2, NULL),
(2, 2, 14, '0', 0, '2021-03-10 06:42:59', '2021-03-10 06:57:44', '2021-03-10 06:57:44', 2, 2, 2),
(3, 2, 3, '0', 1, '2021-03-10 06:58:47', '2021-03-16 04:39:51', NULL, 2, 2, NULL),
(4, 3, 14, '0', 1, '2021-03-11 04:08:33', '2021-03-12 07:39:08', '2021-03-12 07:39:08', 2, 2, 2),
(9, 4, 3, '0', 0, '2021-03-11 07:46:50', '2021-03-11 08:12:38', '2021-03-11 08:12:38', 2, 2, 2),
(23, 3, 14, '0', 1, '2021-03-17 00:30:31', '2021-03-17 00:30:53', '2021-03-17 00:30:53', 2, 2, 2),
(24, 3, 0, '0', 1, '2021-03-17 00:31:03', '2021-03-17 00:44:24', '2021-03-17 00:44:24', 2, 2, 2),
(25, 3, 14, '0', 1, '2021-03-17 00:44:37', '2021-03-17 00:48:13', NULL, 2, 2, NULL),
(26, 1, 14, '0', 1, '2021-03-17 00:50:38', '2021-03-26 04:20:09', NULL, 2, 2, NULL),
(27, 5, 14, '0', 1, '2021-03-17 03:58:00', '2021-03-17 03:58:07', NULL, 2, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_state_description`
--

CREATE TABLE `admin_state_description` (
  `id` int(10) UNSIGNED NOT NULL,
  `state_id` int(11) NOT NULL DEFAULT 0 COMMENT 'FK = admin_state',
  `language_id` int(11) NOT NULL COMMENT 'FK = admin_languages',
  `state_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_state_description`
--

INSERT INTO `admin_state_description` (`id`, `state_id`, `language_id`, `state_name`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 1, 'Gujarat', 1, '2021-03-10 06:38:00', '2021-03-10 06:38:00', NULL, 2, NULL, NULL),
(2, 1, 6, 'Gujarat', 1, '2021-03-10 06:38:01', '2021-03-10 06:38:01', NULL, 2, NULL, NULL),
(3, 1, 9, 'Gujarat', 1, '2021-03-10 06:38:01', '2021-03-10 06:38:01', NULL, 2, NULL, NULL),
(4, 2, 1, 'Gujarat2', 1, '2021-03-10 06:42:59', '2021-03-10 06:57:44', '2021-03-10 06:57:44', 2, 2, 2),
(5, 2, 6, 'Gujarat2', 1, '2021-03-10 06:42:59', '2021-03-10 06:57:44', '2021-03-10 06:57:44', 2, 2, 2),
(6, 2, 9, 'Gujarat2', 1, '2021-03-10 06:42:59', '2021-03-10 06:57:44', '2021-03-10 06:57:44', 2, 2, 2),
(7, 3, 1, 'bihar', 1, '2021-03-10 06:58:47', '2021-03-12 07:39:36', NULL, 2, 2, NULL),
(8, 3, 6, 'Gujarat1', 1, '2021-03-10 06:58:47', '2021-03-10 06:58:47', NULL, 2, NULL, NULL),
(9, 3, 9, 'Gujarat1', 1, '2021-03-10 06:58:47', '2021-03-10 06:58:47', NULL, 2, NULL, NULL),
(10, 1, 10, 'Gujarat', 1, '2021-03-10 07:25:21', '2021-03-10 07:25:21', NULL, 2, NULL, NULL),
(11, 4, 1, 'e', 1, '2021-03-11 04:08:33', '2021-03-12 07:39:08', '2021-03-12 07:39:08', 2, 2, 2),
(12, 4, 6, 'e', 1, '2021-03-11 04:08:33', '2021-03-12 07:39:08', '2021-03-12 07:39:08', 2, 2, 2),
(13, 4, 9, 'e', 1, '2021-03-11 04:08:33', '2021-03-12 07:39:08', '2021-03-12 07:39:08', 2, 2, 2),
(14, 4, 10, 'e', 1, '2021-03-11 04:08:33', '2021-03-12 07:39:08', '2021-03-12 07:39:08', 2, 2, 2),
(15, 7, 1, 'adas', 1, '2021-03-11 07:43:41', '2021-03-11 07:43:47', '2021-03-11 07:43:47', 2, 2, 2),
(16, 7, 6, 'axda', 1, '2021-03-11 07:43:41', '2021-03-11 07:43:47', '2021-03-11 07:43:47', 2, 2, 2),
(17, 7, 9, 'xasx', 1, '2021-03-11 07:43:41', '2021-03-11 07:43:47', '2021-03-11 07:43:47', 2, 2, 2),
(18, 7, 10, 'cs', 1, '2021-03-11 07:43:41', '2021-03-11 07:43:47', '2021-03-11 07:43:47', 2, 2, 2),
(19, 8, 1, 'zxvd', 1, '2021-03-11 07:43:59', '2021-03-11 07:45:16', '2021-03-11 07:45:16', 2, 2, 2),
(20, 8, 6, 'sfsd', 1, '2021-03-11 07:43:59', '2021-03-11 07:45:16', '2021-03-11 07:45:16', 2, 2, 2),
(21, 8, 9, 'fsdgd', 1, '2021-03-11 07:43:59', '2021-03-11 07:45:16', '2021-03-11 07:45:16', 2, 2, 2),
(22, 8, 10, 'fsdfsdf', 1, '2021-03-11 07:43:59', '2021-03-11 07:45:16', '2021-03-11 07:45:16', 2, 2, 2),
(23, 9, 1, 'asaxv', 1, '2021-03-11 07:46:50', '2021-03-11 08:12:38', '2021-03-11 08:12:38', 2, 2, 2),
(24, 9, 6, 'xas', 1, '2021-03-11 07:46:50', '2021-03-11 08:12:38', '2021-03-11 08:12:38', 2, 2, 2),
(25, 9, 9, 'sda', 1, '2021-03-11 07:46:50', '2021-03-11 08:12:38', '2021-03-11 08:12:38', 2, 2, 2),
(26, 9, 10, 'asdaw', 1, '2021-03-11 07:46:50', '2021-03-11 08:12:38', '2021-03-11 08:12:38', 2, 2, 2),
(27, 3, 10, 'bihar', 1, '2021-03-12 07:39:36', '2021-03-12 07:39:36', NULL, 2, NULL, NULL),
(28, 23, 1, 'zxcv', 1, '2021-03-17 00:30:31', '2021-03-17 00:30:53', '2021-03-17 00:30:53', 2, 2, 2),
(29, 23, 6, 'sdf', 1, '2021-03-17 00:30:31', '2021-03-17 00:30:53', '2021-03-17 00:30:53', 2, 2, 2),
(30, 23, 9, 'sdf', 1, '2021-03-17 00:30:31', '2021-03-17 00:30:53', '2021-03-17 00:30:53', 2, 2, 2),
(31, 23, 10, 'sdf', 1, '2021-03-17 00:30:31', '2021-03-17 00:30:53', '2021-03-17 00:30:53', 2, 2, 2),
(32, 24, 1, 'a', 1, '2021-03-17 00:31:03', '2021-03-17 00:44:23', '2021-03-17 00:44:23', 2, 2, 2),
(33, 24, 6, 'as', 1, '2021-03-17 00:31:03', '2021-03-17 00:44:24', '2021-03-17 00:44:24', 2, 2, 2),
(34, 24, 9, 'as', 1, '2021-03-17 00:31:03', '2021-03-17 00:44:24', '2021-03-17 00:44:24', 2, 2, 2),
(35, 24, 10, 'as', 1, '2021-03-17 00:31:03', '2021-03-17 00:44:24', '2021-03-17 00:44:24', 2, 2, 2),
(36, 25, 1, 'Akita', 1, '2021-03-17 00:44:37', '2021-03-26 03:54:16', NULL, 2, 2, NULL),
(37, 25, 6, 'Akita', 1, '2021-03-17 00:44:37', '2021-03-26 03:54:16', NULL, 2, 2, NULL),
(38, 25, 9, 'xc', 1, '2021-03-17 00:44:37', '2021-03-17 00:44:37', NULL, 2, NULL, NULL),
(39, 25, 10, 'xc', 1, '2021-03-17 00:44:37', '2021-03-17 00:44:37', NULL, 2, NULL, NULL),
(40, 26, 1, 'Kochi', 1, '2021-03-17 00:50:38', '2021-03-26 03:54:38', NULL, 2, 2, NULL),
(41, 26, 6, 'Kochi1', 1, '2021-03-17 00:50:38', '2021-03-26 03:54:38', NULL, 2, 2, NULL),
(42, 26, 9, 'wre', 1, '2021-03-17 00:50:38', '2021-03-17 00:50:38', NULL, 2, NULL, NULL),
(43, 26, 10, 're', 1, '2021-03-17 00:50:38', '2021-03-17 00:50:38', NULL, 2, NULL, NULL),
(44, 27, 1, 'mayur', 1, '2021-03-17 03:58:00', '2021-03-17 03:58:00', NULL, 2, NULL, NULL),
(45, 27, 6, 'asd', 1, '2021-03-17 03:58:00', '2021-03-17 03:58:00', NULL, 2, NULL, NULL),
(46, 27, 9, 'asd', 1, '2021-03-17 03:58:00', '2021-03-17 03:58:00', NULL, 2, NULL, NULL),
(47, 27, 10, 'sd', 1, '2021-03-17 03:58:00', '2021-03-17 03:58:00', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_term_and_condition`
--

CREATE TABLE `admin_term_and_condition` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_term_and_condition_description`
--

CREATE TABLE `admin_term_and_condition_description` (
  `id` int(10) UNSIGNED NOT NULL,
  `term_and_condition_id` int(11) NOT NULL COMMENT 'Term and Condition',
  `term_and_condition` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_id` int(11) NOT NULL COMMENT 'FK = admin_languages',
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_unit`
--

CREATE TABLE `admin_unit` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sorting` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_unit`
--

INSERT INTO `admin_unit` (`id`, `sorting`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 2, 0, '2021-03-16 05:28:20', '2021-03-26 05:19:32', NULL, 2, 2, NULL),
(2, 1, 1, '2021-03-16 05:28:27', '2021-03-26 05:19:32', NULL, 2, 2, NULL),
(3, 3, 1, '2021-03-16 05:39:34', '2021-03-16 05:39:55', '2021-03-16 05:39:55', 2, 2, 2),
(4, 3, 1, '2021-03-16 07:16:11', '2021-03-26 05:19:19', NULL, 2, 2, NULL),
(5, 4, 1, '2021-03-26 05:18:53', '2021-03-26 05:18:53', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_unit_description`
--

CREATE TABLE `admin_unit_description` (
  `id` int(10) UNSIGNED NOT NULL,
  `unit_id` int(11) NOT NULL DEFAULT 0 COMMENT 'FK = admin_unit',
  `language_id` int(11) NOT NULL COMMENT 'FK = admin_languages',
  `unit_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_unit_description`
--

INSERT INTO `admin_unit_description` (`id`, `unit_id`, `language_id`, `unit_name`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 1, 'test', 1, '2021-03-16 05:28:20', '2021-03-16 05:40:05', NULL, 2, 2, NULL),
(2, 1, 6, 't', 1, '2021-03-16 05:28:20', '2021-03-16 05:28:20', NULL, 2, NULL, NULL),
(3, 1, 9, 't', 1, '2021-03-16 05:28:20', '2021-03-16 05:28:20', NULL, 2, NULL, NULL),
(4, 1, 10, 't', 1, '2021-03-16 05:28:20', '2021-03-16 05:28:20', NULL, 2, NULL, NULL),
(5, 2, 1, 'unit', 1, '2021-03-16 05:28:27', '2021-03-16 05:40:32', NULL, 2, 2, NULL),
(6, 2, 6, 's', 1, '2021-03-16 05:28:27', '2021-03-16 05:28:27', NULL, 2, NULL, NULL),
(7, 2, 9, 's', 1, '2021-03-16 05:28:27', '2021-03-16 05:28:27', NULL, 2, NULL, NULL),
(8, 2, 10, 's', 1, '2021-03-16 05:28:27', '2021-03-16 05:28:27', NULL, 2, NULL, NULL),
(13, 4, 1, 'data', 1, '2021-03-16 07:16:11', '2021-03-16 07:16:11', NULL, 2, NULL, NULL),
(14, 4, 6, 'as', 1, '2021-03-16 07:16:11', '2021-03-16 07:16:11', NULL, 2, NULL, NULL),
(15, 4, 9, 'sd', 1, '2021-03-16 07:16:11', '2021-03-16 07:16:11', NULL, 2, NULL, NULL),
(16, 4, 10, 'sd', 1, '2021-03-16 07:16:11', '2021-03-16 07:16:11', NULL, 2, NULL, NULL),
(17, 5, 1, 'test data', 1, '2021-03-26 05:18:53', '2021-03-26 05:19:14', NULL, 2, 2, NULL),
(18, 5, 6, 'test data', 1, '2021-03-26 05:18:53', '2021-03-26 05:18:53', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menu_level_1`
--

CREATE TABLE `menu_level_1` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_level_1`
--

INSERT INTO `menu_level_1` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', '2021-03-25 04:44:29', '2021-03-25 04:44:29'),
(2, 'Admin', '2021-03-25 04:44:31', '2021-03-25 04:44:31');

-- --------------------------------------------------------

--
-- Table structure for table `menu_level_2`
--

CREATE TABLE `menu_level_2` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_level_1_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_level_2`
--

INSERT INTO `menu_level_2` (`id`, `menu_level_1_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Role', '2021-03-25 04:44:29', '2021-03-25 04:44:29'),
(2, 1, 'Permission', '2021-03-25 04:44:30', '2021-03-25 04:44:30'),
(3, 1, 'Role - Permission', '2021-03-25 04:44:30', '2021-03-25 04:44:30'),
(4, 1, 'User - Role', '2021-03-25 04:44:30', '2021-03-25 04:44:30'),
(5, 2, 'Dashboard', '2021-03-25 04:44:32', '2021-03-25 04:44:32'),
(6, 2, 'Profile', '2021-03-25 04:44:32', '2021-03-25 04:44:32'),
(7, 2, 'Master', '2021-03-25 04:44:32', '2021-03-25 04:44:32'),
(8, 2, 'General Setting', '2021-03-25 04:44:38', '2021-03-25 04:44:38'),
(9, 1, 'Super-Admin-Dashboard', '2021-03-25 04:44:31', '2021-03-25 04:44:31'),
(10, 2, 'Product', '2021-03-25 04:44:43', '2021-03-25 04:44:43'),
(11, 2, 'Coupons', '2021-03-25 04:44:46', '2021-03-25 04:44:46'),
(12, 2, 'Customer', '2021-03-25 04:44:47', '2021-03-25 04:44:47'),
(13, 2, 'Blog', '2021-03-25 04:44:48', '2021-03-25 04:44:48');

-- --------------------------------------------------------

--
-- Table structure for table `menu_level_3`
--

CREATE TABLE `menu_level_3` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_level_2_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_level_3`
--

INSERT INTO `menu_level_3` (`id`, `menu_level_2_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 7, 'Countries', '2021-03-25 04:44:33', '2021-03-25 04:44:33'),
(2, 7, 'City', '2021-03-25 04:44:33', '2021-03-25 04:44:33'),
(3, 8, 'Settings', '2021-03-25 04:44:38', '2021-03-25 04:44:38'),
(4, 7, 'Language', '2021-03-25 04:44:34', '2021-03-25 04:44:34'),
(5, 7, 'Currency', '2021-03-25 04:44:34', '2021-03-25 04:44:34'),
(6, 7, 'Order Status', '2021-03-25 04:44:35', '2021-03-25 04:44:35'),
(7, 8, 'About Us', '2021-03-25 04:44:39', '2021-03-25 04:44:39'),
(8, 8, 'Contact Us', '2021-03-25 04:44:39', '2021-03-25 04:44:39'),
(9, 7, 'Shipping Method', '2021-03-25 04:44:35', '2021-03-25 04:44:35'),
(10, 7, 'Courier Company', '2021-03-25 04:44:36', '2021-03-25 04:44:36'),
(11, 8, 'Privacy Policy', '2021-03-25 04:44:40', '2021-03-25 04:44:40'),
(12, 8, 'Return Policy', '2021-03-25 04:44:40', '2021-03-25 04:44:40'),
(13, 8, 'Terms & Condition', '2021-03-25 04:44:41', '2021-03-25 04:44:41'),
(14, 7, 'State', '2021-03-25 04:44:37', '2021-03-25 04:44:37'),
(15, 7, 'Payment Method', '2021-03-25 04:44:37', '2021-03-25 04:44:37'),
(16, 8, 'Social Media Settings', '2021-03-25 04:44:41', '2021-03-25 04:44:41'),
(17, 8, 'Media Settings', '2021-03-25 04:44:42', '2021-03-25 04:44:42'),
(18, 10, 'Category', '2021-03-25 04:44:43', '2021-03-25 04:44:43'),
(19, 10, 'Sub Category', '2021-03-25 04:44:44', '2021-03-25 04:44:44'),
(20, 10, 'Brand', '2021-03-25 04:44:44', '2021-03-25 04:44:44'),
(21, 10, 'Units', '2021-03-25 04:44:45', '2021-03-25 04:44:45'),
(22, 10, 'Product', '2021-03-25 04:44:46', '2021-03-25 04:44:46'),
(23, 13, 'News Categories', '2021-03-25 04:44:48', '2021-03-25 04:44:48'),
(24, 13, 'News', '2021-03-25 04:44:49', '2021-03-25 04:44:49');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_07_27_110140_create_permission_tables', 1),
(5, '2020_07_29_121510_create_menu_level', 1),
(6, '2020_09_28_064943_create_admin_settings_table', 1),
(7, '2020_09_30_060117_create_admin_languages_table', 1),
(8, '2020_11_04_053319_create_admins_table', 1),
(9, '2020_11_09_055902_create_admin_country_table', 1),
(10, '2020_11_09_055921_create_admin_country_description_table', 1),
(11, '2020_11_09_142252_create_admin_city_table', 1),
(12, '2020_11_09_142344_create_admin_city_description_table', 1),
(13, '2020_11_23_111804_create_admin_settings_description_table', 1),
(14, '2021_03_08_104218_create_admin_currencies_table', 2),
(15, '2021_03_08_104240_create_admin_order_status_table', 3),
(16, '2021_03_09_055903_create_admin_about_us_table', 4),
(17, '2021_03_09_060128_create_admin_about_us_page_description_table', 4),
(18, '2021_03_09_060937_create_admin_contact_us_table', 5),
(19, '2021_03_09_094438_create_admin_shipping_table', 5),
(20, '2021_03_09_094901_create_admin_shipping_description_table', 6),
(21, '2021_03_09_095405_create_admin_courier_company_table', 7),
(22, '2021_03_09_101229_create_admin_return_policy_description_table', 8),
(23, '2021_03_09_101402_create_admin_return_policy_table', 8),
(24, '2021_03_09_101706_create_admin_privacy_policy_table', 8),
(25, '2021_03_09_101721_create_admin_privacy_policy_description_table', 8),
(26, '2021_03_09_105043_create_admin_term_and_condition_table', 9),
(27, '2021_03_09_105123_create_admin_term_and_condition_description_table', 9),
(28, '2021_03_10_061747_create_admin_payment_method_table', 10),
(29, '2021_03_10_061813_create_admin_payment_method_description_table', 10),
(30, '2021_03_10_062021_create_admin_payment_method_description_table', 11),
(31, '2021_03_10_062331_create_admin_state_table', 12),
(32, '2021_03_10_062619_create_admin_state_description_table', 13),
(33, '2021_03_10_074406_create_admin_social_media_setting_table', 14),
(34, '2021_03_11_061706_create_admin_payment_method_settings_table', 15),
(35, '2021_03_11_055227_create_admin_media_setting_table', 16),
(36, '2021_03_11_100724_create_admin_payment_method_master_table', 17),
(37, '2021_03_15_060008_create_admin_category_table', 18),
(38, '2021_03_15_060123_create_admin_category_description_table', 18),
(39, '2021_03_15_062825_create_admin_brand_table', 19),
(40, '2021_03_15_062841_create_admin_brand_description_table', 19),
(41, '2021_03_16_065017_create_admin_unit_table', 20),
(42, '2021_03_16_065316_create_admin_unit_description_table', 21),
(43, '2021_03_16_071807_create_admin_coupons_table', 22),
(44, '2020_10_07_085146_create_admin_products_table', 23),
(45, '2020_10_07_085225_create_admin_products_description_table', 23),
(46, '2020_10_07_085239_create_admin_products_images_table', 23),
(47, '2021_03_16_075433_create_admin_discount_type_table', 24),
(48, '2020_10_15_074128_create_admin_products_categories_table', 25),
(49, '2021_03_18_061438_create_admin_coupon_discount_categories_table', 26),
(50, '2021_03_18_094419_create_user_address_detail_table', 27),
(51, '2021_03_23_092703_create_admin_blog_table', 28),
(52, '2021_03_23_093128_create_admin_blog_description_table', 29),
(53, '2021_03_25_065728_create_admin_news_categories_table', 30),
(54, '2021_03_25_065931_create_admin_news_categories_description_table', 31);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Admin', 1),
(2, 'App\\Admin', 2),
(2, 'App\\Admin', 5),
(3, 'App\\Admin', 3),
(2, 'App\\Admin', 0);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_level_1_id` int(10) UNSIGNED DEFAULT NULL,
  `menu_level_2_id` int(10) UNSIGNED DEFAULT NULL,
  `menu_level_3_id` int(10) UNSIGNED DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `description`, `menu_level_1_id`, `menu_level_2_id`, `menu_level_3_id`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'View_1', 'admin', 'View-Super Admin', 1, NULL, NULL, 1, '2021-03-25 04:44:29', '2021-03-25 04:44:29', NULL, NULL, NULL, NULL),
(2, 'Create_1', 'admin', 'Create-Super Admin', 1, NULL, NULL, 1, '2021-03-25 04:44:29', '2021-03-25 04:44:29', NULL, NULL, NULL, NULL),
(3, 'Edit_1', 'admin', 'Edit-Super Admin', 1, NULL, NULL, 1, '2021-03-25 04:44:29', '2021-03-25 04:44:29', NULL, NULL, NULL, NULL),
(4, 'Delete_1', 'admin', 'Delete-Super Admin', 1, NULL, NULL, 1, '2021-03-25 04:44:29', '2021-03-25 04:44:29', NULL, NULL, NULL, NULL),
(5, 'View_1_1', 'admin', 'View-Super Admin-Role', 1, 1, NULL, 1, '2021-03-25 04:44:29', '2021-03-25 04:44:29', NULL, NULL, NULL, NULL),
(6, 'Create_1_1', 'admin', 'Create-Super Admin-Role', 1, 1, NULL, 1, '2021-03-25 04:44:29', '2021-03-25 04:44:29', NULL, NULL, NULL, NULL),
(7, 'Edit_1_1', 'admin', 'Edit-Super Admin-Role', 1, 1, NULL, 1, '2021-03-25 04:44:29', '2021-03-25 04:44:29', NULL, NULL, NULL, NULL),
(8, 'Delete_1_1', 'admin', 'Delete-Super Admin-Role', 1, 1, NULL, 1, '2021-03-25 04:44:30', '2021-03-25 04:44:30', NULL, NULL, NULL, NULL),
(9, 'View_1_2', 'admin', 'View-Super Admin-Permission', 1, 2, NULL, 1, '2021-03-25 04:44:30', '2021-03-25 04:44:30', NULL, NULL, NULL, NULL),
(10, 'Create_1_2', 'admin', 'Create-Super Admin-Permission', 1, 2, NULL, 1, '2021-03-25 04:44:30', '2021-03-25 04:44:30', NULL, NULL, NULL, NULL),
(11, 'Edit_1_2', 'admin', 'Edit-Super Admin-Permission', 1, 2, NULL, 1, '2021-03-25 04:44:30', '2021-03-25 04:44:30', NULL, NULL, NULL, NULL),
(12, 'Delete_1_2', 'admin', 'Delete-Super Admin-Permission', 1, 2, NULL, 1, '2021-03-25 04:44:30', '2021-03-25 04:44:30', NULL, NULL, NULL, NULL),
(13, 'View_1_3', 'admin', 'View-Super Admin-Role - Permission', 1, 3, NULL, 1, '2021-03-25 04:44:30', '2021-03-25 04:44:30', NULL, NULL, NULL, NULL),
(14, 'Create_1_3', 'admin', 'Create-Super Admin-Role - Permission', 1, 3, NULL, 1, '2021-03-25 04:44:30', '2021-03-25 04:44:30', NULL, NULL, NULL, NULL),
(15, 'Edit_1_3', 'admin', 'Edit-Super Admin-Role - Permission', 1, 3, NULL, 1, '2021-03-25 04:44:30', '2021-03-25 04:44:30', NULL, NULL, NULL, NULL),
(16, 'Delete_1_3', 'admin', 'Delete-Super Admin-Role - Permission', 1, 3, NULL, 1, '2021-03-25 04:44:30', '2021-03-25 04:44:30', NULL, NULL, NULL, NULL),
(17, 'View_1_4', 'admin', 'View-Super Admin-User - Role', 1, 4, NULL, 1, '2021-03-25 04:44:30', '2021-03-25 04:44:30', NULL, NULL, NULL, NULL),
(18, 'Create_1_4', 'admin', 'Create-Super Admin-User - Role', 1, 4, NULL, 1, '2021-03-25 04:44:30', '2021-03-25 04:44:30', NULL, NULL, NULL, NULL),
(19, 'Edit_1_4', 'admin', 'Edit-Super Admin-User - Role', 1, 4, NULL, 1, '2021-03-25 04:44:30', '2021-03-25 04:44:30', NULL, NULL, NULL, NULL),
(20, 'Delete_1_4', 'admin', 'Delete-Super Admin-User - Role', 1, 4, NULL, 1, '2021-03-25 04:44:31', '2021-03-25 04:44:31', NULL, NULL, NULL, NULL),
(21, 'View_1_9', 'admin', 'View-Super Admin-Super-Admin-Dashboard', 1, 9, NULL, 1, '2021-03-25 04:44:31', '2021-03-25 04:44:31', NULL, NULL, NULL, NULL),
(22, 'Create_1_9', 'admin', 'Create-Super Admin-Super-Admin-Dashboard', 1, 9, NULL, 1, '2021-03-25 04:44:31', '2021-03-25 04:44:31', NULL, NULL, NULL, NULL),
(23, 'Edit_1_9', 'admin', 'Edit-Super Admin-Super-Admin-Dashboard', 1, 9, NULL, 1, '2021-03-25 04:44:31', '2021-03-25 04:44:31', NULL, NULL, NULL, NULL),
(24, 'Delete_1_9', 'admin', 'Delete-Super Admin-Super-Admin-Dashboard', 1, 9, NULL, 1, '2021-03-25 04:44:31', '2021-03-25 04:44:31', NULL, NULL, NULL, NULL),
(25, 'View_2', 'admin', 'View-Admin', 2, NULL, NULL, 1, '2021-03-25 04:44:31', '2021-03-25 04:44:31', NULL, NULL, NULL, NULL),
(26, 'Create_2', 'admin', 'Create-Admin', 2, NULL, NULL, 1, '2021-03-25 04:44:31', '2021-03-25 04:44:31', NULL, NULL, NULL, NULL),
(27, 'Edit_2', 'admin', 'Edit-Admin', 2, NULL, NULL, 1, '2021-03-25 04:44:32', '2021-03-25 04:44:32', NULL, NULL, NULL, NULL),
(28, 'Delete_2', 'admin', 'Delete-Admin', 2, NULL, NULL, 1, '2021-03-25 04:44:32', '2021-03-25 04:44:32', NULL, NULL, NULL, NULL),
(29, 'View_2_5', 'admin', 'View-Admin-Dashboard', 2, 5, NULL, 1, '2021-03-25 04:44:32', '2021-03-25 04:44:32', NULL, NULL, NULL, NULL),
(30, 'Create_2_5', 'admin', 'Create-Admin-Dashboard', 2, 5, NULL, 1, '2021-03-25 04:44:32', '2021-03-25 04:44:32', NULL, NULL, NULL, NULL),
(31, 'Edit_2_5', 'admin', 'Edit-Admin-Dashboard', 2, 5, NULL, 1, '2021-03-25 04:44:32', '2021-03-25 04:44:32', NULL, NULL, NULL, NULL),
(32, 'Delete_2_5', 'admin', 'Delete-Admin-Dashboard', 2, 5, NULL, 1, '2021-03-25 04:44:32', '2021-03-25 04:44:32', NULL, NULL, NULL, NULL),
(33, 'View_2_6', 'admin', 'View-Admin-Profile', 2, 6, NULL, 1, '2021-03-25 04:44:32', '2021-03-25 04:44:32', NULL, NULL, NULL, NULL),
(34, 'Create_2_6', 'admin', 'Create-Admin-Profile', 2, 6, NULL, 1, '2021-03-25 04:44:32', '2021-03-25 04:44:32', NULL, NULL, NULL, NULL),
(35, 'Edit_2_6', 'admin', 'Edit-Admin-Profile', 2, 6, NULL, 1, '2021-03-25 04:44:32', '2021-03-25 04:44:32', NULL, NULL, NULL, NULL),
(36, 'Delete_2_6', 'admin', 'Delete-Admin-Profile', 2, 6, NULL, 1, '2021-03-25 04:44:32', '2021-03-25 04:44:32', NULL, NULL, NULL, NULL),
(37, 'View_2_7', 'admin', 'View-Admin-Master', 2, 7, NULL, 1, '2021-03-25 04:44:33', '2021-03-25 04:44:33', NULL, NULL, NULL, NULL),
(38, 'Create_2_7', 'admin', 'Create-Admin-Master', 2, 7, NULL, 1, '2021-03-25 04:44:33', '2021-03-25 04:44:33', NULL, NULL, NULL, NULL),
(39, 'Edit_2_7', 'admin', 'Edit-Admin-Master', 2, 7, NULL, 1, '2021-03-25 04:44:33', '2021-03-25 04:44:33', NULL, NULL, NULL, NULL),
(40, 'Delete_2_7', 'admin', 'Delete-Admin-Master', 2, 7, NULL, 1, '2021-03-25 04:44:33', '2021-03-25 04:44:33', NULL, NULL, NULL, NULL),
(41, 'View_2_7_1', 'admin', 'View-Admin-Master-Countries', 2, 7, 7, 1, '2021-03-25 04:44:33', '2021-03-25 04:44:33', NULL, NULL, NULL, NULL),
(42, 'Create_2_7_1', 'admin', 'Create-Admin-Master-Countries', 2, 7, 7, 1, '2021-03-25 04:44:33', '2021-03-25 04:44:33', NULL, NULL, NULL, NULL),
(43, 'Edit_2_7_1', 'admin', 'Edit-Admin-Master-Countries', 2, 7, 7, 1, '2021-03-25 04:44:33', '2021-03-25 04:44:33', NULL, NULL, NULL, NULL),
(44, 'Delete_2_7_1', 'admin', 'Delete-Admin-Master-Countries', 2, 7, 7, 1, '2021-03-25 04:44:33', '2021-03-25 04:44:33', NULL, NULL, NULL, NULL),
(45, 'View_2_7_2', 'admin', 'View-Admin-Master-City', 2, 7, 7, 1, '2021-03-25 04:44:33', '2021-03-25 04:44:33', NULL, NULL, NULL, NULL),
(46, 'Create_2_7_2', 'admin', 'Create-Admin-Master-City', 2, 7, 7, 1, '2021-03-25 04:44:34', '2021-03-25 04:44:34', NULL, NULL, NULL, NULL),
(47, 'Edit_2_7_2', 'admin', 'Edit-Admin-Master-City', 2, 7, 7, 1, '2021-03-25 04:44:34', '2021-03-25 04:44:34', NULL, NULL, NULL, NULL),
(48, 'Delete_2_7_2', 'admin', 'Delete-Admin-Master-City', 2, 7, 7, 1, '2021-03-25 04:44:34', '2021-03-25 04:44:34', NULL, NULL, NULL, NULL),
(49, 'View_2_7_4', 'admin', 'View-Admin-Master-Language', 2, 7, 7, 1, '2021-03-25 04:44:34', '2021-03-25 04:44:34', NULL, NULL, NULL, NULL),
(50, 'Create_2_7_4', 'admin', 'Create-Admin-Master-Language', 2, 7, 7, 1, '2021-03-25 04:44:34', '2021-03-25 04:44:34', NULL, NULL, NULL, NULL),
(51, 'Edit_2_7_4', 'admin', 'Edit-Admin-Master-Language', 2, 7, 7, 1, '2021-03-25 04:44:34', '2021-03-25 04:44:34', NULL, NULL, NULL, NULL),
(52, 'Delete_2_7_4', 'admin', 'Delete-Admin-Master-Language', 2, 7, 7, 1, '2021-03-25 04:44:34', '2021-03-25 04:44:34', NULL, NULL, NULL, NULL),
(53, 'View_2_7_5', 'admin', 'View-Admin-Master-Currency', 2, 7, 7, 1, '2021-03-25 04:44:34', '2021-03-25 04:44:34', NULL, NULL, NULL, NULL),
(54, 'Create_2_7_5', 'admin', 'Create-Admin-Master-Currency', 2, 7, 7, 1, '2021-03-25 04:44:35', '2021-03-25 04:44:35', NULL, NULL, NULL, NULL),
(55, 'Edit_2_7_5', 'admin', 'Edit-Admin-Master-Currency', 2, 7, 7, 1, '2021-03-25 04:44:35', '2021-03-25 04:44:35', NULL, NULL, NULL, NULL),
(56, 'Delete_2_7_5', 'admin', 'Delete-Admin-Master-Currency', 2, 7, 7, 1, '2021-03-25 04:44:35', '2021-03-25 04:44:35', NULL, NULL, NULL, NULL),
(57, 'View_2_7_6', 'admin', 'View-Admin-Master-Order Status', 2, 7, 7, 1, '2021-03-25 04:44:35', '2021-03-25 04:44:35', NULL, NULL, NULL, NULL),
(58, 'Create_2_7_6', 'admin', 'Create-Admin-Master-Order Status', 2, 7, 7, 1, '2021-03-25 04:44:35', '2021-03-25 04:44:35', NULL, NULL, NULL, NULL),
(59, 'Edit_2_7_6', 'admin', 'Edit-Admin-Master-Order Status', 2, 7, 7, 1, '2021-03-25 04:44:35', '2021-03-25 04:44:35', NULL, NULL, NULL, NULL),
(60, 'Delete_2_7_6', 'admin', 'Delete-Admin-Master-Order Status', 2, 7, 7, 1, '2021-03-25 04:44:35', '2021-03-25 04:44:35', NULL, NULL, NULL, NULL),
(61, 'View_2_7_9', 'admin', 'View-Admin-Master-Shipping Method', 2, 7, 7, 1, '2021-03-25 04:44:35', '2021-03-25 04:44:35', NULL, NULL, NULL, NULL),
(62, 'Create_2_7_9', 'admin', 'Create-Admin-Master-Shipping Method', 2, 7, 7, 1, '2021-03-25 04:44:36', '2021-03-25 04:44:36', NULL, NULL, NULL, NULL),
(63, 'Edit_2_7_9', 'admin', 'Edit-Admin-Master-Shipping Method', 2, 7, 7, 1, '2021-03-25 04:44:36', '2021-03-25 04:44:36', NULL, NULL, NULL, NULL),
(64, 'Delete_2_7_9', 'admin', 'Delete-Admin-Master-Shipping Method', 2, 7, 7, 1, '2021-03-25 04:44:36', '2021-03-25 04:44:36', NULL, NULL, NULL, NULL),
(65, 'View_2_7_10', 'admin', 'View-Admin-Master-Courier Company', 2, 7, 7, 1, '2021-03-25 04:44:36', '2021-03-25 04:44:36', NULL, NULL, NULL, NULL),
(66, 'Create_2_7_10', 'admin', 'Create-Admin-Master-Courier Company', 2, 7, 7, 1, '2021-03-25 04:44:36', '2021-03-25 04:44:36', NULL, NULL, NULL, NULL),
(67, 'Edit_2_7_10', 'admin', 'Edit-Admin-Master-Courier Company', 2, 7, 7, 1, '2021-03-25 04:44:36', '2021-03-25 04:44:36', NULL, NULL, NULL, NULL),
(68, 'Delete_2_7_10', 'admin', 'Delete-Admin-Master-Courier Company', 2, 7, 7, 1, '2021-03-25 04:44:36', '2021-03-25 04:44:36', NULL, NULL, NULL, NULL),
(69, 'View_2_7_14', 'admin', 'View-Admin-Master-State', 2, 7, 7, 1, '2021-03-25 04:44:37', '2021-03-25 04:44:37', NULL, NULL, NULL, NULL),
(70, 'Create_2_7_14', 'admin', 'Create-Admin-Master-State', 2, 7, 7, 1, '2021-03-25 04:44:37', '2021-03-25 04:44:37', NULL, NULL, NULL, NULL),
(71, 'Edit_2_7_14', 'admin', 'Edit-Admin-Master-State', 2, 7, 7, 1, '2021-03-25 04:44:37', '2021-03-25 04:44:37', NULL, NULL, NULL, NULL),
(72, 'Delete_2_7_14', 'admin', 'Delete-Admin-Master-State', 2, 7, 7, 1, '2021-03-25 04:44:37', '2021-03-25 04:44:37', NULL, NULL, NULL, NULL),
(73, 'View_2_7_15', 'admin', 'View-Admin-Master-Payment Method', 2, 7, 7, 1, '2021-03-25 04:44:37', '2021-03-25 04:44:37', NULL, NULL, NULL, NULL),
(74, 'Create_2_7_15', 'admin', 'Create-Admin-Master-Payment Method', 2, 7, 7, 1, '2021-03-25 04:44:37', '2021-03-25 04:44:37', NULL, NULL, NULL, NULL),
(75, 'Edit_2_7_15', 'admin', 'Edit-Admin-Master-Payment Method', 2, 7, 7, 1, '2021-03-25 04:44:37', '2021-03-25 04:44:37', NULL, NULL, NULL, NULL),
(76, 'Delete_2_7_15', 'admin', 'Delete-Admin-Master-Payment Method', 2, 7, 7, 1, '2021-03-25 04:44:37', '2021-03-25 04:44:37', NULL, NULL, NULL, NULL),
(77, 'View_2_8', 'admin', 'View-Admin-General Setting', 2, 8, NULL, 1, '2021-03-25 04:44:38', '2021-03-25 04:44:38', NULL, NULL, NULL, NULL),
(78, 'Create_2_8', 'admin', 'Create-Admin-General Setting', 2, 8, NULL, 1, '2021-03-25 04:44:38', '2021-03-25 04:44:38', NULL, NULL, NULL, NULL),
(79, 'Edit_2_8', 'admin', 'Edit-Admin-General Setting', 2, 8, NULL, 1, '2021-03-25 04:44:38', '2021-03-25 04:44:38', NULL, NULL, NULL, NULL),
(80, 'Delete_2_8', 'admin', 'Delete-Admin-General Setting', 2, 8, NULL, 1, '2021-03-25 04:44:38', '2021-03-25 04:44:38', NULL, NULL, NULL, NULL),
(81, 'View_2_8_3', 'admin', 'View-Admin-General Setting-Settings', 2, 8, 8, 1, '2021-03-25 04:44:38', '2021-03-25 04:44:38', NULL, NULL, NULL, NULL),
(82, 'Create_2_8_3', 'admin', 'Create-Admin-General Setting-Settings', 2, 8, 8, 1, '2021-03-25 04:44:38', '2021-03-25 04:44:38', NULL, NULL, NULL, NULL),
(83, 'Edit_2_8_3', 'admin', 'Edit-Admin-General Setting-Settings', 2, 8, 8, 1, '2021-03-25 04:44:38', '2021-03-25 04:44:38', NULL, NULL, NULL, NULL),
(84, 'Delete_2_8_3', 'admin', 'Delete-Admin-General Setting-Settings', 2, 8, 8, 1, '2021-03-25 04:44:39', '2021-03-25 04:44:39', NULL, NULL, NULL, NULL),
(85, 'View_2_8_7', 'admin', 'View-Admin-General Setting-About Us', 2, 8, 8, 1, '2021-03-25 04:44:39', '2021-03-25 04:44:39', NULL, NULL, NULL, NULL),
(86, 'Create_2_8_7', 'admin', 'Create-Admin-General Setting-About Us', 2, 8, 8, 1, '2021-03-25 04:44:39', '2021-03-25 04:44:39', NULL, NULL, NULL, NULL),
(87, 'Edit_2_8_7', 'admin', 'Edit-Admin-General Setting-About Us', 2, 8, 8, 1, '2021-03-25 04:44:39', '2021-03-25 04:44:39', NULL, NULL, NULL, NULL),
(88, 'Delete_2_8_7', 'admin', 'Delete-Admin-General Setting-About Us', 2, 8, 8, 1, '2021-03-25 04:44:39', '2021-03-25 04:44:39', NULL, NULL, NULL, NULL),
(89, 'View_2_8_8', 'admin', 'View-Admin-General Setting-Contact Us', 2, 8, 8, 1, '2021-03-25 04:44:39', '2021-03-25 04:44:39', NULL, NULL, NULL, NULL),
(90, 'Create_2_8_8', 'admin', 'Create-Admin-General Setting-Contact Us', 2, 8, 8, 1, '2021-03-25 04:44:39', '2021-03-25 04:44:39', NULL, NULL, NULL, NULL),
(91, 'Edit_2_8_8', 'admin', 'Edit-Admin-General Setting-Contact Us', 2, 8, 8, 1, '2021-03-25 04:44:39', '2021-03-25 04:44:39', NULL, NULL, NULL, NULL),
(92, 'Delete_2_8_8', 'admin', 'Delete-Admin-General Setting-Contact Us', 2, 8, 8, 1, '2021-03-25 04:44:40', '2021-03-25 04:44:40', NULL, NULL, NULL, NULL),
(93, 'View_2_8_11', 'admin', 'View-Admin-General Setting-Privacy Policy', 2, 8, 8, 1, '2021-03-25 04:44:40', '2021-03-25 04:44:40', NULL, NULL, NULL, NULL),
(94, 'Create_2_8_11', 'admin', 'Create-Admin-General Setting-Privacy Policy', 2, 8, 8, 1, '2021-03-25 04:44:40', '2021-03-25 04:44:40', NULL, NULL, NULL, NULL),
(95, 'Edit_2_8_11', 'admin', 'Edit-Admin-General Setting-Privacy Policy', 2, 8, 8, 1, '2021-03-25 04:44:40', '2021-03-25 04:44:40', NULL, NULL, NULL, NULL),
(96, 'Delete_2_8_11', 'admin', 'Delete-Admin-General Setting-Privacy Policy', 2, 8, 8, 1, '2021-03-25 04:44:40', '2021-03-25 04:44:40', NULL, NULL, NULL, NULL),
(97, 'View_2_8_12', 'admin', 'View-Admin-General Setting-Return Policy', 2, 8, 8, 1, '2021-03-25 04:44:40', '2021-03-25 04:44:40', NULL, NULL, NULL, NULL),
(98, 'Create_2_8_12', 'admin', 'Create-Admin-General Setting-Return Policy', 2, 8, 8, 1, '2021-03-25 04:44:40', '2021-03-25 04:44:40', NULL, NULL, NULL, NULL),
(99, 'Edit_2_8_12', 'admin', 'Edit-Admin-General Setting-Return Policy', 2, 8, 8, 1, '2021-03-25 04:44:41', '2021-03-25 04:44:41', NULL, NULL, NULL, NULL),
(100, 'Delete_2_8_12', 'admin', 'Delete-Admin-General Setting-Return Policy', 2, 8, 8, 1, '2021-03-25 04:44:41', '2021-03-25 04:44:41', NULL, NULL, NULL, NULL),
(101, 'View_2_8_13', 'admin', 'View-Admin-General Setting-Terms & Condition', 2, 8, 8, 1, '2021-03-25 04:44:41', '2021-03-25 04:44:41', NULL, NULL, NULL, NULL),
(102, 'Create_2_8_13', 'admin', 'Create-Admin-General Setting-Terms & Condition', 2, 8, 8, 1, '2021-03-25 04:44:41', '2021-03-25 04:44:41', NULL, NULL, NULL, NULL),
(103, 'Edit_2_8_13', 'admin', 'Edit-Admin-General Setting-Terms & Condition', 2, 8, 8, 1, '2021-03-25 04:44:41', '2021-03-25 04:44:41', NULL, NULL, NULL, NULL),
(104, 'Delete_2_8_13', 'admin', 'Delete-Admin-General Setting-Terms & Condition', 2, 8, 8, 1, '2021-03-25 04:44:41', '2021-03-25 04:44:41', NULL, NULL, NULL, NULL),
(105, 'View_2_8_16', 'admin', 'View-Admin-General Setting-Social Media Settings', 2, 8, 8, 1, '2021-03-25 04:44:41', '2021-03-25 04:44:41', NULL, NULL, NULL, NULL),
(106, 'Create_2_8_16', 'admin', 'Create-Admin-General Setting-Social Media Settings', 2, 8, 8, 1, '2021-03-25 04:44:42', '2021-03-25 04:44:42', NULL, NULL, NULL, NULL),
(107, 'Edit_2_8_16', 'admin', 'Edit-Admin-General Setting-Social Media Settings', 2, 8, 8, 1, '2021-03-25 04:44:42', '2021-03-25 04:44:42', NULL, NULL, NULL, NULL),
(108, 'Delete_2_8_16', 'admin', 'Delete-Admin-General Setting-Social Media Settings', 2, 8, 8, 1, '2021-03-25 04:44:42', '2021-03-25 04:44:42', NULL, NULL, NULL, NULL),
(109, 'View_2_8_17', 'admin', 'View-Admin-General Setting-Media Settings', 2, 8, 8, 1, '2021-03-25 04:44:42', '2021-03-25 04:44:42', NULL, NULL, NULL, NULL),
(110, 'Create_2_8_17', 'admin', 'Create-Admin-General Setting-Media Settings', 2, 8, 8, 1, '2021-03-25 04:44:42', '2021-03-25 04:44:42', NULL, NULL, NULL, NULL),
(111, 'Edit_2_8_17', 'admin', 'Edit-Admin-General Setting-Media Settings', 2, 8, 8, 1, '2021-03-25 04:44:42', '2021-03-25 04:44:42', NULL, NULL, NULL, NULL),
(112, 'Delete_2_8_17', 'admin', 'Delete-Admin-General Setting-Media Settings', 2, 8, 8, 1, '2021-03-25 04:44:42', '2021-03-25 04:44:42', NULL, NULL, NULL, NULL),
(113, 'View_2_10', 'admin', 'View-Admin-Product', 2, 10, NULL, 1, '2021-03-25 04:44:43', '2021-03-25 04:44:43', NULL, NULL, NULL, NULL),
(114, 'Create_2_10', 'admin', 'Create-Admin-Product', 2, 10, NULL, 1, '2021-03-25 04:44:43', '2021-03-25 04:44:43', NULL, NULL, NULL, NULL),
(115, 'Edit_2_10', 'admin', 'Edit-Admin-Product', 2, 10, NULL, 1, '2021-03-25 04:44:43', '2021-03-25 04:44:43', NULL, NULL, NULL, NULL),
(116, 'Delete_2_10', 'admin', 'Delete-Admin-Product', 2, 10, NULL, 1, '2021-03-25 04:44:43', '2021-03-25 04:44:43', NULL, NULL, NULL, NULL),
(117, 'View_2_10_18', 'admin', 'View-Admin-Product-Category', 2, 10, 10, 1, '2021-03-25 04:44:43', '2021-03-25 04:44:43', NULL, NULL, NULL, NULL),
(118, 'Create_2_10_18', 'admin', 'Create-Admin-Product-Category', 2, 10, 10, 1, '2021-03-25 04:44:44', '2021-03-25 04:44:44', NULL, NULL, NULL, NULL),
(119, 'Edit_2_10_18', 'admin', 'Edit-Admin-Product-Category', 2, 10, 10, 1, '2021-03-25 04:44:44', '2021-03-25 04:44:44', NULL, NULL, NULL, NULL),
(120, 'Delete_2_10_18', 'admin', 'Delete-Admin-Product-Category', 2, 10, 10, 1, '2021-03-25 04:44:44', '2021-03-25 04:44:44', NULL, NULL, NULL, NULL),
(121, 'View_2_10_19', 'admin', 'View-Admin-Product-Sub Category', 2, 10, 10, 1, '2021-03-25 04:44:44', '2021-03-25 04:44:44', NULL, NULL, NULL, NULL),
(122, 'Create_2_10_19', 'admin', 'Create-Admin-Product-Sub Category', 2, 10, 10, 1, '2021-03-25 04:44:44', '2021-03-25 04:44:44', NULL, NULL, NULL, NULL),
(123, 'Edit_2_10_19', 'admin', 'Edit-Admin-Product-Sub Category', 2, 10, 10, 1, '2021-03-25 04:44:44', '2021-03-25 04:44:44', NULL, NULL, NULL, NULL),
(124, 'Delete_2_10_19', 'admin', 'Delete-Admin-Product-Sub Category', 2, 10, 10, 1, '2021-03-25 04:44:44', '2021-03-25 04:44:44', NULL, NULL, NULL, NULL),
(125, 'View_2_10_20', 'admin', 'View-Admin-Product-Brand', 2, 10, 10, 1, '2021-03-25 04:44:45', '2021-03-25 04:44:45', NULL, NULL, NULL, NULL),
(126, 'Create_2_10_20', 'admin', 'Create-Admin-Product-Brand', 2, 10, 10, 1, '2021-03-25 04:44:45', '2021-03-25 04:44:45', NULL, NULL, NULL, NULL),
(127, 'Edit_2_10_20', 'admin', 'Edit-Admin-Product-Brand', 2, 10, 10, 1, '2021-03-25 04:44:45', '2021-03-25 04:44:45', NULL, NULL, NULL, NULL),
(128, 'Delete_2_10_20', 'admin', 'Delete-Admin-Product-Brand', 2, 10, 10, 1, '2021-03-25 04:44:45', '2021-03-25 04:44:45', NULL, NULL, NULL, NULL),
(129, 'View_2_10_21', 'admin', 'View-Admin-Product-Units', 2, 10, 10, 1, '2021-03-25 04:44:45', '2021-03-25 04:44:45', NULL, NULL, NULL, NULL),
(130, 'Create_2_10_21', 'admin', 'Create-Admin-Product-Units', 2, 10, 10, 1, '2021-03-25 04:44:45', '2021-03-25 04:44:45', NULL, NULL, NULL, NULL),
(131, 'Edit_2_10_21', 'admin', 'Edit-Admin-Product-Units', 2, 10, 10, 1, '2021-03-25 04:44:45', '2021-03-25 04:44:45', NULL, NULL, NULL, NULL),
(132, 'Delete_2_10_21', 'admin', 'Delete-Admin-Product-Units', 2, 10, 10, 1, '2021-03-25 04:44:46', '2021-03-25 04:44:46', NULL, NULL, NULL, NULL),
(133, 'View_2_10_22', 'admin', 'View-Admin-Product-Product', 2, 10, 10, 1, '2021-03-25 04:44:46', '2021-03-25 04:44:46', NULL, NULL, NULL, NULL),
(134, 'Create_2_10_22', 'admin', 'Create-Admin-Product-Product', 2, 10, 10, 1, '2021-03-25 04:44:46', '2021-03-25 04:44:46', NULL, NULL, NULL, NULL),
(135, 'Edit_2_10_22', 'admin', 'Edit-Admin-Product-Product', 2, 10, 10, 1, '2021-03-25 04:44:46', '2021-03-25 04:44:46', NULL, NULL, NULL, NULL),
(136, 'Delete_2_10_22', 'admin', 'Delete-Admin-Product-Product', 2, 10, 10, 1, '2021-03-25 04:44:46', '2021-03-25 04:44:46', NULL, NULL, NULL, NULL),
(137, 'View_2_11', 'admin', 'View-Admin-Coupons', 2, 11, NULL, 1, '2021-03-25 04:44:46', '2021-03-25 04:44:46', NULL, NULL, NULL, NULL),
(138, 'Create_2_11', 'admin', 'Create-Admin-Coupons', 2, 11, NULL, 1, '2021-03-25 04:44:47', '2021-03-25 04:44:47', NULL, NULL, NULL, NULL),
(139, 'Edit_2_11', 'admin', 'Edit-Admin-Coupons', 2, 11, NULL, 1, '2021-03-25 04:44:47', '2021-03-25 04:44:47', NULL, NULL, NULL, NULL),
(140, 'Delete_2_11', 'admin', 'Delete-Admin-Coupons', 2, 11, NULL, 1, '2021-03-25 04:44:47', '2021-03-25 04:44:47', NULL, NULL, NULL, NULL),
(141, 'View_2_12', 'admin', 'View-Admin-Customer', 2, 12, NULL, 1, '2021-03-25 04:44:47', '2021-03-25 04:44:47', NULL, NULL, NULL, NULL),
(142, 'Create_2_12', 'admin', 'Create-Admin-Customer', 2, 12, NULL, 1, '2021-03-25 04:44:47', '2021-03-25 04:44:47', NULL, NULL, NULL, NULL),
(143, 'Edit_2_12', 'admin', 'Edit-Admin-Customer', 2, 12, NULL, 1, '2021-03-25 04:44:47', '2021-03-25 04:44:47', NULL, NULL, NULL, NULL),
(144, 'Delete_2_12', 'admin', 'Delete-Admin-Customer', 2, 12, NULL, 1, '2021-03-25 04:44:47', '2021-03-25 04:44:47', NULL, NULL, NULL, NULL),
(145, 'View_2_13', 'admin', 'View-Admin-Blog', 2, 13, NULL, 1, '2021-03-25 04:44:48', '2021-03-25 04:44:48', NULL, NULL, NULL, NULL),
(146, 'Create_2_13', 'admin', 'Create-Admin-Blog', 2, 13, NULL, 1, '2021-03-25 04:44:48', '2021-03-25 04:44:48', NULL, NULL, NULL, NULL),
(147, 'Edit_2_13', 'admin', 'Edit-Admin-Blog', 2, 13, NULL, 1, '2021-03-25 04:44:48', '2021-03-25 04:44:48', NULL, NULL, NULL, NULL),
(148, 'Delete_2_13', 'admin', 'Delete-Admin-Blog', 2, 13, NULL, 1, '2021-03-25 04:44:48', '2021-03-25 04:44:48', NULL, NULL, NULL, NULL),
(149, 'View_2_13_23', 'admin', 'View-Admin-Blog-News Categories', 2, 13, 13, 1, '2021-03-25 04:44:48', '2021-03-25 04:44:48', NULL, NULL, NULL, NULL),
(150, 'Create_2_13_23', 'admin', 'Create-Admin-Blog-News Categories', 2, 13, 13, 1, '2021-03-25 04:44:48', '2021-03-25 04:44:48', NULL, NULL, NULL, NULL),
(151, 'Edit_2_13_23', 'admin', 'Edit-Admin-Blog-News Categories', 2, 13, 13, 1, '2021-03-25 04:44:49', '2021-03-25 04:44:49', NULL, NULL, NULL, NULL),
(152, 'Delete_2_13_23', 'admin', 'Delete-Admin-Blog-News Categories', 2, 13, 13, 1, '2021-03-25 04:44:49', '2021-03-25 04:44:49', NULL, NULL, NULL, NULL),
(153, 'View_2_13_24', 'admin', 'View-Admin-Blog-News', 2, 13, 13, 1, '2021-03-25 04:44:49', '2021-03-25 04:44:49', NULL, NULL, NULL, NULL),
(154, 'Create_2_13_24', 'admin', 'Create-Admin-Blog-News', 2, 13, 13, 1, '2021-03-25 04:44:49', '2021-03-25 04:44:49', NULL, NULL, NULL, NULL),
(155, 'Edit_2_13_24', 'admin', 'Edit-Admin-Blog-News', 2, 13, 13, 1, '2021-03-25 04:44:49', '2021-03-25 04:44:49', NULL, NULL, NULL, NULL),
(156, 'Delete_2_13_24', 'admin', 'Delete-Admin-Blog-News', 2, 13, 13, 1, '2021-03-25 04:44:49', '2021-03-25 04:44:49', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'super-admin', 'admin', 1, '2021-03-08 00:52:02', '2021-03-08 00:52:02', NULL, NULL, NULL, NULL),
(2, 'admin', 'admin', 1, '2021-03-08 00:52:02', '2021-03-08 00:52:02', NULL, NULL, NULL, NULL),
(3, 'camp-admin', 'admin', 1, '2021-03-08 00:52:02', '2021-03-08 00:52:02', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(5, 2),
(5, 3),
(6, 1),
(6, 2),
(6, 3),
(7, 1),
(7, 2),
(7, 3),
(8, 1),
(8, 2),
(8, 3),
(9, 1),
(9, 2),
(9, 3),
(10, 1),
(10, 2),
(10, 3),
(11, 1),
(11, 2),
(11, 3),
(12, 1),
(12, 2),
(12, 3),
(13, 1),
(13, 2),
(13, 3),
(14, 1),
(14, 2),
(14, 3),
(15, 1),
(15, 2),
(15, 3),
(16, 1),
(16, 2),
(16, 3),
(17, 1),
(17, 2),
(17, 3),
(18, 1),
(18, 2),
(18, 3),
(19, 1),
(19, 2),
(19, 3),
(20, 1),
(20, 2),
(20, 3),
(21, 1),
(21, 2),
(21, 3),
(22, 1),
(22, 2),
(22, 3),
(23, 1),
(23, 2),
(23, 3),
(24, 1),
(24, 2),
(24, 3),
(25, 1),
(25, 2),
(25, 3),
(26, 1),
(26, 2),
(26, 3),
(27, 1),
(27, 2),
(27, 3),
(28, 1),
(28, 2),
(28, 3),
(29, 1),
(29, 2),
(29, 3),
(30, 1),
(30, 2),
(30, 3),
(31, 1),
(31, 2),
(31, 3),
(32, 1),
(32, 2),
(32, 3),
(33, 1),
(33, 2),
(33, 3),
(34, 1),
(34, 2),
(34, 3),
(35, 1),
(35, 2),
(35, 3),
(36, 1),
(36, 2),
(36, 3),
(37, 1),
(37, 2),
(37, 3),
(38, 1),
(38, 2),
(38, 3),
(39, 1),
(39, 2),
(39, 3),
(40, 1),
(40, 2),
(40, 3),
(41, 1),
(41, 2),
(41, 3),
(42, 1),
(42, 2),
(42, 3),
(43, 1),
(43, 2),
(43, 3),
(44, 1),
(44, 2),
(44, 3),
(45, 1),
(45, 2),
(45, 3),
(46, 1),
(46, 2),
(46, 3),
(47, 1),
(47, 2),
(47, 3),
(48, 1),
(48, 2),
(48, 3),
(49, 1),
(49, 2),
(49, 3),
(50, 1),
(50, 2),
(50, 3),
(51, 1),
(51, 2),
(51, 3),
(52, 1),
(52, 2),
(52, 3),
(53, 1),
(53, 2),
(53, 3),
(54, 1),
(54, 2),
(54, 3),
(55, 1),
(55, 2),
(55, 3),
(56, 1),
(56, 2),
(56, 3),
(57, 1),
(57, 2),
(57, 3),
(58, 1),
(58, 2),
(58, 3),
(59, 1),
(59, 2),
(59, 3),
(60, 1),
(60, 2),
(60, 3),
(61, 1),
(61, 2),
(61, 3),
(62, 1),
(62, 2),
(62, 3),
(63, 1),
(63, 2),
(63, 3),
(64, 1),
(64, 2),
(64, 3),
(65, 1),
(65, 2),
(65, 3),
(66, 1),
(66, 2),
(66, 3),
(67, 1),
(67, 2),
(67, 3),
(68, 1),
(68, 2),
(68, 3),
(69, 1),
(69, 2),
(69, 3),
(70, 1),
(70, 2),
(70, 3),
(71, 1),
(71, 2),
(71, 3),
(72, 1),
(72, 2),
(72, 3),
(73, 1),
(73, 2),
(73, 3),
(74, 1),
(74, 2),
(74, 3),
(75, 1),
(75, 2),
(75, 3),
(76, 1),
(76, 2),
(76, 3),
(77, 1),
(77, 2),
(77, 3),
(78, 1),
(78, 2),
(78, 3),
(79, 1),
(79, 2),
(79, 3),
(80, 1),
(80, 2),
(80, 3),
(81, 1),
(81, 2),
(81, 3),
(82, 1),
(82, 2),
(82, 3),
(83, 1),
(83, 2),
(83, 3),
(84, 1),
(84, 2),
(84, 3),
(85, 1),
(85, 2),
(85, 3),
(86, 1),
(86, 2),
(86, 3),
(87, 1),
(87, 2),
(87, 3),
(88, 1),
(88, 2),
(88, 3),
(89, 1),
(89, 2),
(89, 3),
(90, 1),
(90, 2),
(90, 3),
(91, 1),
(91, 2),
(91, 3),
(92, 1),
(92, 2),
(92, 3),
(93, 1),
(93, 2),
(93, 3),
(94, 1),
(94, 2),
(94, 3),
(95, 1),
(95, 2),
(95, 3),
(96, 1),
(96, 2),
(96, 3),
(97, 1),
(97, 2),
(97, 3),
(98, 1),
(98, 2),
(98, 3),
(99, 1),
(99, 2),
(99, 3),
(100, 1),
(100, 2),
(100, 3),
(101, 1),
(101, 2),
(101, 3),
(102, 1),
(102, 2),
(102, 3),
(103, 1),
(103, 2),
(103, 3),
(104, 1),
(104, 2),
(104, 3),
(105, 1),
(105, 2),
(105, 3),
(106, 1),
(106, 2),
(106, 3),
(107, 1),
(107, 2),
(107, 3),
(108, 1),
(108, 2),
(108, 3),
(109, 1),
(109, 2),
(109, 3),
(110, 1),
(110, 2),
(110, 3),
(111, 1),
(111, 2),
(111, 3),
(112, 1),
(112, 2),
(112, 3),
(113, 1),
(113, 2),
(113, 3),
(114, 1),
(114, 2),
(114, 3),
(115, 1),
(115, 2),
(115, 3),
(116, 1),
(116, 2),
(116, 3),
(117, 1),
(117, 2),
(117, 3),
(118, 1),
(118, 2),
(118, 3),
(119, 1),
(119, 2),
(119, 3),
(120, 1),
(120, 2),
(120, 3),
(121, 1),
(121, 2),
(121, 3),
(122, 1),
(122, 2),
(122, 3),
(123, 1),
(123, 2),
(123, 3),
(124, 1),
(124, 2),
(124, 3),
(125, 1),
(125, 2),
(125, 3),
(126, 1),
(126, 2),
(126, 3),
(127, 1),
(127, 2),
(127, 3),
(128, 1),
(128, 2),
(128, 3),
(129, 1),
(129, 2),
(129, 3),
(130, 1),
(130, 2),
(130, 3),
(131, 1),
(131, 2),
(131, 3),
(132, 1),
(132, 2),
(132, 3),
(133, 1),
(133, 2),
(133, 3),
(134, 1),
(134, 2),
(134, 3),
(135, 1),
(135, 2),
(135, 3),
(136, 1),
(136, 2),
(136, 3),
(137, 1),
(137, 2),
(137, 3),
(138, 1),
(138, 2),
(138, 3),
(139, 1),
(139, 2),
(139, 3),
(140, 1),
(140, 2),
(140, 3),
(141, 1),
(141, 2),
(141, 3),
(142, 1),
(142, 2),
(142, 3),
(143, 1),
(143, 2),
(143, 3),
(144, 1),
(144, 2),
(144, 3),
(145, 1),
(145, 2),
(145, 3),
(146, 1),
(146, 2),
(146, 3),
(147, 1),
(147, 2),
(147, 3),
(148, 1),
(148, 2),
(148, 3),
(149, 1),
(149, 2),
(149, 3),
(150, 1),
(150, 2),
(150, 3),
(151, 1),
(151, 2),
(151, 3),
(152, 1),
(152, 2),
(152, 3),
(153, 1),
(153, 2),
(153, 3),
(154, 1),
(154, 2),
(154, 3),
(155, 1),
(155, 2),
(155, 3),
(156, 1),
(156, 2),
(156, 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` tinyint(1) DEFAULT 0,
  `social_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login_type` enum('website','google','facebook') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'website',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_date` date DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `state_id` int(10) DEFAULT NULL,
  `password_reset_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_picture` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `last_login` datetime DEFAULT NULL,
  `locale` int(11) DEFAULT NULL COMMENT 'User selected language ID',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `gender`, `social_id`, `login_type`, `email`, `email_verified_at`, `password`, `mobile`, `birth_date`, `country`, `state_id`, `password_reset_token`, `remember_token`, `profile_picture`, `is_active`, `last_login`, `locale`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'AS', 'as', '', 1, NULL, 'website', 'sdf@gmail.com', NULL, '$2y$10$MHJKVdnGrhRuKQx0oIKBJeNxAi3NwoICV53Es4SEM8T8.6X/Qrn6m', '1324', '2021-03-16', 14, 1, NULL, NULL, NULL, 1, NULL, NULL, '2021-03-19 03:52:32', '2021-03-19 03:53:43', '2021-03-19 03:53:43', 2, 2, 2),
(2, 'trfsd', 'rwesdf', '', 1, NULL, 'website', 'testrel2018@gmail.com', NULL, '$2y$10$cT.Np5wSJnR1QHnXCCRENO3Fp3b/ekt6khduXgaNTdKAdt.cLU0.K', '81609172', '2021-03-07', 14, 26, NULL, NULL, NULL, 1, NULL, NULL, '2021-03-19 03:54:37', '2021-03-19 04:00:00', '2021-03-19 04:00:00', 2, 2, 2),
(3, 'test123', 'test123', '', 1, NULL, 'website', 'aV@gmail.com', NULL, '$2y$10$DEk/vpnuw6x0I91BemisaeTXKiG5WYETcMX/2jN0/RfWVjjDBt2Lq', '23234553', '2021-03-01', 14, 27, NULL, NULL, NULL, 1, NULL, NULL, '2021-03-19 03:55:05', '2021-03-26 00:13:16', NULL, 2, 2, NULL),
(4, 'wred', 'rews', '', 1, NULL, 'website', 'a2323@gmail.com', NULL, '$2y$10$Cp.eWG2Uk.pEDDTcJAS09u8ZG3jfsd9PDhd/rWW9pJz.DNjJEcQS.', '312231321', '2021-03-15', 3, 3, NULL, NULL, NULL, 1, NULL, NULL, '2021-03-19 04:00:45', '2021-03-19 05:50:20', '2021-03-19 05:50:20', 2, 2, 2),
(5, 'asdf2345', 'sdf', '', 1, NULL, 'website', 'asdf@gmail.com', NULL, '$2y$10$2lMYRrSwVSa0lLs43Vc.m.XP.WJeiVkA7AY6USus82Rv9ukUAHYyO', '2345634534', '2021-03-16', 14, 26, NULL, NULL, NULL, 0, NULL, NULL, '2021-03-19 05:27:06', '2021-03-19 05:39:09', '2021-03-19 05:39:09', 2, 2, 2),
(6, 'nayna', 'rathod', '', 0, NULL, 'website', 'nayna@gmail.com', NULL, '$2y$10$kGMTUvCn.41dYkVojM1x8eewwQEfZy2C7vHH4Rzoswg5slJK/lxlS', '9632587415', '2021-03-23', 3, 3, NULL, NULL, NULL, 1, NULL, NULL, '2021-03-26 06:01:20', '2021-03-26 06:05:21', NULL, 2, 2, NULL),
(7, 'ss', 'ss', '', 1, NULL, 'website', 'abc@gmail.com', NULL, '$2y$10$rnT.miqlWvHUSeHy9IVxJej1iE6Kf1nAQIALg5rw3n98PD5bjbR7m', '99247622', '2021-03-25', 14, 25, NULL, NULL, NULL, 1, NULL, NULL, '2021-03-26 06:28:16', '2021-03-26 06:28:16', NULL, 2, NULL, NULL),
(8, 'test', 'user', '', 1, NULL, 'website', 'test@test.com', NULL, '$2y$10$VppwwhuOp2ugl3N/z/T2reTUPHDHYPN4qlUwZ6/iniN.SUgncYNBG', '1234567890', NULL, 3, 3, NULL, NULL, NULL, 1, NULL, NULL, '2021-03-30 07:43:16', '2021-03-30 07:43:16', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_address_detail`
--

CREATE TABLE `user_address_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'users',
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suburb` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'admin_country',
  `state_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'admin_state',
  `default_shipping_address` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_address_detail`
--

INSERT INTO `user_address_detail` (`id`, `user_id`, `company_name`, `first_name`, `last_name`, `address`, `suburb`, `postcode`, `city`, `country_id`, `state_id`, `default_shipping_address`, `is_active`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 'asd', 'asd', 'asdc', 'asd', 'sdc', '12345', 'xc', 3, 3, 1, 1, '2021-03-19 03:52:55', '2021-03-19 03:53:43', '2021-03-19 03:53:43', 2, 2, 2),
(2, 1, 'sc', 'xc', 'xc', 'xc', 'xcv', '234', 'sxcv', 14, 1, 0, 1, '2021-03-19 03:53:05', '2021-03-19 03:53:43', '2021-03-19 03:53:43', 2, 2, 2),
(3, 2, 'wedas', 'wesa', 'weqsa', 'wqeas', 'wesa', '213', 'ws', 14, 27, 1, 1, '2021-03-19 03:55:48', '2021-03-19 04:00:00', '2021-03-19 04:00:00', 2, 2, 2),
(4, 2, 'wae c', 'XZX', 'ASWAE', 'WEWQE', 'QEWWQEEW', '382025', 'QWEdx', 14, 1, 1, 1, '2021-03-19 03:56:19', '2021-03-19 04:00:00', '2021-03-19 04:00:00', 2, 2, 2),
(5, 3, 'wdswe1111111111111111111111111', '3ewwe122', 'ewewe1', 'weqwee122222222222222222323333323', 'PARABURDOO11111111111111111111', '67541', 'ewdswedsweqs1', 14, 27, 1, 1, '2021-03-19 04:01:16', '2021-03-19 05:47:24', '2021-03-19 05:47:24', 2, 2, 2),
(6, 3, 'sxdcv', 'xcv', 'zxc', 'zxc', 'zxc', '452345', 'asd', 14, 27, 0, 1, '2021-03-19 05:47:18', '2021-03-19 05:49:01', '2021-03-19 05:49:01', 2, 2, 2),
(7, 3, 'asdcv', 'AS', 'ASD', 'DS', 'ASD', '2345', 'ASD', 14, 27, 0, 1, '2021-03-19 05:48:38', '2021-03-19 05:48:54', '2021-03-19 05:48:54', 2, 2, 2),
(8, 4, 'waS', 'as', 'sdzxc', 'sdxc', 'dfxcv', '3443', 'aszdxc', 14, 1, 0, 1, '2021-03-19 05:49:41', '2021-03-19 05:50:20', '2021-03-19 05:50:20', 2, 2, 2),
(9, 3, 'asdf', 'asdf', 'asd', 'asdf123', 'asdf', '2345', 'zsd', 14, 1, 0, 1, '2021-03-19 06:17:44', '2021-03-22 04:31:06', NULL, 2, 2, NULL),
(10, 6, 'neogen', 'nayna', 'rathod', 'kundhadaa', 'chotila', '363520', 'rajkot', 3, 3, 1, 1, '2021-03-26 06:06:49', '2021-03-26 06:06:49', NULL, 2, NULL, NULL),
(11, 7, 'DElhivery', 'Anisa', 'Chief', '123', 'parramatta', '4602', 'parramatta', 3, 3, 0, 1, '2021-03-26 06:29:08', '2021-03-26 06:29:08', NULL, 2, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `admin_about_us`
--
ALTER TABLE `admin_about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_about_us_page_description`
--
ALTER TABLE `admin_about_us_page_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_blog`
--
ALTER TABLE `admin_blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_blog_description`
--
ALTER TABLE `admin_blog_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_brand`
--
ALTER TABLE `admin_brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_brand_description`
--
ALTER TABLE `admin_brand_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_category`
--
ALTER TABLE `admin_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_category_description`
--
ALTER TABLE `admin_category_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_city`
--
ALTER TABLE `admin_city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_city_description`
--
ALTER TABLE `admin_city_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_contact_us`
--
ALTER TABLE `admin_contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_country`
--
ALTER TABLE `admin_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_country_description`
--
ALTER TABLE `admin_country_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_coupons`
--
ALTER TABLE `admin_coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_coupon_discount_categories`
--
ALTER TABLE `admin_coupon_discount_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_courier_company`
--
ALTER TABLE `admin_courier_company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_currencies`
--
ALTER TABLE `admin_currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_discount_type`
--
ALTER TABLE `admin_discount_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_languages`
--
ALTER TABLE `admin_languages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_LANGUAGES_NAME` (`name`);

--
-- Indexes for table `admin_media_setting`
--
ALTER TABLE `admin_media_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_news_categories`
--
ALTER TABLE `admin_news_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_news_categories_description`
--
ALTER TABLE `admin_news_categories_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_order_status`
--
ALTER TABLE `admin_order_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_payment_method`
--
ALTER TABLE `admin_payment_method`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_payment_method_description`
--
ALTER TABLE `admin_payment_method_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_payment_method_master`
--
ALTER TABLE `admin_payment_method_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_payment_method_settings`
--
ALTER TABLE `admin_payment_method_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_privacy_policy`
--
ALTER TABLE `admin_privacy_policy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_privacy_policy_description`
--
ALTER TABLE `admin_privacy_policy_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_products`
--
ALTER TABLE `admin_products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `admin_order_status`
--
ALTER TABLE `admin_order_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `admin_products`
--
ALTER TABLE `admin_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
