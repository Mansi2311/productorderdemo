
<?php $__env->startSection('page_title',  trans('admin.product')); ?>
<?php $__env->startSection('additional_css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/select2/css/select2.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <?php echo $__env->make('layout/toaster', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?php echo e(__('admin.product')); ?></h1>
                </div>
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home.index')); ?>"><?php echo e(__('admin.home')); ?></a></li>
                        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.product.index')); ?>"><?php echo e(__('admin.product')); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo e(__('admin.list')); ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <form class="list-table" method="get" action="<?php echo e(route('admin.product.index')); ?>">
                  
                        <div class="card <?php echo e(config('custom.card-primary')); ?>">
                            <div class="table-card card-header pl-0">
                                <div class="card-tools w-100">
                                    <div class="input-group input-group-sm float-right custom-search">
                                        <input type="text" name="q" class="form-control float-right float-sm-right" placeholder="<?php echo e(__('admin.search')); ?>" value="<?php echo e(isset($searchText)?$searchText:''); ?>">

                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default" title="<?php echo e(__('admin.search')); ?>"><i class="fas fa-search"></i></button>
                                        </div>
                                        <a class="pl-2"  href="<?php echo e(route('admin.product.add')); ?>"><button type="button" class="<?php echo e(config('custom.btn-primary','btn btn-outline-primary')); ?> add-new-btn" title="<?php echo e(__('admin.add')); ?>"><?php echo e(__('admin.add')); ?></button></a>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <table data-model="AdminProduct" class="table normal-table table-striped">
                                <thead>
                                    <tr>
                                        <th class="asc-desc text-center" id="<?php if($sortKey=='id'): ?><?php echo e($sortVal); ?><?php endif; ?>"><?php echo e(__('admin.first_column')); ?><input <?php if($sortKey!='id'): ?>disabled=""<?php endif; ?>  type="hidden" name="id" value="<?php if($sortKey=='id'): ?><?php echo e($sortVal); ?><?php endif; ?>"></th>
                                        <th class="asc-desc" id="<?php if($sortKey=='product_name'): ?><?php echo e($sortVal); ?><?php endif; ?>"><?php echo e(__('admin.product_name')); ?><input <?php if($sortKey!='product_name'): ?>disabled=""<?php endif; ?>  type="hidden" name="product_name" value="<?php if($sortKey=='product_name'): ?><?php echo e($sortVal); ?><?php endif; ?>"></th>
                                        <th class="text-center asc-desc" id="<?php if($sortKey=='product_price'): ?><?php echo e($sortVal); ?><?php endif; ?>"><?php echo e(__('admin.product_price')); ?><input <?php if($sortKey!='product_price'): ?>disabled=""<?php endif; ?>  type="hidden" name="product_price" value="<?php if($sortKey=='product_price'): ?><?php echo e($sortVal); ?><?php endif; ?>"></th>
                                        <th class="text-center asc-desc" id="<?php if($sortKey=='product_quantity'): ?><?php echo e($sortVal); ?><?php endif; ?>"><?php echo e(__('admin.product_quantity_index')); ?><input <?php if($sortKey!='product_quantity'): ?>disabled=""<?php endif; ?>  type="hidden" name="product_quantity" value="<?php if($sortKey=='product_quantity'): ?><?php echo e($sortVal); ?><?php endif; ?>"></th>
                                        <th class="text-center"><?php echo e(__('admin.action')); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $__empty_1 = true; $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                    <tr id="<?php echo e($record->id); ?>">
                                        <td class="text-center"><?php echo e(ucfirst($record->id)); ?></td>
                                        <td><?php echo e(ucfirst($record->product_name)); ?></td>
                                        <td class="text-center"><?php echo e($record->product_price); ?></td>
                                        <td class="text-center">
                                            <?php if($record->product_quantity): ?>
                                            <?php echo e($record->product_quantity); ?>

                                            <?php else: ?>
                                            <span class='badge badge-danger'><?php echo e(__('admin.out_of_stock')); ?></span>
                                            <?php endif; ?>
                                        </td>

                                        <td class="text-center">
                                            <a href="<?php echo e(route('admin.product.edit',['id'=>$record->id])); ?>"><button type="button" class="<?php echo e(config('custom.btn-primary','btn btn-outline-primary btn-sm')); ?>" title="<?php echo e(__('admin.edit')); ?>"><i class="fas fa-edit"></i></button></a>
                                            <a onclick="showSweetAlert('<?php echo e(route('admin.product.delete',['id'=>$record->id])); ?>')"><button type="button" class="<?php echo e(config('custom.btn-danger','btn btn-outline-danger btn-sm')); ?>" title="<?php echo e(__('admin.delete')); ?>"><i class="fas fa-trash"></i></button></a>
                                        </td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                            <!-- /.card-body -->
                        </div>
                        <div class="table-pagination pt20 float-right">
                            <?php echo e($product->appends(request()->input())->links()); ?>

                        </div>
                    </form>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_specific_js'); ?>
<?php echo $__env->make('common/sorting', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout/sweetalert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout/datatable', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout/select2', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 
<script>
    $(document).ready(function ()
    {
    $(".select-category").select2({
    placeholder: "<?php echo e(__('admin.filter_by_category')); ?>",
            allowClear: true
    });
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout/main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\product_demo\resources\views/admin/product/index.blade.php ENDPATH**/ ?>