<link rel="stylesheet" href="<?php echo e(asset('plugins/datatables-rowreorder/css/rowReorder.bootstrap4.min.css')); ?>">
<script src="<?php echo e(asset('plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/datatables-rowreorder/js/dataTables.rowReorder.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')); ?>"></script>
<script>
$(document).ready(function ()
{
    var oTable = $('.normal-table').DataTable({
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": false,
        "responsive": true,
        "oLanguage": {
            "sEmptyTable": "<?php echo e(__('admin.no_record_found')); ?>"
        }
    });
});
</script><?php /**PATH C:\xampp\htdocs\product_demo\resources\views/layout/datatable.blade.php ENDPATH**/ ?>