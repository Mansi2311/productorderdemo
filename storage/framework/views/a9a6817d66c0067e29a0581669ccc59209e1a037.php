<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo e(route("admin.home.index")); ?>" class="brand-link">
        <img src="<?php echo e(Helper::getAppLogo()?Helper::getAppLogo(): ''); ?>" alt="<?php echo e(__('admin.logo')); ?>" class="brand-image img-circle elevation-3" onerror="this.onerror=null;this.src='<?php echo e(asset(Storage::url('default.png'))); ?>'">
        <span class="brand-text font-weight-light"><?php echo e(Helper::getAppName()?Helper::getAppName():config('app.name',__('admin.admin'))); ?></span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!--Super Admin-->
                <li class="nav-header"><?php echo e(strtoupper(Auth::user()->name)); ?></li>
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_super_admin'))): ?>
                <!--<li class="nav-header"><?php echo e(__('admin.menu_super_admin')); ?></li>-->
                <li class="nav-item has-treeview <?php echo e(request()->routeIs('super-admin.*') ?'menu-open':''); ?>">
                    <a href="#" class="nav-link <?php echo e(request()->routeIs('super-admin.*') ?'active':''); ?>">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p><?php echo e(__('admin.menu_authorization')); ?><i class="fas fa-angle-left right"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_super_admin_role'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('super-admin.role.index')); ?>" class="nav-link <?php echo e(request()->routeIs('super-admin.role.*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.menu_role')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_super_admin_permission'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('super-admin.permission.index')); ?>" class="nav-link <?php echo e(request()->routeIs('super-admin.permission*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.menu_permission')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_super_admin_role_permission'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('super-admin.role-permission.index')); ?>" class="nav-link <?php echo e(request()->routeIs('super-admin.role-permission*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.menu_role_permission')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_super_admin_user_role'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('super-admin.user-role.index')); ?>" class="nav-link <?php echo e(request()->routeIs('super-admin.user-role*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.menu_user_role')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>
                        <!--/.Dashboard-->
                    </ul>
                </li>
                <?php if(false): ?>
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_super_admin_dashboard'))): ?>
                <li class="nav-item">
                    <a href="<?php echo e(route('super-admin.super-admin-dashboard.index')); ?>" class="nav-link <?php echo e(request()->routeIs('super-admin.super-admin-dashboard.index') ?'active':''); ?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p><?php echo e(__('admin.super-dashboard')); ?></p>
                    </a>
                </li>
                <?php endif; ?> 
                <?php endif; ?>
                <?php endif; ?>
                <!--/.Super Admin-->
                <!--Admin-->
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_admin'))): ?>                
                <!--<li class="nav-header"><?php echo e(__('admin.admin')); ?></li>-->
                <!--Dashboard-->

                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_dashboard'))): ?>
                <li class="nav-item">
                    <a href="<?php echo e(route('admin.home.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.home.index') ?'active':''); ?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p><?php echo e(__('admin.menu_dashboard')); ?></p>
                    </a>
                </li>
                <?php endif; ?>

                <!--/.Dashboard-->

                <!--Master-->
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_master'))): ?>
                <li class="nav-item has-treeview <?php echo e(request()->routeIs('admin.master.*') ?'menu-open':''); ?>">
                    <a href="#" class="nav-link <?php echo e(request()->routeIs('admin.master.*') ?'active':''); ?>">
                        <i class="nav-icon fas fa-globe-asia"></i>
                        <p><?php echo e(__('admin.menu_master')); ?><i class="fas fa-angle-left right"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_master_country'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('admin.master.country.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.master.country.*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.menu_country')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>
                        <!--Added code by nayna 10-03-2021-->
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_master_state'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('admin.master.state.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.master.state.*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.menu_state')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>
                        <!--End code by nayna 10-03-2021-->
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_master_city'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('admin.master.city.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.master.city.*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.menu_city')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>
                        <!--code by maharshi 08-03-2021-->
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_master_order_status'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('admin.master.order-status.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.master.order-status.*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.order_status')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>
                        <!--End code by maharshi 08-03-2021-->
                        <!--Added code by nayna 08-03-2021-->

                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_master_currency'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('admin.master.currency.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.master.currency.*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.menu_currency')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>
                        <!--End code by nayna 08-03-2021-->
                        <!--code by maharshi 09-03-2021-->
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_master_courier_company'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('admin.master.courier-company.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.master.courier-company.*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.courier_company')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>

                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_master_shipping_method'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('admin.master.shipping-method.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.master.shipping-method.*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.shipping')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>
                        <!--End code by nayna 09-03-2021-->
                        <?php if(false): ?>
                        <!--code by maharshi 10-03-2021-->
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_master_payment_method'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('admin.master.payment-method.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.master.payment-method.*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.payment_method')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php endif; ?>
                        <!--End code by maharshi 10-03-2021-->
                    </ul>
                </li>
                <?php endif; ?>
                <!--/.Master-->
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_customer'))): ?>
                <li class="nav-item">
                    <a href="<?php echo e(route('admin.customer.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.customer.*') ?'active':''); ?>">
                        <i class="nav-icon fas fa-user"></i>
                        <p><?php echo e(__('admin.menu_customer')); ?></p>
                    </a>
                </li>
                <?php endif; ?>
                <!--Product-->
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_product'))): ?>
                <li class="nav-item">
                    <a href="<?php echo e(route('admin.product.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.product.*') ?'active':''); ?>">
                        <i class="nav-icon fas fa-user"></i>
                        <p><?php echo e(__('admin.menu_product')); ?></p>
                    </a>
                </li>
                <?php endif; ?>
                <!--Product-->
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_order'))): ?>
                <li class="nav-item">
                    <a href="<?php echo e(route('admin.order-status.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.order-status.*') ?'active':''); ?>">
                        <i class="nav-icon fas fa-user"></i>
                        <p><?php echo e(__('admin.menu_order_status')); ?></p>
                    </a>
                </li>
                <?php endif; ?>
                </li>
                <!--/.Product-->

                <!--Setting-->
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_general_setting'))): ?>
                <li class="nav-item has-treeview <?php echo e(request()->routeIs('admin.general-setting.*') ?'menu-open':''); ?>">
                    <a href="#" class="nav-link <?php echo e(request()->routeIs('admin.general-setting.*') ?'active':''); ?>">
                        <i class="nav-icon fas fa-tools"></i>
                        <p><?php echo e(__('admin.menu_general_setting')); ?><i class="fas fa-angle-left right"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_general_setting_setting'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('admin.general-setting.setting.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.general-setting.setting.*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.menu_setting')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>
                    </ul>
                    <!--code add by mayur 11-03-2021-->
                    <ul class="nav nav-treeview">
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_general_setting_media_settings'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('admin.general-setting.media-setting.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.general-setting.media-setting.*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.menu_media_setting')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>
                    </ul>
                    <!--code end by mayur 11-03-2021-->
                    <!--//code add by mayur 10-03-2021-->
                    <ul class="nav nav-treeview">
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_general_setting_social_media_settings'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('admin.general-setting.social-media-setting.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.general-setting.social-media-setting.*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.menu_social_media_setting')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>
                    </ul>
                    <!--code end by mayur 10-03-2021-->
                    <!--code by mayur 09-03-2021-->
                    <ul class="nav nav-treeview">
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_general_setting_about_us'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('admin.general-setting.about-us.add')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.general-setting.about-us.*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.menu_about_us')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>                       
                    </ul>
                    <ul class="nav nav-treeview">
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_general_setting_contact_us'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('admin.general-setting.contact-us.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.general-setting.contact-us.*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.menu_contact_us')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>                       
                    </ul>
                    <ul class="nav nav-treeview">
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_general_setting_privacy_policy'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('admin.general-setting.privacy-policy.add')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.general-setting.privacy-policy.*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.menu_privacy_policy')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>                       
                    </ul>
                    <ul class="nav nav-treeview">
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_general_setting_return_policy'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('admin.general-setting.return-policy.add')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.general-setting.return-policy.*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.menu_return_policy')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>                       
                    </ul>
                    <ul class="nav nav-treeview">
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_general_setting_term_and_condition'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(route('admin.general-setting.term.add')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.general-setting.term.*') ?'active':''); ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo e(__('admin.menu_term')); ?></p>
                            </a>
                        </li>
                        <?php endif; ?>                       
                    </ul>
                    <!--code by mayur 09-03-2021-->
                    <!--Customer: code add by nayna 17-03-2021-->
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_customer'))): ?>
                <li class="nav-item">
                    <a href="<?php echo e(route('admin.customer.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.customer.*') ?'active':''); ?>">
                        <i class="nav-icon fas fa-user"></i>
                        <p><?php echo e(__('admin.menu_customer')); ?></p>
                    </a>
                </li>
                <?php endif; ?>
                <!--Customer: code end by nayna 17-03-2021-->
                <!--Blog: code add by nayna 23-03-2021-->
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.view_blog'))): ?>
                <li class="nav-item">
                    <a href="<?php echo e(route('admin.blog.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.blog.*') ?'active':''); ?>">
                        <i class="nav-icon fas fa-database"></i>
                        <p><?php echo e(__('admin.menu_blog')); ?></p>
                    </a>
                </li>
                <?php endif; ?>
                <!--Customer: code end by nayna 17-03-2021-->
                <?php endif; ?>
                </li>

                <!--/.Setting-->

                <?php endif; ?>
                <!--/.Admin-->
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside><?php /**PATH C:\xampp\htdocs\product_order_demo\resources\views/layout/sidebar.blade.php ENDPATH**/ ?>