<aside class="main-sidebar sidebar-light-primary elevation-4">
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-header"><?php echo e(strtoupper(Auth::user()->name)); ?></li>
                <li class="nav-item">
                    <a href="<?php echo e(route('admin.customer.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.customer.*') ?'active':''); ?>">
                        <i class="nav-icon fas fa-user"></i>
                        <p><?php echo e(__('admin.menu_customer')); ?></p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo e(route('admin.product.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.product.*') ?'active':''); ?>">
                        <i class="nav-icon fas fa-user"></i>
                        <p><?php echo e(__('admin.menu_product')); ?></p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo e(route('admin.order-status.index')); ?>" class="nav-link <?php echo e(request()->routeIs('admin.order-status.*') ?'active':''); ?>">
                        <i class="nav-icon fas fa-user"></i>
                        <p><?php echo e(__('admin.menu_order_status')); ?></p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside><?php /**PATH C:\xampp\htdocs\product_demo\resources\views/layout/sidebar.blade.php ENDPATH**/ ?>