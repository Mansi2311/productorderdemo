<?php if(isset($country)): ?>
<option disabled selected="">Select State </option>
<?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<option value="<?php echo e($value->id); ?>" <?php echo e((collect($state_id)->contains($value->id)) ? 'selected':''); ?>><?php echo e(ucfirst($value->getStateDescription()->state_name)); ?></option>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
<?php /**PATH C:\xampp\htdocs\japan_ecomm_admin\resources\views/admin/customer/get_state.blade.php ENDPATH**/ ?>