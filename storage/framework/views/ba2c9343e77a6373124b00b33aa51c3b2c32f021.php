
<?php $__env->startSection('page_title',  trans('admin.product')); ?>
<?php $__env->startSection('additional_css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/select2/css/select2.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <?php echo $__env->make('layout/toaster', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?php echo e(__('admin.product')); ?></h1>
                </div>
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home.index')); ?>"><?php echo e(__('admin.home')); ?></a></li>
                        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.product.product.index')); ?>"><?php echo e(__('admin.product')); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo e(__('admin.list')); ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <form class="list-table" method="get" action="<?php echo e(route('admin.product.product.index')); ?>">
                        <?php if(false): ?>
                        <div class="row">
                            <div class="col-12">
                                <section class="content">
                                    <div class="card <?php echo e(config('custom.card-primary')); ?>">                                      
                                        <div class="card-body pb-0">
                                            <div class="form-group">
                                                <select name="_c[]"  class="form-control select2 select-category <?php echo e(config('custom.select2-css')); ?>" data-dropdown-css-class="<?php echo e(config('custom.select2-css')); ?>" multiple="" style="width: 100%">
                                                    <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php
                                                    $categoryName = $category->getCategoryDescription();
                                                    ?>
                                                    <option value="<?php echo e($category->id); ?>" <?php echo e((collect($filterCategory)->contains($category->id)) ? 'selected':''); ?>>
                                                        <?php if($category->parent_id): ?>
                                                        <?php
                                                        $parentCategory = $category->getParent($category->parent_id);
                                                        $parentCategoryName = $parentCategory->getCategoryDescription();
                                                        ?>
                                                        <?php echo e($parentCategoryName?ucfirst($parentCategoryName->category_name).' > ':''); ?>

                                                        <?php endif; ?>
                                                        <?php echo e($categoryName?ucfirst($categoryName->category_name):''); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <div class="icheck-primary">
                                                    <input value="1" type="checkbox" name="OOSFilter" id="OOSFilter" <?php echo e($outOfStockFilter?'checked':''); ?>>
                                                    <label for="OOSFilter"><?php echo e(__('admin.out_of_stock_filter')); ?>

                                                    </label>
                                                </div>
                                            </div>
                                        </div>                                        
                                        <div class="card-footer">
                                            <div class="btn-toolbar float-right">
                                                <div class="btn-group">
                                                    <button type="submit" class="<?php echo e(config('custom.btn-success-form')); ?>" title="<?php echo e(__('admin.search')); ?>"><?php echo e(__('admin.search')); ?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>                       
                        <input type="hidden" name="id" value="<?php echo e($productId); ?>">
                        <?php endif; ?>
                        <div class="card <?php echo e(config('custom.card-primary')); ?>">
                            <div class="table-card card-header pl-0">
                                <div class="card-tools w-100">
                                    <?php echo $__env->make('common/paginate', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    <div class="input-group input-group-sm float-right custom-search">
                                        <input type="text" name="q" class="form-control float-right float-sm-right" placeholder="<?php echo e(__('admin.search')); ?>" value="<?php echo e(isset($searchText)?$searchText:''); ?>">

                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default" title="<?php echo e(__('admin.search')); ?>"><i class="fas fa-search"></i></button>
                                        </div>
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->any([config('custom_middleware.add_product')])): ?>
                                        <a class="pl-2"  href="<?php echo e(route('admin.product.product.add')); ?>"><button type="button" class="<?php echo e(config('custom.btn-primary','btn btn-outline-primary')); ?> add-new-btn" title="<?php echo e(__('admin.add')); ?>"><?php echo e(__('admin.add')); ?></button></a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <table data-model="AdminProduct" class="table sorting-table table-striped">
                                <thead>
                                    <tr>
                                        <th class="asc-desc text-center" id="<?php if($sortKey=='sorting'): ?><?php echo e($sortVal); ?><?php endif; ?>"><?php echo e(__('admin.index')); ?><input <?php if($sortKey!='sorting'): ?>disabled=""<?php endif; ?>  type="hidden" name="sorting" value="<?php if($sortKey=='sorting'): ?><?php echo e($sortVal); ?><?php endif; ?>"></th>
                                        <th class="asc-desc" id="<?php if($sortKey=='name'): ?><?php echo e($sortVal); ?><?php endif; ?>"><?php echo e(__('admin.name')); ?><input <?php if($sortKey!='name'): ?>disabled=""<?php endif; ?>  type="hidden" name="name" value="<?php if($sortKey=='name'): ?><?php echo e($sortVal); ?><?php endif; ?>"></th>
                                        <th class="asc-desc" id="<?php if($sortKey=='product_sku'): ?><?php echo e($sortVal); ?><?php endif; ?>"><?php echo e(__('admin.sku')); ?><input <?php if($sortKey!='product_sku'): ?>disabled=""<?php endif; ?>  type="hidden" name="product_sku" value="<?php if($sortKey=='product_sku'): ?><?php echo e($sortVal); ?><?php endif; ?>"></th>
                                        <th class="text-center asc-desc" id="<?php if($sortKey=='product_regular_price'): ?><?php echo e($sortVal); ?><?php endif; ?>"><?php echo e(__('admin.regular_price')); ?><input <?php if($sortKey!='product_regular_price'): ?>disabled=""<?php endif; ?>  type="hidden" name="product_regular_price" value="<?php if($sortKey=='product_regular_price'): ?><?php echo e($sortVal); ?><?php endif; ?>"></th>
                                        <th class="text-center asc-desc" id="<?php if($sortKey=='product_sale_price'): ?><?php echo e($sortVal); ?><?php endif; ?>"><?php echo e(__('admin.selling_price')); ?><input <?php if($sortKey!='product_sale_price'): ?>disabled=""<?php endif; ?>  type="hidden" name="product_sale_price" value="<?php if($sortKey=='product_sale_price'): ?><?php echo e($sortVal); ?><?php endif; ?>"></th>
                                        <th class="text-center asc-desc" id="<?php if($sortKey=='product_quantity'): ?><?php echo e($sortVal); ?><?php endif; ?>"><?php echo e(__('admin.product_quantity_index')); ?><input <?php if($sortKey!='product_quantity'): ?>disabled=""<?php endif; ?>  type="hidden" name="product_quantity" value="<?php if($sortKey=='product_quantity'): ?><?php echo e($sortVal); ?><?php endif; ?>"></th>
                                        <th class="asc-desc" id="<?php if($sortKey=='category'): ?><?php echo e($sortVal); ?><?php endif; ?>"><?php echo e(__('admin.category')); ?><input <?php if($sortKey!='category'): ?>disabled=""<?php endif; ?>  type="hidden" name="category" value="<?php if($sortKey=='category'): ?><?php echo e($sortVal); ?><?php endif; ?>"></th>
                                        <th class="text-center"><?php echo e(__('admin.image')); ?></th>
                                        <th class="asc-desc text-center" id="<?php if($sortKey=='is_active'): ?><?php echo e($sortVal); ?><?php endif; ?>"><?php echo e(__('admin.status')); ?><input <?php if($sortKey!='is_active'): ?>disabled=""<?php endif; ?>  type="hidden" name="is_active" value="<?php if($sortKey=='is_active'): ?><?php echo e($sortVal); ?><?php endif; ?>"></th>
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->any([config('custom_middleware.edit_product_product'),config('custom_middleware.delete_product_product')])): ?>
                                        <th class="text-center"><?php echo e(__('admin.action')); ?></th>
                                        <?php endif; ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $__empty_1 = true; $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                    <tr id="<?php echo e($record->id); ?>">
                                        <td class="text-center"><?php echo e($record->sorting); ?></td>
                                        <?php
                                        $productName     = $record->getProductDescription();
                                        $productCategory = $record->getProductCategories();
                                        $defaultImage    = $record->getDefaultImage();
                                        ?>
                                        <td><?php echo e($productName?ucfirst($productName->products_name):''); ?></td>
                                        <td><?php echo e($record->product_sku); ?></td>
                                        <td class="text-center"><?php echo e($record->product_regular_price); ?></td>
                                        <td class="text-center"><?php echo e($record->product_sale_price); ?></td>
                                        <td class="text-center">
                                            <?php if($record->product_quantity): ?>
                                            <?php echo e($record->product_quantity); ?>

                                            <?php else: ?>
                                            <span class='badge badge-danger'><?php echo e(__('admin.out_of_stock')); ?></span>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php $__currentLoopData = $productCategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php
                                            $categoryDetail  = $category->getCategory();
                                            if ($categoryDetail)
                                            {
                                                $categoryDescription = $categoryDetail->getCategoryDescription();
                                                if ($categoryDescription)
                                                {
                                                    echo $categoryDescription->category_name . "<br/>";
                                                }
                                            }
                                            ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </td>
                                        <td class="text-center reorder-exclude"><img class="logo-table text-center" src="<?php echo e(asset(Storage::url($defaultImage?$defaultImage->image_thumb_url:""))); ?>" onclick="showDetails(this.src)" onerror="this.onerror=null;this.src='<?php echo e(asset(Storage::url('default.png'))); ?>'"/>
                                        <td class="text-center">
                                            <?php if($record->is_active): ?>
                                            <span class="<?php echo e(config('custom.badge-success','badge bg-success')); ?>"><?php echo e(__('admin.active')); ?></span>
                                            <?php else: ?>
                                            <span class="<?php echo e(config('custom.badge-danger','badge bg-danger')); ?>"><?php echo e(__('admin.in_active')); ?></span> 
                                            <?php endif; ?>
                                        </td>
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->any([config('custom_middleware.edit_product_product'),config('custom_middleware.delete_product_product')])): ?>
                                        <td class="text-center">
                                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.edit_product_product'))): ?>
                                            <a href="<?php echo e(route('admin.product.product.edit',['id'=>$record->id])); ?>"><button type="button" class="<?php echo e(config('custom.btn-primary','btn btn-outline-primary btn-sm')); ?>" title="<?php echo e(__('admin.edit')); ?>"><i class="fas fa-edit"></i></button></a>
                                            <?php if($record->is_active): ?>
                                            <a href="<?php echo e(route('admin.product.product.change-status',['id'=>$record->id,'active'=>0])); ?>"><button type="button" class="<?php echo e(config('custom.btn-danger','btn btn-outline-danger btn-sm')); ?>" title="<?php echo e(__('admin.click_in_active')); ?>"><i class="fas fa-times"></i></button></a>
                                            <?php else: ?>
                                            <a href="<?php echo e(route('admin.product.product.change-status',['id'=>$record->id,'active'=>1])); ?>"><button type="button" class="<?php echo e(config('custom.btn-success','btn btn-outline-success btn-sm')); ?>" title="<?php echo e(__('admin.click_active')); ?>"><i class="fas fa-check"></i></button></a>
                                            <?php endif; ?>
                                            <?php endif; ?>
                                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check(config('custom_middleware.delete_product_product'))): ?>
                                            <a onclick="showSweetAlert('<?php echo e(route('admin.product.product.delete',['id'=>$record->id])); ?>')"><button type="button" class="<?php echo e(config('custom.btn-danger','btn btn-outline-danger btn-sm')); ?>" title="<?php echo e(__('admin.delete')); ?>"><i class="fas fa-trash"></i></button></a>
                                            <?php endif; ?>
                                        </td>
                                        <?php endif; ?>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                            <!-- /.card-body -->
                        </div>
                        <div class="table-pagination pt20 float-right">
                            <?php echo e($product->appends(request()->input())->links()); ?>

                        </div>
                    </form>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_specific_js'); ?>
<?php echo $__env->make('common/sorting', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout/sweetalert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout/image_preview', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout/datatable', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout/select2', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 
<script>
    $(document).ready(function ()
    {
    $(".select-category").select2({
    placeholder: "<?php echo e(__('admin.filter_by_category')); ?>",
            allowClear: true
    });
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout/main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\japan_ecomm_admin\resources\views/admin/product/product/index.blade.php ENDPATH**/ ?>