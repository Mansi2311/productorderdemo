<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo e(__('admin.login')); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?php echo e(asset('plugins/fontawesome-free/css/all.min.css')); ?>">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo e(asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('dist/css/adminlte.min.css')); ?>">
        <style>
            .page-logo
            {
                height: 100px;
                width: 100px;
            }
        </style>
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
         
            <div class="card">
                <div class="card-body login-card-body">
                    <p class="login-box-msg"><?php echo e(__('admin.login_text')); ?></p>
                    <?php echo $__env->make('layout/toaster', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <?php if($errors->has('email') || $errors->has('password')): ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo e($errors->first('email')?$errors->first('email'):$errors->first('password')); ?>

                    </div>
                    <?php endif; ?>
                    <form action="<?php echo e(route('admin.login')); ?>" method="post">
                        <?php echo csrf_field(); ?>
                        <div class="input-group mb-3">
                            <input name="email" type="text" class="form-control <?php echo e($errors->has('email') ?'is-invalid':''); ?>" placeholder="<?php echo e(__('admin.email_username')); ?>" value="<?php echo e(old('email')); ?>">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input name="password" type="password" class="form-control <?php echo e($errors->has('password') ?'is-invalid':''); ?>" placeholder="<?php echo e(__('admin.password')); ?>">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-8">
                                <p class="mt-2">
                                    <a></a>
                                </p>
                            </div>
                            <div class="col-4">
                                <button type="submit" class="<?php echo e(config('custom.btn-primary-form')); ?> btn-block"><?php echo e(__('admin.login')); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script src="<?php echo e(asset('plugins/jquery/jquery.min.js')); ?>"></script>
        <script src="<?php echo e(asset('plugins/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>
        <script src="<?php echo e(asset('dist/js/adminlte.min.js')); ?>"></script>
    </body>
</html>
<?php /**PATH C:\xampp\htdocs\product_demo\resources\views/auth/login.blade.php ENDPATH**/ ?>