
<?php $__env->startSection('page_title',  trans('admin.customer')); ?>
<?php $__env->startSection('additional_css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/select2/css/select2.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('public/custom/css/custom.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layout/toaster', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?php echo e(__('admin.customer')); ?></h1>
                </div>
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home.index')); ?>"><?php echo e(__('admin.home')); ?></a></li>
                        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.customer.index')); ?>"><?php echo e(__('admin.customer')); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo e(__('admin.edit')); ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card <?php echo e(config('custom.card-primary')); ?>">
                <div class="card-header">
                    <h3 class="card-title"><?php echo e(__('admin.edit')); ?></h3>
                </div>
                <form method="post" action="<?php echo e(route('admin.customer.update',['id' => $singleRecord->id])); ?>">
                    <?php echo csrf_field(); ?>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group"> 
                                    <label><?php echo e(__('admin.first_name')); ?></label><span class="required"></span>
                                    <input type="text" name="first_name" placeholder="<?php echo e(__('admin.first_name')); ?>" class="form-control text-input length-30 <?php echo e($errors->has('first_name') ?'is-invalid':''); ?>"  value="<?php echo e(old('first_name',$singleRecord->first_name)); ?>">
                                    <?php $__errorArgs = ['first_name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo $message; ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group"> 
                                    <label><?php echo e(__('admin.last_name')); ?></label><span class="required"></span>
                                    <input type="text" name="last_name" placeholder="<?php echo e(__('admin.last_name')); ?>" class="form-control text-input length-30 <?php echo e($errors->has('last_name') ?'is-invalid':''); ?>" value="<?php echo e(old('last_name',$singleRecord->last_name)); ?>">
                                    <?php $__errorArgs = ['last_name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo $message; ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-12">
                                <div class="form-group"> 
                                    <label><?php echo e(__('admin.telephone')); ?></label><span class="required"></span>
                                    <input type="text" name="mobile" placeholder="<?php echo e(__('admin.telephone')); ?>" class="form-control number-input length-10 <?php echo e($errors->has('mobile') ?'is-invalid':''); ?>" value="<?php echo e(old('mobile',$singleRecord->mobile)); ?>">
                                    <?php $__errorArgs = ['mobile'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo $message; ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group"> 
                                    <label><?php echo e(__('admin.email_address')); ?></label><span class="required"></span>
                                    <input type="email" name="email" placeholder="<?php echo e(__('admin.email_address')); ?>" class="form-control text-input length-50 <?php echo e($errors->has('email') ?'is-invalid':''); ?>" value="<?php echo e(old('email',$singleRecord->email)); ?>">
                                    <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo $message; ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-12">
                                <div class="form-group">
                                    <label class="radio-label"><?php echo e(__('admin.gender')); ?></label>
                                    <div class="radiobox-row">
                                        <label class="radiobox-col">
                                            <input type="radio" checked="checked" id="male" name="gender"  value="1" <?php echo e(old('gender',$singleRecord->gender) == '1' ? 'checked' : ''); ?> >
                                            <span class="checkmark"></span>
                                            <?php echo e(__('admin.male')); ?>

                                        </label>
                                        <label class="radiobox-col">
                                            <input type="radio" name="gender" id="female" value="0" <?php echo e(old('gender',$singleRecord->gender) == '0' ? 'checked' : ''); ?> >
                                            <span class="checkmark"></span>
                                            <?php echo e(__('admin.female')); ?>

                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group"> 
                                <label><?php echo e(__('admin.address')); ?></label><span class="required"></span>
                                <textarea type="text" name="address" placeholder="<?php echo e(__('admin.address')); ?>" class="form-control text-input length-50 <?php echo e($errors->has('address') ?'is-invalid':''); ?>" ><?php echo e(old('address',$singleRecord->address)); ?></textarea>
                                <?php $__errorArgs = ['address'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <div class="text-red"><?php echo $message; ?></div>
                                <?php endif; ?>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <span class="float-left"><?php echo __('admin.additional_notes'); ?></span>
                        <div class="btn-toolbar float-right">
                            <div class="btn-group mr-2">
                                <a href="<?php echo e(route('admin.customer.index')); ?>" class="<?php echo e(config('custom.btn-danger-form')); ?>" title="<?php echo e(__('admin.cancel')); ?>"><?php echo e(__('admin.cancel')); ?></a>
                            </div>
                            <div class="btn-group">
                                <button type="submit" class="<?php echo e(config('custom.btn-success-form')); ?>" title="<?php echo e(__('admin.update')); ?>"><?php echo e(__('admin.update')); ?></button>
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </section>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_specific_js'); ?>
<?php echo $__env->make('layout/select2', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script>

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout/main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\product_order_demo\resources\views/admin/customer/edit.blade.php ENDPATH**/ ?>