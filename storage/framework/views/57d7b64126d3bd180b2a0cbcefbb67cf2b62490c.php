
<?php $__env->startSection('page_title',  trans('admin.customer')); ?>
<?php $__env->startSection('additional_css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/select2/css/select2.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/datetimepicker/bootstrap-4.min.css')); ?>" crossorigin="anonymous" />
<link rel="stylesheet" href="<?php echo e(asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('public/custom/css/custom.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layout/toaster', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?php echo e(__('admin.customer')); ?></h1>
                </div>
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home.index')); ?>"><?php echo e(__('admin.home')); ?></a></li>
                        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.customer.index')); ?>"><?php echo e(__('admin.customer')); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo e(__('admin.add')); ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card <?php echo e(config('custom.card-primary')); ?>">
                <div class="card-header">
                    <h3 class="card-title"><?php echo e(__('admin.add_new')); ?></h3>
                </div>
                <form method="post" action="<?php echo e(route('admin.customer.add')); ?>">
                    <?php echo csrf_field(); ?>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                                <div class="form-group"> 
                                    <label><?php echo e(__('admin.first_name')); ?></label><span class="required"></span>
                                    <input type="text" name="first_name" placeholder="<?php echo e(__('admin.first_name')); ?>" class="form-control text-input length-30 <?php echo e($errors->has('first_name') ?'is-invalid':''); ?>"  value="<?php echo e(old('first_name')); ?>">
                                    <?php $__errorArgs = ['first_name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo $message; ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <div class="form-group"> 
                                    <label><?php echo e(__('admin.last_name')); ?></label><span class="required"></span>
                                    <input type="text" name="last_name" placeholder="<?php echo e(__('admin.last_name')); ?>" class="form-control text-input length-30 <?php echo e($errors->has('last_name') ?'is-invalid':''); ?>" value="<?php echo e(old('last_name')); ?>">
                                    <?php $__errorArgs = ['last_name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo $message; ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <div class="form-group">
                                    <label for="name" class=" control-label"><?php echo e(__('admin.dob')); ?></label><span class="required"></span>
                                    <input  data-toggle="datetimepicker" data-target=".start-date" class="start-date form-control datetimepicker-input  <?php echo e($errors->has('birth_date') ?'is-invalid':''); ?>" placeholder="<?php echo e(__('admin.dob')); ?>" name="birth_date" type="text" value="<?php echo e(old('birth_date')); ?>" readonly="">
                                    <?php $__errorArgs = ['birth_date'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo e($message); ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <div class="form-group">
                                    <label><?php echo e(__('admin.country')); ?></label><span class="required"></span>
                                    <select  id="country_id" class="form-control select2 select-is-feature <?php echo e(config('custom.select2-css')); ?>" data-dropdown-css-class="<?php echo e(config('custom.select2-css')); ?>" name="country_id">
                                        <?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                        $description = $val->getCountryDescription();
                                        ?>
                                        <option value="<?php echo e($val->id); ?>" <?php echo e((collect(old('country_id'))->contains($val->id)) ? 'selected':''); ?>><?php echo e(ucfirst($description->country_name)); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <?php $__errorArgs = ['country_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo e($message); ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                                <div class="form-group">
                                    <label><?php echo e(__('admin.state')); ?></label><span class="required"></span>
                                    <select id="state_id" class="form-control select2 select-is-feature <?php echo e(config('custom.select2-css')); ?>" data-dropdown-css-class="<?php echo e(config('custom.select2-css')); ?>" name="state_id" value="<?php echo e(old('state_id')); ?>">
                                        <option value="" selected="">Select State</option>
                                    </select>
                                    <?php $__errorArgs = ['state_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo e($message); ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <div class="form-group"> 
                                    <label><?php echo e(__('admin.telephone')); ?></label><span class="required"></span>
                                    <input type="text" name="mobile" placeholder="<?php echo e(__('admin.telephone')); ?>" class="form-control number-input length-10 <?php echo e($errors->has('mobile') ?'is-invalid':''); ?>" value="<?php echo e(old('mobile')); ?>">
                                    <?php $__errorArgs = ['mobile'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo $message; ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <div class="form-group"> 
                                    <label><?php echo e(__('admin.email_address')); ?></label><span class="required"></span>
                                    <input type="email" name="email" placeholder="<?php echo e(__('admin.email_address')); ?>" class="form-control text-input length-50 <?php echo e($errors->has('email') ?'is-invalid':''); ?>" value="<?php echo e(old('email')); ?>">
                                    <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo $message; ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <div class="form-group"> 
                                    <label><?php echo e(__('admin.password')); ?></label><span class="required"></span>
                                    <input type="password" name="password" placeholder="<?php echo e(__('admin.password')); ?>" class="form-control text-input <?php echo e($errors->has('password') ?'is-invalid':''); ?>" value="<?php echo e(old('password')); ?>">
                                    <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo $message; ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group">
                                <label class="radio-label"><?php echo e(__('admin.gender')); ?></label>
                                <div class="radiobox-row">
                                    <label class="radiobox-col">
                                        <input type="radio" checked="checked" id="male" name="gender"  value="1" <?php echo e(old('gender') == '1' ? 'checked' : ''); ?> >
                                        <span class="checkmark"></span>
                                        <?php echo e(__('admin.male')); ?>

                                    </label>
                                    <label class="radiobox-col">
                                        <input type="radio" name="gender" id="female" value="0" <?php echo e(old('gender') == '0' ? 'checked' : ''); ?> >
                                        <span class="checkmark"></span>
                                        <?php echo e(__('admin.female')); ?>

                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <span class="float-left"><?php echo __('admin.additional_notes'); ?></span>
                        <div class="btn-toolbar float-right">
                            <div class="btn-group mr-2">
                                <a href="<?php echo e(route('admin.customer.index')); ?>" class="<?php echo e(config('custom.btn-danger-form')); ?>" title="<?php echo e(__('admin.cancel')); ?>"><?php echo e(__('admin.cancel')); ?></a>
                            </div>
                            <div class="btn-group">
                                <button type="submit" class="<?php echo e(config('custom.btn-success-form')); ?>" title="<?php echo e(__('admin.submit')); ?>"><?php echo e(__('admin.submit')); ?></button>
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </section>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_specific_js'); ?>
<?php echo $__env->make('layout/select2', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script src="<?php echo e(asset('plugins/moment/moment.min.js')); ?>" crossorigin="anonymous"></script>
<script src="<?php echo e(asset('plugins/datetimepicker/bootstrap-4.js')); ?>" crossorigin="anonymous"></script>
<script>
/*
 * Datepicker
 */
startDateTime();
function startDateTime()
{
    $(".start-date").datetimepicker({
        format: "<?php echo e(config('custom.js_date_format')); ?>",
        maxDate: moment("<?php echo e(date('Y-m-d')); ?>"),
        ignoreReadonly: true,
        buttons: {
            showClear: false,
        },
        icons: {
            clear: 'fa fa-trash',
        },
    });
    $(".start-date").val("<?php echo e(old('birth_date')); ?>");
    $(".start-time").datetimepicker({
        format: "<?php echo e(config('custom.js_time_format')); ?>",
        ignoreReadonly: true,
        buttons: {
            showClear: false,
        },
        icons: {
            clear: 'fa fa-trash',
        },
    });
    $(".start-time").val("<?php echo e(date(config('custom.php_time_format'))); ?>")
}
//country dropdown
$("#country_id").change(function ()
{
    var countryID = $(this).find("option:selected").val();
    get_state();
});
get_state();
//state selected
function get_state()
{
    var stateID = ($("[name=country_id]").find("option:selected").val(), "<?php echo e(old('state_id')); ?>");
    var countryID = $('#country_id').find("option:selected").val();
    if (countryID)
    {
        $.ajax({
            type: "get",
            url: "<?php echo e(route('admin.customer.state-bind')); ?>",
            data: {'countryID': countryID, 'stateID': stateID},
            success: function (res)
            {
                if (res)
                {
                    $("#state_id").empty();
                    $("#state_id").append(res);
                } else
                {
                    $("#state_id").empty();
                }
            }
        });
    }
}
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout/main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\japan_ecomm_admin\resources\views/admin/customer/add.blade.php ENDPATH**/ ?>