
<?php $__env->startSection('page_title',  trans('admin.product')); ?>
<?php $__env->startSection('additional_css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/select2/css/select2.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/datetimepicker/bootstrap-4.min.css')); ?>" crossorigin="anonymous" />
<link rel="stylesheet" href="<?php echo e(asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layout/toaster', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?php echo e(__('admin.product')); ?></h1>
                </div>
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home.index')); ?>"><?php echo e(__('admin.home')); ?></a></li>
                        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.product.product.index')); ?>"><?php echo e(__('admin.product')); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo e(__('admin.add')); ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card <?php echo e(config('custom.card-primary')); ?>">
                <div class="card-header">
                    <h3 class="card-title"><?php echo e(__('admin.add')); ?></h3>
                </div>
                <form method="post" action="<?php echo e(route('admin.product.product.add')); ?>" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <div class="card-body">
                        <div class="row">
                            <?php $__currentLoopData = $languageList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-md-6 col-xs-12">
                                <div class="">
                                    <div class="form-group">
                                        <label><?php echo e(__('admin.product_name')); ?> (<?php echo e(ucfirst($language->name)); ?>)</label><span class="required"></span>
                                        <input name="product_name_<?php echo e($language->id); ?>" type="text" class="form-control <?php echo e($errors->has('product_name_'.$language->id) ?'is-invalid':''); ?>" placeholder="<?php echo e(__('admin.product_name')); ?> (<?php echo e(ucfirst($language->name)); ?>)" value="<?php echo e(old('product_name_'.$language->id)); ?>">
                                        <?php $__errorArgs = ['product_name_'.$language->id];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <div class="text-red"><?php echo e($message); ?></div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="form-group">
                                        <label><?php echo e(__('admin.product_description')); ?>  (<?php echo e(ucfirst($language->name)); ?>)</label>
                                        <textarea  class="textarea form-control <?php echo e($errors->has('product_description_'.$language->id) ?'is-invalid':''); ?>" placeholder="<?php echo e(__('admin.product_description')); ?> (<?php echo e(ucfirst($language->name)); ?>)" id="product_description" name="product_description_<?php echo e($language->id); ?>" rows="4" cols="300"><?php echo e(old('product_description_'.$language->id)); ?></textarea>
                                        <?php $__errorArgs = ['product_description_'.$language->id];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <div class="text-red"><?php echo e($message); ?></div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="form-group">
                                        <label><?php echo e(__('admin.product-detail')); ?>  (<?php echo e(ucfirst($language->name)); ?>)</label>
                                        <textarea  class="textarea form-control <?php echo e($errors->has('product_detail_'.$language->id) ?'is-invalid':''); ?>" placeholder="<?php echo e(__('admin.product-detail')); ?> (<?php echo e(ucfirst($language->name)); ?>)" name="product_detail_<?php echo e($language->id); ?>" rows="4" cols="300"><?php echo e(old('product_detail_'.$language->id)); ?></textarea>
                                        <?php $__errorArgs = ['product_detail_'.$language->id];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <div class="text-red"><?php echo e($message); ?></div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="form-group">
                                        <label><?php echo e(__('admin.additional-info')); ?>  (<?php echo e(ucfirst($language->name)); ?>)</label>
                                        <textarea  class="textarea form-control <?php echo e($errors->has('additional_info_'.$language->id) ?'is-invalid':''); ?>" placeholder="<?php echo e(__('admin.additional-info')); ?> (<?php echo e(ucfirst($language->name)); ?>)" name="additional_info_<?php echo e($language->id); ?>" rows="4" cols="300"><?php echo e(old('additional_info_'.$language->id)); ?></textarea>
                                        <?php $__errorArgs = ['additional_info_'.$language->id];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <div class="text-red"><?php echo e($message); ?></div>
                                        <?php endif; ?>
                                    </div>
                                </div>                              
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-3">
                                <div class="form-group">
                                    <label><?php echo e(__('admin.product_code')); ?></label><span class="required"></span>
                                    <input class="form-control length-10" placeholder="<?php echo e(__('admin.product_code')); ?>" name="product_code" type="text" value="<?php echo e(old('product_code')); ?>">
                                    <?php $__errorArgs = ['product_code'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo e($message); ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form-group">
                                    <label><?php echo e(__('admin.product_sku')); ?></label><span class="required"></span>
                                    <input class="form-control length-10" placeholder="<?php echo e(__('admin.product_sku')); ?>" name="product_sku" type="text" value="<?php echo e(old('product_sku')); ?>">
                                    <?php $__errorArgs = ['product_sku'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo e($message); ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form-group">
                                    <label><?php echo e(__('admin.category')); ?></label><span class="required"></span>
                                    <select name="category_id[]"  class="form-control select2 select-category <?php echo e(config('custom.select2-css')); ?>" data-dropdown-css-class="<?php echo e(config('custom.select2-css')); ?>">
                                        <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                        $categoryName = $category->getCategoryDescription();
                                        ?>
                                        <option value="<?php echo e($category->id); ?>" <?php echo e((collect(old('category_id'))->contains($category->id)) ? 'selected':''); ?>>
                                            <?php if($category->parent_id): ?>
                                            <?php
                                            $parentCategory = $category->getParent($category->parent_id);
                                            ?>
                                            <?php endif; ?>
                                            <?php echo e($categoryName?ucfirst($categoryName->category_name):''); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <small class="<?php echo e(config('custom.text-note-css')); ?>"> 
                                    </small>
                                    <?php $__errorArgs = ['category_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo e($message); ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form-group">
                                    <label><?php echo e(__('admin.brand')); ?></label><span class="required"></span>
                                    <select name="brand"  class="form-control select2 select-category <?php echo e(config('custom.select2-css')); ?>" data-dropdown-css-class="<?php echo e(config('custom.select2-css')); ?>">
                                        <?php $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                        $record = $brand->getBrandDescription();
                                        ?>
                                        <option value="<?php echo e($brand->id); ?>" <?php echo e((collect(old('brand'))->contains($brand->id)) ? 'selected':''); ?>>
                                            <?php echo e($record?$record->brand_name:''); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <?php $__errorArgs = ['brand'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo e($message); ?></div>
                                    <?php endif; ?>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label><?php echo e(__('admin.product_regular_price')); ?></label><span class="required"></span>
                                    <input class="form-control float-input length-10" placeholder="<?php echo e(__('admin.product_regular_price')); ?>" name="product_regular_price" type="text" value="<?php echo e(old('product_regular_price')); ?>">
                                    <?php $__errorArgs = ['product_regular_price'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo e($message); ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label><?php echo e(__('admin.product_sale_price')); ?></label>
                                    <input class="form-control float-input length-10" placeholder="<?php echo e(__('admin.product_sale_price')); ?>" name="product_sale_price" type="text" value="<?php echo e(old('product_sale_price')); ?>">
                                    <?php $__errorArgs = ['product_sale_price'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo e($message); ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>                                                                             
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label><?php echo e(__('admin.product_quantity')); ?></label>
                                    <input class="form-control float-input length-10" placeholder="<?php echo e(__('admin.product_quantity')); ?>" name="product_quantity" type="text" value="<?php echo e(old('product_quantity')); ?>">
                                    <?php $__errorArgs = ['product_quantity'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo e($message); ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group"> 
                                    <label><?php echo e(__('admin.sorting_order')); ?></label><span class="required"></span><small class="<?php echo e(config('custom.text-note-css')); ?>">(<?php echo e(__('admin.highest_sorting')); ?> : <?php echo e($sortingNumber); ?>)</small>
                                    <input type="text" readonly="" name="sorting" placeholder="<?php echo e(__('admin.sorting_order')); ?>" class="form-control number-input <?php echo e($errors->has('sorting') ?'is-invalid':''); ?>" value="<?php echo e(old('sorting',$sortingNumber)); ?>">
                                    <?php $__errorArgs = ['sorting'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo $message; ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-group"> 
                                    <label><?php echo e(__('admin.product_default_image')); ?></label><span class="required"></span> <small class="<?php echo e(config('custom.text-note-css')); ?>"><?php echo e($message_text_dimen); ?></small>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" name="product_default_image" class="custom-file-input" onchange="readURL(this, 'default-image')">
                                            <label class="custom-file-label"><?php echo e(__('admin.product_default_image')); ?></label>
                                        </div>                                      
                                    </div>
                                    <?php $__errorArgs = ['product_default_image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo e($message); ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-1 text-center">
                                <label class="profile_preview">&nbsp;</label>
                                <div class="text-center">
                                    <img class="logo-table default-image text-center mt-0" src="" onerror="this.onerror=null;this.src='<?php echo e(asset(Storage::url('default.png'))); ?>'" onclick="showDetails(this.src)"/>
                                </div>
                            </div>
                        </div>
                        <hr class="hr-custom">
                        <div class="row">
                            <div class="col-md-12">
                                <div class=""> 
                                    <label><?php echo e(__('admin.product_images')); ?> <small class="<?php echo e(config('custom.text-note-css')); ?>"><?php echo e($message_text_dimen); ?></small></label>
                                </div>
                            </div>
                        </div>
                        <div class="add-more-append">
                            <?php for($i = 1;$i<= session()->get('form_count');$i++): ?>
                            <div class="row">
                                <div class="col-md-11">
                                    <div class="form-group"> 
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" name="product_images[]" class="custom-file-input" onchange="readURL(this, 'other-image-<?php echo $i; ?>')">
                                                <label class="custom-file-label"><?php echo e(__('admin.product_images')); ?></label>
                                            </div>
                                            <div class="input-group-append">
                                                <!--<span class="input-group-text">Upload</span>-->
                                            </div>
                                        </div>
                                        <?php $__errorArgs = ['product_images.*'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <div class="text-red"><?php echo e($message); ?></div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-md-1 text-center">
                                    <div class="btn-toolbar float-right">
                                        <div class="btn-group mr-2 ">
                                            <div class="">
                                                <img class="logo-table text-center other-image-<?php echo $i; ?>" src="" onclick="showDetails(this.src)" onerror="this.onerror=null;this.src='<?php echo e(asset(Storage::url('default.png'))); ?>'"/>
                                            </div>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" class="remove-btn <?php echo e(config('custom.btn-danger','btn btn-outline-danger btn-sm')); ?>" title="<?php echo e(__('admin.remove')); ?>"><i class="fas fa-trash"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endfor; ?>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <button type="button" title="<?php echo e(__('admin.add_more')); ?>"class="add_button text-center <?php echo e(config('custom.btn-success-form')); ?>"><?php echo e(__('admin.add_more')); ?> (<span class="add-more-limit"><?php echo e(config('admin.product.addMoreLimit')); ?></span>)</button>
                            </div>
                        </div>                     
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="icheck-primary">
                                        <input type="checkbox" name="product_seo" id="product_seo" value="1" <?php echo e((collect(old('product_seo'))->contains("1")) ? 'checked':''); ?>>
                                        <label for="product_seo">
                                            <?php echo e(__('admin.product_seo')); ?>

                                        </label>
                                    </div>
                                </div>
                            </div>                           
                        </div>
                        <div class="row seo-fields-box">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label><?php echo e(__('admin.product_seo_title')); ?></label><span class=""></span>
                                    <input disabled="" class="form-control length-150 seo-fields" placeholder="<?php echo e(__('admin.product_seo_title')); ?>" name="product_seo_title" type="text" value="<?php echo e(old('product_seo_title')); ?>">
                                    <?php $__errorArgs = ['product_seo_title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo e($message); ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label><?php echo e(__('admin.product_seo_description')); ?></label><span class=""></span>
                                    <div class="product_seo_description hidden"></div>
                                    <input disabled="" class="form-control length-150 seo-fields" placeholder="<?php echo e(__('admin.product_seo_description')); ?>" name="product_seo_description" type="text" value="<?php echo e(old('product_seo_description')); ?>">
                                    <?php $__errorArgs = ['product_seo_description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="text-red"><?php echo e($message); ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <span class="float-left"><?php echo __('admin.additional_notes'); ?></span>
                        <div class="btn-toolbar float-right">
                            <div class="btn-group mr-2">
                                <a href="<?php echo e(route('admin.product.product.index')); ?>" class="<?php echo e(config('custom.btn-danger-form')); ?>" title="<?php echo e(__('admin.cancel')); ?>"><?php echo e(__('admin.cancel')); ?></a>
                            </div>
                            <div class="btn-group">
                                <button type="submit" class="<?php echo e(config('custom.btn-success-form')); ?>" title="<?php echo e(__('admin.submit')); ?>"><?php echo e(__('admin.submit')); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_specific_js'); ?>
<?php echo $__env->make('layout/select2', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout/ckeditor', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout/fileupload', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout/image_preview', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script src="<?php echo e(asset('plugins/moment/moment.min.js')); ?>" crossorigin="anonymous"></script>
<script src="<?php echo e(asset('plugins/datetimepicker/bootstrap-4.js')); ?>" crossorigin="anonymous"></script>
<script>
                                                    var addMoreLimit = "<?php echo e(config('admin.product.addMoreLimit')); ?>";
                                                    var addMoreCount = '<?php echo session()->get('form_count'); ?>';
                                                    var dynamicClass = 0;
                                                    $(document).ready(function ()
                                                    {
                                                        changeCount();
                                                        /*
                                                         * For delete old input image 
                                                         */
                                                        removeElement();
                                                        $(".add_button").click(function ()
                                                        {
                                                            addMoreCount++;
                                                            dynamicClass++;
                                                            if (addMoreCount <= addMoreLimit)
                                                            {
                                                                bindHtml(dynamicClass);
                                                                if (addMoreCount == addMoreLimit)
                                                                {
                                                                    $(this).attr("disabled", "disabled");
                                                                    $(this).hide();
                                                                }
                                                            }
                                                            else
                                                            {
                                                                $(this).attr("disabled", "disabled");
                                                                $(this).hide();
                                                            }
                                                        });
                                                        $(".select-category").select2({
                                                            placeholder: "<?php echo e(__('admin.select_category')); ?>",
                                                            allowClear: true
                                                        });
                                                        $(".select-sub-category").select2({
                                                            placeholder: "<?php echo e(__('admin.select_subcategory')); ?>",
                                                            allowClear: true
                                                        });
                                                        $(".select-unit").select2({
                                                            placeholder: "<?php echo e(__('admin.product_weight_unit')); ?>",
                                                            allowClear: true
                                                        });
                                                        /*
                                                         *OnLoad 
                                                         */
                                                        seoFields($("input[name=product_seo]"));

                                                        /*
                                                         * OnChange
                                                         */
                                                        $("input[name=product_seo]").change(function ()
                                                        {
                                                            seoFields($(this));
                                                        });

                                                        {
                                                            /*
                                                             * Datepicker
                                                             */
                                                            startDateTime();
                                                            endDateTime();
                                                            $(".start-date").on('change.datetimepicker', function (e)
                                                            {
                                                                $(".end-date").datetimepicker("destroy");
                                                                $(".end-time").datetimepicker("destroy");
                                                                $(".end-date").val("");
                                                                $(".end-time").val("");
                                                                endDateTime();
                                                                $(".end-date").datetimepicker('minDate', e.date);
                                                            });
                                                            startDateTimeSpecial();
                                                            endDateTimeSpecial();
                                                            $(".special-start-date").on('change.datetimepicker', function (e)
                                                            {
                                                                $(".special-end-date").datetimepicker("destroy");
                                                                $(".special-end-time").datetimepicker("destroy");
                                                                $(".special-end-date").val("");
                                                                $(".special-end-time").val("");
                                                                endDateTimeSpecial();
                                                                $(".special-end-date").datetimepicker('minDate', e.date);
                                                            });
                                                        }
                                                    });
                                                    function removeElement()
                                                    {
                                                        $(".remove-btn").click(function ()
                                                        {
                                                            $(this).closest('div.row').remove(); //Remove field html
                                                            addMoreCount--; //Decrease added count
                                                            $(".add-more-limit").text(addMoreLimit - addMoreCount) //Change count text
                                                            $(".add_button").removeAttr("disabled"); //enable button
                                                            $(".add_button").show();
                                                        });
                                                    }

                                                    function seoFields(seo)
                                                    {
                                                        if (seo.prop('checked'))
                                                        {
                                                            $(".seo-fields").removeAttr('disabled');
                                                            $(".seo-fields-box").show();
                                                            $("input[name=product_seo_title]").val($("input[name=product_name_1]").val());
                                                            $(".product_seo_description").html($("#product_description").val());
                                                            $("input[name=product_seo_description]").val($(".product_seo_description").text());
                                                        }
                                                        else
                                                        {
                                                            $(".seo-fields").attr('disabled', 'disabled');
                                                            $(".seo-fields").val("");
                                                            $(".seo-fields-box").hide();
                                                        }
                                                    }
                                                    function readURL(input, imageClass)
                                                    {
                                                        if (input.files && input.files[0])
                                                        {
                                                            var reader = new FileReader();
                                                            reader.onload = function (e)
                                                            {
                                                                $('.' + imageClass).attr('src', e.target.result);
                                                            }
                                                            reader.readAsDataURL(input.files[0]);
                                                        }
                                                    }
                                                    /*
                                                     * Bind Image Div
                                                     */
                                                    function bindHtml(countHtml)
                                                    {
                                                        $.ajax({
                                                            type: "POST",
                                                            url: "<?php echo e(route('admin.product.product.bind-html')); ?>",
                                                            data: {count: countHtml, '_token': "<?php echo e(csrf_token()); ?>"},
                                                            success: function (res)
                                                            {
                                                                if (res)
                                                                {
                                                                    $(".add-more-append").append(res);
                                                                    changeCount();
                                                                    /*
                                                                     * Bind file input for newly added element
                                                                     */
                                                                    bsCustomFileInput.init();
                                                                    /*
                                                                     * Unbind previously binded event and bind it again
                                                                     */
                                                                    $(".remove-btn").unbind('click');
                                                                    removeElement();
                                                                }
                                                            }
                                                        });
                                                    }

                                                    /*
                                                     * Change Addmore Count
                                                     */
                                                    function changeCount()
                                                    {
                                                        $(".add-more-limit").text(addMoreLimit - addMoreCount)
                                                    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\japan_ecomm_admin\resources\views/admin/product/product/add.blade.php ENDPATH**/ ?>