<?php

use Illuminate\Support\Facades\Route;

Route::get("/admin", function()
{
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('login');
});
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['guest:admin']], function ()
{
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('login');
});
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth:admin']], function ()
{
    Route::post('/logout', 'Auth\AdminLoginController@logout')->name('logout');
});
Route::group(['namespace' => 'Admin', 'middleware' => ['can:' . config('custom_middleware.view_admin'), 'auth:admin'], 'prefix' => 'admin', 'as' => 'admin.'], function ()
{
    Route::group(['namespace' => 'Home', 'prefix' => 'home', 'as' => 'home.', 'middleware' => ['can:' . config('custom_middleware.view_dashboard')]], function ()
    {
        $default    = "index";
        $controller = "HomeController@";
        Route::get('/', $controller . $default)->name('index'); //Default Page
    });
    Route::group(['namespace' => 'Product', 'prefix' => 'product', 'as' => 'product.', 'middleware' => ['can:' . config('custom_middleware.view_product_product')]], function ()
    {
        $default    = "index";
        $controller = "ProductController@";
        Route::get('/', ['middleware' => ['can:' . config('custom_middleware.view_product_product')], 'uses' => $controller . $default])->name('index'); //Default Page
        Route::get('/add', ['middleware' => ['can:' . config('custom_middleware.create_product_product')], 'uses' => $controller . 'create'])->name('add'); //Add Form
        Route::post('/add', ['middleware' => ['can:' . config('custom_middleware.create_product_product')], 'uses' => $controller . 'store'])->name('add'); //Save
        Route::get('/edit/{id}', ['middleware' => ['can:' . config('custom_middleware.edit_product_product')], 'uses' => $controller . 'edit'])->name('edit'); //Update Form
        Route::post('/update/{id}', ['middleware' => ['can:' . config('custom_middleware.edit_product_product')], 'uses' => $controller . 'update'])->name('update'); //Update
        Route::get('/delete/{id}', ['middleware' => ['can:' . config('custom_middleware.delete_product_product')], 'uses' => $controller . 'delete'])->name('delete'); //Delete
    });

    Route::group(['namespace' => 'OrderStatus', 'prefix' => 'order-status', 'as' => 'order-status.', 'middleware' => ['can:' . config('custom_middleware.view_product_product')]], function ()
    {
        $default    = "index";
        $controller = "OrderStatusController@";
        Route::get('/', ['middleware' => ['can:' . config('custom_middleware.view_product_product')], 'uses' => $controller . $default])->name('index'); //Default Page
        Route::get('/add', ['middleware' => ['can:' . config('custom_middleware.create_product_product')], 'uses' => $controller . 'create'])->name('add'); //Add Form
        Route::post('/add', ['middleware' => ['can:' . config('custom_middleware.create_product_product')], 'uses' => $controller . 'store'])->name('add'); //Save
        Route::get('/edit/{id}', ['middleware' => ['can:' . config('custom_middleware.edit_product_product')], 'uses' => $controller . 'edit'])->name('edit'); //Update Form
        Route::post('/update/{id}', ['middleware' => ['can:' . config('custom_middleware.edit_product_product')], 'uses' => $controller . 'update'])->name('update'); //Update
        Route::get('/delete/{id}', ['middleware' => ['can:' . config('custom_middleware.delete_product_product')], 'uses' => $controller . 'delete'])->name('delete'); //Delete
    });

    Route::group(['namespace' => 'Customer', 'prefix' => 'customer', 'as' => 'customer.', 'middleware' => ['can:' . config('custom_middleware.view_customer')]], function ()
    {
        $default    = "index";
        $controller = "CustomerController@";

        Route::get('/', ['middleware' => ['can:' . config('custom_middleware.view_customer')], 'uses' => $controller . $default])->name('index'); //Default Page
        Route::get('/add', ['middleware' => ['can:' . config('custom_middleware.create_customer')], 'uses' => $controller . 'create'])->name('add'); //Add Form
        Route::post('/add', ['middleware' => ['can:' . config('custom_middleware.create_customer')], 'uses' => $controller . 'store'])->name('add'); //Add Form
        Route::get('/edit/{id}', ['middleware' => ['can:' . config('custom_middleware.edit_customer')], 'uses' => $controller . 'edit'])->name('edit'); //Add Form
        Route::post('/update/{id}', ['middleware' => ['can:' . config('custom_middleware.edit_customer')], 'uses' => $controller . 'update'])->name('update'); //Add Form
        Route::get('/delete/{id}', ['middleware' => ['can:' . config('custom_middleware.delete_customer')], 'uses' => $controller . 'delete'])->name('delete'); //Delete
        Route::get('/change-status/{id}/{active}', ['middleware' => ['can:' . config('custom_middleware.edit_customer')], 'uses' => $controller . 'changeStatus'])->name('change-status'); //Change Status
    });
});



