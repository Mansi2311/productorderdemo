<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use Auth;
use Session;

class Helper
{

    public static function getOrderColumn($request, $defaultKey = "", $defaultVal = "")
    {
        $sortKey      = $defaultKey ? $defaultKey : "id";
        $sortVal      = $defaultVal ? $defaultVal : "asc";
        $requestParam = $request->except('q');
        foreach ($requestParam as $key => $val)
        {
            if ($val == 'asc' || $val == 'desc')
            {
                $sortKey = $key;
                $sortVal = $val;
                break;
            }
        }

        return [
            'sortKey' => $sortKey,
            'sortVal' => $sortVal
        ];
    }

    public static function getOrderSortingColumn($request)
    {
        $sortKey      = "sorting";
        $sortVal      = "asc";
        $requestParam = $request->except('q');
        foreach ($requestParam as $key => $val)
        {
            if ($val == 'asc' || $val == 'desc')
            {
                $sortKey = $key;
                $sortVal = $val;
                break;
            }
        }

        return [
            'sortKey' => $sortKey,
            'sortVal' => $sortVal
        ];
    }


}
