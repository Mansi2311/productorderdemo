<?php

namespace App\AdminModel\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminProduct extends Model
{

    use SoftDeletes;

    protected $table   = 'admin_products';
    protected $guarded = ['created_at,updated_at,deleted_at'];

    /*
     * Boot Functions
     */

    public static function boot()
    {
        parent::boot();

        static::creating(function ($data)
        {
            $data->created_by = auth()->user()->id;
        });

        static::updating(function ($data)
        {
            $data->updated_by = auth()->user()->id;
        });

        static::deleting(function ($data)
        {
            $data->deleted_by = auth()->user()->id;
            $data->save();
        });
    }

    /*
     * Bind Table Data
     */

    public function getAllData($searchText, $sortKey, $sortVal, $pageLength)
    {

        $query = $this->orderBy($sortKey, $sortVal);


        if ($searchText)
        {
            $query->where(function ($query) use ($searchText)
            {
                $query->orWhere('product_name', 'LIKE', '%' . $searchText . '%');
                $query->orWhere('product_price', 'LIKE', '%' . $searchText . '%');
                $query->orWhere('product_quantity', 'LIKE', '%' . $searchText . '%');
                $query->orWhere('product_code', 'LIKE', '%' . $searchText . '%');
            });
        }

        if ($pageLength)
        {
            $paginate = strtolower($pageLength) == 'all' ? $query->count() : $pageLength;
        }
        else
        {
            $paginate = config('custom.per_page', 10);
        }
        return $query->paginate($paginate);
    }

}
