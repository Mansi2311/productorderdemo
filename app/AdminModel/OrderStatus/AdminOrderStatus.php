<?php

namespace App\AdminModel\OrderStatus;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Admin;
use App\AdminModel\Product\AdminProduct;

class AdminOrderStatus extends Model
{

    use SoftDeletes;

    protected $table   = 'admin_order_status';
    protected $guarded = ['created_at,updated_at,deleted_at'];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($data)
        {
            $data->created_by = auth()->user()->id;
        });

        static::updating(function ($data)
        {
            $data->updated_by = auth()->user()->id;
        });

        static::deleting(function ($data)
        {
            $data->deleted_by = auth()->user()->id;
            $data->save();
        });
    }

    public static function getAllData($searchText = '', $sortKey = '', $sortVal = '', $pageLength)
    {
        $query = AdminOrderStatus::orderBy($sortKey, $sortVal);
        if ($sortKey && $sortVal)
        {
            $query->orderBy($sortKey, $sortVal);
        }
        if ($searchText)
        {
            $query->where('order_status', 'LIKE', '%' . $searchText . '%');
            
        }
        /*
         * Search by Active/Inactive Keyword
         */
        if (strtolower($searchText) == 'active')
        {
            $query->orWhere('is_active', 1);
        }
        if (strtolower($searchText) == 'inactive')
        {
            $query->orWhere('is_active', 0);
        }
        if ($pageLength)
        {
            $paginate = strtolower($pageLength) == 'all' ? $query->count() : $pageLength;
        }
        else
        {
            $paginate = config('custom.per_page', 10);
        }
        return $query->paginate($paginate);
    }

    public function getProduct($id)
    {
        return AdminProduct::where('id', $id)->first();
    }

    public function getCustomer($id)
    {
        return Admin::where('id', $id)->first();
    }

}
