<?php

namespace App\AdminModel\Customer;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\AdminModel\Customer\AdminUserAddressDetail;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Auth;

class AdminCustomer extends Model
{

    use HasRoles,
        SoftDeletes;

    protected $table   = 'admins';
    protected $guarded = ['created_at,updated_at,deleted_at'];
//    protected $guard   = 'admins';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($data)
        {
            $data->created_by = auth()->user()->id;
        });
        static::updating(function ($data)
        {
            $data->updated_by = auth()->user()->id;
        });
        static::deleting(function ($data)
        {
            $data->deleted_by = auth()->user()->id;
            $data->save();
        });
    }

    public function getAllData($searchText = '', $sortKey = '', $sortVal = '', $pageLength)
    {
        $query = $this->orderBy($sortKey, $sortVal);
        if ($searchText)
        {
            $query->where(function ($query) use ($searchText)
            {
                /*
                 * Search by category
                 */
                $query->orWhere('id', 'LIKE', '%' . $searchText . '%');
                $query->orWhere('first_name', 'LIKE', '%' . $searchText . '%');
                $query->orWhere('last_name', 'LIKE', '%' . $searchText . '%');
                $query->orWhere('email', 'LIKE', '%' . $searchText . '%');
                $query->orWhere('mobile', 'LIKE', '%' . $searchText . '%');
                $record = $this->searchbByAddress($searchText);
                $query->orWhereIn('id', $record);
                /*
                 * Search by Active/Inactive Keyword
                 */
                if (strtolower($searchText) == 'active')
                {
                    $query->orWhere('is_active', 1);
                }
                if (strtolower($searchText) == 'inactive')
                {
                    $query->orWhere('is_active', 0);
                }
            });
        }
        if ($pageLength)
        {
            $paginate = strtolower($pageLength) == 'all' ? $query->count() : $pageLength;
        }
        else
        {
            $paginate = config('custom.per_page', 10);
        }
        return $query->paginate($paginate);
    }

    public function getUserAddressData()
    {
        $data = $this->hasOne('App\AdminModel\Customer\AdminUserAddressDetail', 'user_id', 'id')->first();
        return $data;
    }

    /*
     * search By Title
     */

    public function searchbByAddress($searchText)
    {
        $query = AdminUserAddressDetail::orderBy('updated_at', 'desc');
        if ($searchText)
        {
            $query->where('address', 'LIKE', '%' . $searchText . '%');
            $query->orWhere('suburb', 'LIKE', '%' . $searchText . '%');
            $query->orWhere('city', 'LIKE', '%' . $searchText . '%');
            $query->orWhere('postcode', 'LIKE', '%' . $searchText . '%');
            $record  = $this->searchbByCountry($searchText);
            $query->orWhereIn('country_id', $record);
            $record1 = $this->searchbByState($searchText);
            $query->orWhereIn('state_id', $record1);
        }
        return $query->pluck('user_id');
    }

    public function searchbByCountry($searchText)
    {
        return \App\AdminModel\AdminCountryDescription::where('language_id', 1)->where('country_name', 'LIKE', '%' . $searchText . '%')->pluck('country_id');
    }

    public function searchbByState($searchText)
    {
        return \App\AdminModel\Master\State\AdminStateDescription::where('language_id', 1)->where('state_name', 'LIKE', '%' . $searchText . '%')->pluck('state_id');
    }

}
