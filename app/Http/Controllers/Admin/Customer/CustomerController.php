<?php

namespace App\Http\Controllers\Admin\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Helper;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\SuperAdmin\Role;
use Illuminate\Support\Facades\Auth;
use App\Admin;

class CustomerController extends Controller
{

    private $langFile   = 'admin';
    private $module     = '.customer';

    public function index(Request $request)
    {
        /*
         * Get SortKey and SortOrder from helper
         */
        $sorting    = Helper::getOrderColumn($request);
        $sortKey    = $sorting['sortKey'];
        $sortVal    = $sorting['sortVal'];
        $searchText = $request->q;
        $pageLength = $request->pageLength;
        $customer   = new admin;
        $customer   = $customer->getAllData($searchText, $sortKey, $sortVal, $pageLength);
        return view('admin.customer.index', compact('customer', 'sortKey', 'sortVal', 'searchText', 'pageLength'));
    }

    public function create()
    {
        return view("admin.customer.add");
    }

    public function store(Request $request)
    {
        /*
         * Validations
         */
        $validationRule    = [];
        $validationMessage = [];
        $validationRule    = [
            'first_name' => "required",
            'last_name'  => 'required',
            'mobile'     => 'required',
            'email'      => 'required|email|unique:users,email',
            'password'   => 'required',
        ];
        $validationMessage = [
            'first_name.required' => __($this->langFile . '.error-req-first-name'),
            'last_name.required'  => __($this->langFile . '.error-req-last-name'),
            'mobile.required'     => __($this->langFile . '.error-req-telephone'),
            'email.required'      => __($this->langFile . '.error-req-email'),
            'email.email'         => __($this->langFile . '.error-email-email'),
            'email.unique'        => __($this->langFile . '.error-unique-email'),
            'password.required'   => __($this->langFile . '.error-req-password'),
        ];
        $validatedData     = $request->validate($validationRule, $validationMessage);
        DB::beginTransaction();
        try
        {
            $model             = new Admin();
            $model->first_name = $request->first_name;
            $model->last_name  = $request->last_name;
            $model->mobile     = $request->mobile;
            $model->email      = $request->email;
            $model->password   = Hash::make($request->password);
            $model->gender     = $request->gender;
            $model->address    = $request->address;
            /*
             * main save
             */
            if ($model->save())
            {
                /*
                 * Insert status Flag
                 */
                $flag        = true;
                $roleId      = 2;
//assign role
                $roleManager = \App\SuperAdmin\Role::find($roleId); //App Admin 
                $model->assignRole($roleManager);
                if ($flag)
                {
                    DB::commit();
                    $status       = 'success';
                    $message_text = __($this->langFile . '.add-success');
                    $message_text = Str::replaceFirst("#module#", __($this->langFile . $this->module), $message_text);
                }
                else
                {
                    DB::rollback();
                    $status       = 'error';
                    $message_text = __($this->langFile . '.add-error');
                    $message_text = Str::replaceFirst("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
                }
            }
            else
            {
                DB::rollback();
                $status       = 'error';
                $message_text = __($this->langFile . '.add-error');
                $message_text = Str::replaceFirst("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
            }
        }
        catch (Throwable $e)
        {
            DB::rollback();
            $status       = 'error';
            $message_text = __($this->langFile . '.add-error');
            $message_text = Str::replaceFirst("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
        }
        return redirect()->route('admin.customer.index')->with($status, $message_text);
    }

    public function edit($id)
    {
        $singleRecord = Admin::find($id);
        if ($singleRecord)
        {
            return view("admin.customer.edit", compact('id', 'singleRecord'));
        }
        else
        {
            $status       = 'error';
            $message_text = __($this->langFile . '.record_not_found');
        }
        return redirect()->route('admin.customer.index')->with($status, $message_text);
    }

    public function update(Request $request, $id)
    {
        $model = Admin::find($id);
        if ($model)
        {
            /*
             * Validations
             */
            $validationRule    = [];
            $validationMessage = [];
            $validationRule    = [
                'first_name' => "required",
                'last_name'  => 'required',
                'mobile'     => 'required',
                'address'    => 'required',
                'email'      => 'required|email|unique:Admin,email,' . $id . ',id,deleted_at,NULL',
            ];
            $validationMessage = [
                'first_name.required' => __($this->langFile . '.error-req-first-name'),
                'last_name.required'  => __($this->langFile . '.error-req-last-name'),
                'mobile.required'     => __($this->langFile . '.error-req-telephone'),
                'email.required'      => __($this->langFile . '.error-req-email'),
                'email.email'         => __($this->langFile . '.error-email-email'),
                'email.unique'        => __($this->langFile . '.error-unique-email'),
                'address.required'    => __($this->langFile . '.error-req-required'),
            ];
            $validatedData     = $request->validate($validationRule, $validationMessage);

            $model->first_name = $request->first_name;
            $model->last_name  = $request->last_name;
            $model->mobile     = $request->mobile;
            $model->email      = $request->email;
            $model->address    = $request->address;
            $model->gender     = $request->gender;
            try
            {
                DB::beginTransaction();
                $flag = true;
                if ($model->save())
                {

                    /*
                     * Insert status Flag
                     */
                    if ($flag)
                    {
                        DB::commit();
                        $status       = 'success';
                        $message_text = __($this->langFile . '.update-success');
                        $message_text = Str::replaceFirst("#module#", __($this->langFile . $this->module), $message_text);
                    }
                    else
                    {
                        DB::rollback();
                        $status       = 'error';
                        $message_text = __($this->langFile . '.update-error');
                        $message_text = Str::replaceFirst("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
                    }
                }
                else
                {
                    DB::rollback();
                    $status       = 'error';
                    $message_text = __($this->langFile . '.update-error');
                    $message_text = Str::replaceFirst("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
                }
            }
            catch (Throwable $e)
            {
                DB::rollback();
                $status       = 'error';
                $message_text = __($this->langFile . '.update-error');
                $message_text = Str::replaceFirst("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
            }
        }
        else
        {
            DB::rollback();
            $status       = 'error';
            $message_text = __($this->langFile . '.update-error');
            $message_text = Str::replaceFirst("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
        }
        return redirect()->route('admin.customer.index')->with($status, $message_text);
    }

    /*
     * Change Status
     */

    public function changeStatus($id, $active)
    {
        $singleRecord = admin::find($id);
        if ($singleRecord && $active == 0 || $active == 1)
        {
            DB::beginTransaction();
            try
            {
                $singleRecord->is_active = $active;
                if ($singleRecord->save())
                {
                    DB::commit();
                    $status       = 'success';
                    $message_text = __($this->langFile . '.status-change-success');
                    $message_text = Str::replaceFirst("#module#", __($this->langFile . $this->module), $message_text);
                    if ($active == 1)
                    {
                        $message_text = Str::replaceFirst("#status#", '"' . (__($this->langFile . '.active') . '"'), $message_text);
                    }
                    else
                    {
                        $message_text = Str::replaceFirst("#status#", '"' . (__($this->langFile . '.in_active') . '"'), $message_text);
                    }
                }
                else
                {
                    DB::rollback();
                    $status       = 'error';
                    $message_text = __($this->langFile . '.status-change-error');
                    $message_text = Str::replaceFirst("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
                }
            }
            catch (Throwable $e)
            {
                DB::rollback();
                $status       = 'error';
                $message_text = __($this->langFile . '.status-change-error');
                $message_text = Str::replaceFirst("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
            }
            return redirect()->route('admin.customer.index', [http_build_query($_GET)])->with($status, $message_text);
        }
    }

    public function delete($id)
    {
        if ($id)
        {
            $record = admin::find($id);
            if ($record)
            {
                DB::beginTransaction();
                try
                {
                    if ($record->delete())
                    {
                        DB::commit();
                        $status       = 'success';
                        $message_text = __($this->langFile . '.delete-success');
                        $message_text = Str::replaceFirst("#module#", __($this->langFile . $this->module), $message_text);
                    }
                    else
                    {
                        DB::rollback();
                        $status       = 'error';
                        $message_text = __($this->langFile . '.delete-error');
                        $message_text = Str::replaceFirst("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
                    }
                }
                catch (Throwable $e)
                {
                    DB::rollback();
                    $status       = 'error';
                    $message_text = __($this->langFile . '.delete-error');
                    $message_text = Str::replaceFirst("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
                }
            }
            else
            {
                $status       = 'error';
                $message_text = __($this->langFile . '.record_not_found');
            }
        }
        else
        {
            $status       = 'error';
            $message_text = __($this->langFile . '.cannot-delete');
        }
        return redirect()->route('admin.customer.index')->with($status, $message_text);
    }

}
