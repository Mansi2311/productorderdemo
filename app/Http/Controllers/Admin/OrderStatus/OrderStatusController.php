<?php

namespace App\Http\Controllers\Admin\OrderStatus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Helper;
use DB;
use Illuminate\Support\Str;
use App\AdminModel\OrderStatus\AdminOrderStatus;
use App\Admin;
use App\AdminModel\Product\AdminProduct;

class OrderStatusController extends Controller
{

    private $langFile = 'admin';
    private $module   = '.order_status';

    public function index(Request $request)
    {
        /*
         * Get SortKey and SortOrder from helper
         */
        $sorting    = Helper::getOrderSortingColumn($request);
        $sortKey    = $sorting['sortKey'];
        $sortVal    = $sorting['sortVal'];
        $searchText = $request->q;
        $pageLength = $request->pageLength;

        $orderStatus = new AdminOrderStatus();
        $orderStatus = $orderStatus->getAllData($searchText, $sortKey, $sortVal, $pageLength);
        return view('admin.OrderStatus.index', compact('pageLength', 'searchText', 'sortKey', 'sortVal', 'orderStatus'));
    }

    public function create()
    {
        $customer = new Admin();
        $customer = $customer->all();
        $product  = new AdminProduct();
        $product  = $product->all();
        return view("admin.OrderStatus.add", compact('customer', 'product'));
    }

    public function store(Request $request)
    {
        $request->validate(
                [
                    'order_status' => 'required',
                    'admin_id'     => 'required',
                    'product_id'   => 'required',
                ]
                , [
            'order_status.required' => __($this->langFile . '.error-req-order-status'),
            'admin_id.required'     => __($this->langFile . '.error-req-select'),
            'product_id.required'   => __($this->langFile . '.error-req-select'),
        ]);
        DB::beginTransaction();

        try
        {
            $orderStatus               = new AdminOrderStatus();
            $orderStatus->order_status = $request->order_status;
            $orderStatus->admin_id     = $request->admin_id;
            $orderStatus->product_id   = $request->product_id;
            if ($orderStatus->save())
            {
                DB::commit();
                $status       = 'success';
                $message_text = __($this->langFile . '.add-success');
                $message_text = Str::replaceFirst("#module#", __($this->langFile . $this->module), $message_text);
            }
            else
            {
                DB::rollback();
                $status       = 'error';
                $message_text = __($this->langFile . '.add-error');
                $message_text = Str::replaceFirst("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
            }
        }
        catch (Throwable $e)
        {
            DB::rollback();
            $status       = 'error';
            $message_text = __($this->langFile . '.add-error');
            $message_text = Str::replaceFirst("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
        }

        return redirect()->route('admin.order-status.index')->with($status, $message_text);
    }

    public function edit($id)
    {
        $singleRecord = AdminOrderStatus::find($id);
        if ($singleRecord)
        {
            $customer = new Admin();
            $customer = $customer->all();
            $product  = new AdminProduct();
            $product  = $product->all();
            return view("admin.OrderStatus.edit", compact('singleRecord', 'customer', 'product'));
        }
        else
        {
            $status       = 'error';
            $message_text = __($this->langFile . '.record_not_found');
            return redirect()->route('admin.order-status.index')->with($status, $message_text);
        }
    }

    public function update(Request $request, $id)
    {
        $orderStatus = AdminOrderStatus::find($id);
        if ($orderStatus)
        {
            $request->validate(
                    [
                        'order_status' => 'required',
                        'admin_id'     => 'required',
                        'product_id'   => 'required',
                    ]
                    , [
                'order_status.required' => __($this->langFile . '.error-req-order-status'),
                'admin_id.required'     => __($this->langFile . '.error-req-select'),
                'product_id.required'   => __($this->langFile . '.error-req-select'),
            ]);

            DB::beginTransaction();
            try
            {
                $orderStatus->order_status = strtolower($request->order_status);
                $orderStatus->admin_id     = $request->admin_id;
                $orderStatus->product_id   = $request->product_id;

                if ($orderStatus->save())
                {
                    DB::commit();
                    $status       = 'success';
                    $message_text = __($this->langFile . '.update-success');
                    $message_text = Str::replaceFirst("#module#", __($this->langFile . $this->module), $message_text);
                }
                else
                {
                    DB::rollback();
                    $status       = 'error';
                    $message_text = __($this->langFile . '.update-error');
                    $message_text = Str::replaceFirst("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
                }
            }
            catch (Throwable $e)
            {
                DB::rollback();
                $status       = 'error';
                $message_text = __($this->langFile . '.update-error');
                $message_text = Str::replaceFirst("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
            }
        }
        else
        {
            $status       = 'error';
            $message_text = __($this->langFile . '.record_not_found');
        }
        return redirect()->route('admin.order-status.index')->with($status, $message_text);
    }

    public function delete($id)
    {
        $orderStatus = AdminOrderStatus::all()->count();
        if ($orderStatus > 1)
        {
            if ($id)
            {
                $record = AdminOrderStatus::find($id);

                if ($record)
                {
                    DB::beginTransaction();
                    try
                    {
                        if ($record->delete())
                        {
                            DB::commit();
                            $status       = 'success';
                            $message_text = __($this->langFile . '.delete-success');
                            $message_text = Str::replaceFirst("#module#", __($this->langFile . $this->module), $message_text);
                        }
                        else
                        {
                            DB::rollback();
                            $status       = 'error';
                            $message_text = __($this->langFile . '.delete-error');
                            $message_text = Str::replaceFirst("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
                        }
                    }
                    catch (Throwable $e)
                    {
                        DB::rollback();
                        $status       = 'error';
                        $message_text = __($this->langFile . '.delete-error');
                        $message_text = Str::replaceFirst("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
                    }
                }
                else
                {
                    $status       = 'error';
                    $message_text = __($this->langFile . '.record_not_found');
                }
            }
            else
            {
                $status       = 'error';
                $message_text = __($this->langFile . '.record_not_found');
            }
        }
        else
        {
            $status       = 'error';
            $message_text = __($this->langFile . '.cannot-delete');
        }
        return redirect()->route('admin.order-status.index')->with($status, $message_text);
    }

}
