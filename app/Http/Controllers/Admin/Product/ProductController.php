<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Helper;
use DB;
use App\AdminModel\Product\AdminProduct;

class ProductController extends Controller
{

    private $langFile = 'admin';
    private $module   = '.product';
    private $mediaId  = 1;

    public function index(Request $request)
    {
        /*
         * Get SortKey and SortOrder from helper
         */
        $sorting    = Helper::getOrderSortingColumn($request);
        $sortKey    = $sorting['sortKey'];
        $sortVal    = $sorting['sortVal'];
        $searchText = $request->q;
        $pageLength = $request->pageLength;
        $product    = new AdminProduct();
        $product    = $product->getAllData($searchText, $sortKey, $sortVal, $pageLength);
        return view('admin.product.index', compact('sortKey', 'sortVal', 'searchText', 'product', 'pageLength'));
    }

    public function create()
    {
        return view("admin.product.add");
    }

    public function store(Request $request)
    {
        /*
         * Validations
         */
        $validationRule    = [
            'product_code'     => 'required',
            'product_name'     => 'required',
            'product_price'    => 'required|numeric',
            'product_quantity' => 'required|numeric',
        ];
        $validationMessage = [
            'product_name.required'     => __($this->langFile . '.error-product-req-product-code'),
            'product_code.required'     => __($this->langFile . '.error-product-req-product-code'),
            'product_price.required'    => __($this->langFile . '.error-product-req-reg-price'),
            'product_price.numeric'     => __($this->langFile . '.error-product-numeric-reg-price'),
            'product_quantity.required' => __($this->langFile . '.error-product-req-product_quantity'),
            'product_quantity.numeric'  => __($this->langFile . '.error-product-numeric-product_quantity'),
        ];

        $validatedData = $request->validate($validationRule, $validationMessage);

        /*
         * All validation Passed
         */

        DB::beginTransaction();

        try
        {
            /*
             * Insert status Flag
             */
            $flag = true;

            $model = new AdminProduct();

            $model->product_name        = $request->product_name;
            $model->product_code        = $request->product_code;
            $model->product_quantity    = $request->product_quantity;
            $model->product_description = $request->product_description;
            $model->product_price       = $request->product_price;

            /*
             * Save Main Record
             */
            if ($model->save())
            {
                if ($flag)
                {
                    DB::commit();
                    $status       = 'success';
                    $message_text = __($this->langFile . '.add-success');
                    $message_text = str_replace("#module#", __($this->langFile . $this->module), $message_text);
                }
                else
                {
                    DB::rollback();
                    $status       = 'error';
                    $message_text = __($this->langFile . '.add-error');
                    $message_text = str_replace("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
                }
            }
        }
        catch (Throwable $e)
        {
            DB::rollback();
            $status       = 'error';
            $message_text = __($this->langFile . '.add-error');
            $message_text = str_replace("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
        }

        return redirect()->route('admin.product.index')->with($status, $message_text);
    }

    public function edit($id)
    {
        $singleRecord = AdminProduct::find($id);

        if ($singleRecord)
        {

            return view("admin.product.edit", compact('singleRecord'));
        }
        else
        {
            return redirect()->route('admin.product.index')->with('error', __($this->langFile . '.record_not_found'));
        }
    }

    public function update(Request $request, $id)
    {
        $model = AdminProduct::find($id);
        if ($model)
        {
            /*
             * Validations
             */
            $validationRule    = [
                'product_code'     => 'required',
                'product_name'     => 'required',
                'product_price'    => 'required|numeric',
                'product_quantity' => 'required|numeric',
            ];
            $validationMessage = [
                'product_name.required'     => __($this->langFile . '.error-product-req-product-code'),
                'product_code.required'     => __($this->langFile . '.error-product-req-product-code'),
                'product_price.required'    => __($this->langFile . '.error-product-req-reg-price'),
                'product_price.numeric'     => __($this->langFile . '.error-product-numeric-reg-price'),
                'product_quantity.required' => __($this->langFile . '.error-product-req-product_quantity'),
                'product_quantity.numeric'  => __($this->langFile . '.error-product-numeric-product_quantity'),
            ];


            $validatedData = $request->validate($validationRule, $validationMessage);

            /*
             * All validation Passed
             */
            DB::beginTransaction();
            try
            {
                /*
                 * Insert status Flag
                 */
                $flag = true;

                $model->product_name        = $request->product_name;
                $model->product_code        = $request->product_code;
                $model->product_quantity    = $request->product_quantity;
                $model->product_description = $request->product_description;
                $model->product_price       = $request->product_price;

                /*
                 * Save Main Record
                 */
                if ($model->save())
                {
                    if ($flag)
                    {
                        DB::commit();
                        $status       = 'success';
                        $message_text = __($this->langFile . '.update-success');
                        $message_text = str_replace("#module#", __($this->langFile . $this->module), $message_text);
                    }
                    else
                    {
                        DB::rollback();
                        $status       = 'error';
                        $message_text = __($this->langFile . '.update-error');
                        $message_text = str_replace("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
                    }
                }
            }
            catch (Throwable $e)
            {
                DB::rollback();
                $status       = 'error';
                $message_text = __($this->langFile . '.update-error');
                $message_text = str_replace("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
            }
        }
        else
        {
            $status       = 'error';
            $message_text = __($this->langFile . '.record_not_found');
        }

        return redirect()->route('admin.product.index')->with($status, $message_text);
    }

    /*
     * Delete
     */

    public function delete($id)
    {
        if ($id)
        {
            $record = AdminProduct::find($id);

            if ($record)
            {
                DB::beginTransaction();

                try
                {

                    if ($record->delete())
                    {
                        DB::commit();
                        $status       = 'success';
                        $message_text = __($this->langFile . '.delete-success');
                        $message_text = str_replace("#module#", __($this->langFile . $this->module), $message_text);
                    }
                    else
                    {
                        DB::rollback();
                        $status       = 'error';
                        $message_text = __($this->langFile . '.delete-error');
                        $message_text = str_replace("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
                    }
                }
                catch (Throwable $e)
                {
                    DB::rollback();
                    $status       = 'error';
                    $message_text = __($this->langFile . '.delete-error');
                    $message_text = str_replace("#module#", strtolower(__($this->langFile . $this->module)), $message_text);
                }
            }
            else
            {
                $status       = 'error';
                $message_text = __($this->langFile . '.record_not_found');
            }
        }
        else
        {
            $status       = 'error';
            $message_text = __($this->langFile . '.record_not_found');
        }
        return redirect()->route('admin.product.index')->with($status, $message_text);
    }

}
