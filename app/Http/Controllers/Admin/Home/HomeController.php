<?php

namespace App\Http\Controllers\Admin\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use App\UserModel\Order; // Will Remove
use App\AdminModel\Order\WebOrder;
use App\AdminModel\Product\AdminProduct;
use App\AdminModel\Order\AdminOrderStatus;
use App\AdminModel\AdminCustomizedForm;
use App\AdminModel\AdminContactUs;
use App\AdminModel\Product\Wishlist;
use App\User;
use App\Admin;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{

    private $langFile = 'admin';
    private $module   = '.profile';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     */
    public function index()
    {
        return view('index');
    }

}
