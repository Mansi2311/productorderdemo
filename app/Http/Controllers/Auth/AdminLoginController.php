<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Lang;
use Auth;
use App\Admin;
use App\Helpers\Helper;

class AdminLoginController extends Controller
{

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    private $langFile     = 'admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
        $this->username = $this->findUsername();
    }

    public function showLoginForm()
    {
        $url = url()->previous();
        return view('auth.login', compact('url'));
    }

    public function login(Request $request)
    {
        $validatedData = $request->validate([
            'email'    => 'required',
            'password' => 'required',
                ], [
            'email.required'    => Lang::get($this->langFile . '.error-req-email'),
            'password.required' => Lang::get($this->langFile . '.error-req-email'),
        ]);

        $remember_me = $request->remember_me ? true : false;

        if (Auth::guard('admin')->attempt([$this->username => $request->email, 'password' => $request->password, 'is_active' => 1], $remember_me))
        {
            if (isset($request->url))
            {
                return redirect()->to($request->url)->with('success', Lang::get($this->langFile . '.login-success'));
            }
            return redirect()->route('admin.home.index')->with('success', Lang::get($this->langFile . '.login-success'));
        }
        else
        {
            $this->incrementLoginAttempts($request);
            return redirect()->route('admin.login')->with('error', Lang::get($this->langFile . '.login_fail'));
        }

        $this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);
    }

    public function logout(Request $request)
    {
        if (Auth::guard('admin')->logout())
        {

            return redirect()->route('admin.login')->with('success', Lang::get($this->langFile . '.logout_success'));
        }
        else
        {
            return redirect()->route('admin.home.index');
        }
    }





}
