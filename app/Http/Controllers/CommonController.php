<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
/*
 * Models
 */
use App\AdminModel\Product\AdminCategory;
use App\AdminModel\City\AdminCity;
use App\AdminModel\Color\AdminColor;
use App\AdminModel\AdminCountry;
use App\AdminModel\Order\AdminOrderStatus;
use App\AdminModel\GeneralSetting\HomeSlider\AdminHomeSlider;
use App\AdminModel\Product\Product\AdminProduct;
use App\AdminModel\Size\AdminSize;
use App\Helpers\Helper;
use App\AdminModel\Master\PaymentMethod\AdminPaymentMethod;
use App\AdminModel\Master\State\AdminState;
use App\AdminModel\Product\Brand\AdminBrand;
use App\AdminModel\Product\Unit\AdminUnit;
use App\AdminModel\Blog\AdminBlog;

class CommonController extends Controller
{


}
